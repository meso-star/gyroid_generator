	integer Ndim_mx
	integer Nsol_mx
	integer Niter_mx
	integer Nppc_mx
	integer Ncontour_mx
	integer Nv_so_mx
	integer Nf_so_mx
	integer Nv_mx
	integer Nf_mx
	integer Nv_on_boundary_mx
	integer Ngroup_mx
	integer Nv_in_group_mx
	integer Nproba_mx
	integer Nmaterial_mx
	integer Ncode_mx
	integer Nelem_mx
	integer Ncf_mx
	integer Nfig_mx
	integer Npatch_mx
	integer Nprobe_mx
	integer Ntime_mx
	integer Nchar_mx

	parameter(Ndim_mx=3)
	parameter(Nsol_mx=100)
	parameter(Niter_mx=100)
	parameter(Nppc_mx=1000)
	parameter(Ncontour_mx=100)
        parameter(Nv_so_mx=10000)
        parameter(Nf_so_mx=10000)
        parameter(Nv_mx=1000000)
        parameter(Nf_mx=1000000)
        parameter(Nv_on_boundary_mx=10000)
	parameter(Ngroup_mx=1000)
	parameter(Nv_in_group_mx=2000)
	parameter(Nproba_mx=1000)
	parameter(Nmaterial_mx=10)
	parameter(Ncode_mx=1000)
	parameter(Nelem_mx=10000)
	parameter(Ncf_mx=30)
	parameter(Nfig_mx=100000)
	parameter(Npatch_mx=1000)
	parameter(Nprobe_mx=100)
	parameter(Ntime_mx=100)
	parameter(Nchar_mx=1000)
