c     Copyright (C) 2023 |Meso|Star> (contact@meso-star.com)
      subroutine add_patch(dim,Nv01,Nf01,v01,f01,patch_index,Nv_patch,Nf_patch,v_patch,f_patch)
      implicit none
      include 'max.inc'
      include 'size_params.inc'
c     
c     Purpose: to add a patch to the list of patches
c     
c     Input:
c       + dim: dimension of space
c       + Nv01, Nf01, v01, f01: definition of patch
c       + patch_index: place in the list
c       + Nv_patch, Nf_patch, v_patch, f_patch: patch list
c     
c     Output:
c       + Nv_patch, Nf_patch, v_patch, f_patch: updated
c     
c     I/O
      integer dim
      integer Nv01,Nf01
      double precision v01(1:Nv_so_mx,1:Ndim_mx)
      integer f01(1:Nf_so_mx,1:Nvinface)
      integer patch_index
      integer Nv_patch(1:Npatch_mx)
      integer Nf_patch(1:Npatch_mx)
      double precision v_patch(1:Npatch_mx,1:Nv_so_mx,1:Ndim_mx)
      integer f_patch(1:Npatch_mx,1:Nf_so_mx,1:Nvinface)
c     temp
      integer ivertex,iface,j
c     label
      character*(Nchar_mx) label
      label='subroutine add_patch'

      if (patch_index.le.0) then
         call error(label)
         write(*,*) 'patch_index=',patch_index
         write(*,*) 'should be > 0'
         stop
      endif
      if (patch_index.gt.Npatch_mx) then
         call error(label)
         write(*,*) 'patch_index=',patch_index
         write(*,*) 'should be ≤ Npatch_mx=',Npatch_mx
         stop
      endif
      if (Nv01.le.0) then
         call error(label)
         write(*,*) 'Nv01=',Nv01
         write(*,*) 'should be > 0'
         stop
      endif
      if (Nv01.gt.Nv_so_mx) then
         call error(label)
         write(*,*) 'Nv01=',Nv01
         write(*,*) 'should be ≤ Nv_so_mx=',Nv_so_mx
         stop
      endif
      if (Nf01.le.0) then
         call error(label)
         write(*,*) 'Nf01=',Nf01
         write(*,*) 'should be > 0'
         stop
      endif
      if (Nf01.gt.Nf_so_mx) then
         call error(label)
         write(*,*) 'Nf01=',Nf01
         write(*,*) 'should be ≤ Nf_so_mx=',Nf_so_mx
         stop
      endif
      
      Nv_patch(patch_index)=Nv01
      do ivertex=1,Nv_patch(patch_index)
         do j=1,dim
            v_patch(patch_index,ivertex,j)=v01(ivertex,j)
         enddo                  ! j
      enddo                     ! ivertex
      Nf_patch(patch_index)=Nf01
      do iface=1,Nf_patch(patch_index)
         do j=1,Nvinface
            f_patch(patch_index,iface,j)=f01(iface,j)
         enddo                  ! j
      enddo                     ! iface

      return
      end


      
      subroutine get_patch(dim,Nv_patch,Nf_patch,v_patch,f_patch,patch_index,Nv01,Nf01,v01,f01)
      implicit none
      include 'max.inc'
      include 'size_params.inc'
c     
c     Purpose: to get a patch from the list of patches
c     
c     Input:
c       + dim: dimension of space
c       + Nv_patch, Nf_patch, v_patch, f_patch: patch list
c       + patch_index: index of the patch to get
c     
c     Output:
c       + Nv01, Nf01, v01, f01: definition of patch
c     
c     I/O
      integer dim
      integer Nv_patch(1:Npatch_mx)
      integer Nf_patch(1:Npatch_mx)
      double precision v_patch(1:Npatch_mx,1:Nv_so_mx,1:Ndim_mx)
      integer f_patch(1:Npatch_mx,1:Nf_so_mx,1:Nvinface)
      integer patch_index
      integer Nv01,Nf01
      double precision v01(1:Nv_so_mx,1:Ndim_mx)
      integer f01(1:Nf_so_mx,1:Nvinface)
c     temp
      integer ivertex,iface,j
c     label
      character*(Nchar_mx) label
      label='subroutine add_patch'

      if (patch_index.le.0) then
         call error(label)
         write(*,*) 'patch_index=',patch_index
         write(*,*) 'should be > 0'
         stop
      endif
      if (patch_index.gt.Npatch_mx) then
         call error(label)
         write(*,*) 'patch_index=',patch_index
         write(*,*) 'should be ≤ Npatch_mx=',Npatch_mx
         stop
      endif
      
      Nv01=Nv_patch(patch_index)
      if (Nv01.le.0) then
         call error(label)
         write(*,*) 'Nv01=',Nv01
         write(*,*) 'should be > 0'
         stop
      endif
      if (Nv01.gt.Nv_so_mx) then
         call error(label)
         write(*,*) 'Nv01=',Nv01
         write(*,*) 'should be ≤ Nv_so_mx=',Nv_so_mx
         stop
      endif
      do ivertex=1,Nv01
         do j=1,dim
            v01(ivertex,j)=v_patch(patch_index,ivertex,j)
         enddo                  ! j
      enddo                     ! ivertex
      Nf01=Nf_patch(patch_index)
      if (Nf01.le.0) then
         call error(label)
         write(*,*) 'Nf01=',Nf01
         write(*,*) 'should be > 0'
         stop
      endif
      if (Nf01.gt.Nf_so_mx) then
         call error(label)
         write(*,*) 'Nf01=',Nf01
         write(*,*) 'should be ≤ Nf_so_mx=',Nf_so_mx
         stop
      endif
      do iface=1,Nf01
         do j=1,Nvinface
            f01(iface,j)=f_patch(patch_index,iface,j)
         enddo                  ! j
      enddo                     ! iface

      return
      end
      
      

      subroutine identify_boundary_vertex_groups(dim,L,Nv,Nf,vertices,faces,node_is_on_edge,Nedge,edge_idx,dx,Nvg,vg_dim_side,vg_bbox,loops_only)
      implicit none
      include 'max.inc'
      include 'size_params.inc'
c     
c     Purpose: to identify groups of vertices that reside on the boundaries of the domain
c     
c     Input:
c       + dim: dimension of space
c       + L: typical length over each direction [m]
c       + Nv, Nf, vertices, faces: definition of the main surface OBJ
c       + node_is_on_edge: T if node is on a edge of the domain
c       + Nedge: number of edges checked (1-3)
c       + egde_idx: edge index
c         - edge_idx(ivertex,iedge,1)=1-3 (direction)
c         - edge_idx(ivertex,iedge,2)=1 or 2 (negative / positive side)
c       + dx: size of discretization volumes
c     
c     Output:
c       + Nvg: number of vertices groups
c       + vg_dim_side: dimension and side for each vertices group
c       + vg_bbox: bounding box for each vertices group
c       + loops_only: T when side groups of vertices are all on a loop
c     
c     I/O
      integer dim
      double precision L(1:Ndim_mx)
      integer Nv,Nf
      double precision vertices(1:Nv_mx,1:Ndim_mx)
      integer faces(1:Nf_mx,1:Nvinface)
      logical node_is_on_edge(1:Nv_mx)
      integer Nedge(1:Nv_mx)
      integer edge_idx(1:Nv_mx,1:Ndim_mx,1:2)
      double precision dx(1:Ndim_mx)
      integer Nvg
      integer vg_dim_side(1:Ngroup_mx,1:2)
      double precision vg_bbox(1:Ngroup_mx,1:Ndim_mx,1:2)
      logical loops_only
c     temp
      logical debug
      integer idim,jdim,iside,jside,ivertex,jvertex,iedge,i,j,igroup,jgroup,kgroup
      integer Nv_on_boundary
      integer vidx(1:Nv_on_boundary_mx)
      logical is_on_boundary
      double precision d,dmax,dmax2
      integer Ngroup
      integer Nv_in_group(1:Ngroup_mx)
      integer v_in_group(1:Ngroup_mx,1:Nv_in_group_mx)
      logical add2group,create_new_group
      integer group_idx
      double precision P0(1:Ndim_mx)
      double precision P1(1:Ndim_mx)
      double precision bbox(1:Ngroup_mx,1:Ndim_mx,1:2)
      logical keep_going,fusion
      integer dim1,dim2
      logical dim1_found,dim2_found
      double precision box1(1:2,1:2)
      double precision box2(1:2,1:2)
      double precision xmin,xmax,ymin,ymax
      integer Nv0
      integer group_vidx(1:Nv_so_mx)
      logical loop,duplicate
      double precision vgroup(1:Nv_so_mx,1:Ndim_mx)
c     functions
      logical is_even
c     parameters
      double precision epsilon
      parameter(epsilon=1.0D-8)
c     label
      character*(Nchar_mx) label
      label='subroutine identify_boundary_vertex_groups'

      Nvg=0
      do idim=1,dim
         dmax2=0.0D+0
         do jdim=1,dim
            if (jdim.ne.idim) then
               dmax2=dmax2+dx(jdim)**2.0D+0
            endif
         enddo                  ! jdim
         dmax=1.20D+0*dsqrt(dmax2)
c     Identify indexes of other dimension
         dim1_found=.false.
         dim2_found=.false.
         do j=1,dim
            if (j.ne.idim) then
               if ((.not.dim1_found).and.(.not.dim2_found)) then
                  dim1=j
                  dim1_found=.true.
                  goto 123
               endif
               if ((dim1_found).and.(.not.dim2_found)) then
                  dim2=j
                  dim2_found=.true.
                  goto 123
               endif
            endif               ! j.ne.idim
 123        continue
         enddo                  ! j
         do iside=1,2
            Nv_on_boundary=0
c     Debug
c            if ((idim.eq.1).and.(iside.eq.1)) then
c               debug=.true.
c            else
            debug=.false.
c            endif
c     Debug
c     Debug
c            if (debug) then
c               write(*,*) 'dim1=',dim1,' dim2=',dim2
c            endif
c     Debug
c     Identify all nodes that belong to the boundary for (idim, iside)
            do ivertex=1,Nv
               if (node_is_on_edge(ivertex)) then
                  is_on_boundary=.false.
                  do iedge=1,Nedge(ivertex)
                     if ((edge_idx(ivertex,iedge,1).eq.idim).and.(edge_idx(ivertex,iedge,2).eq.iside)) then
                        is_on_boundary=.true.
                        goto 111
                     endif
                  enddo         ! iedge
 111              continue
                  if (is_on_boundary) then
                     Nv_on_boundary=Nv_on_boundary+1
                     if (Nv_on_boundary.gt.Nv_on_boundary_mx) then
                        call error(label)
                        write(*,*) 'Nv_on_boundary_mx has been reached'
                        stop
                     endif
                     vidx(Nv_on_boundary)=ivertex
                  endif         ! is_on_boundary
               endif            ! node_is_on_edge(ivertex)
            enddo               ! ivertex
c     Now we have a list of Nv_on_boundary vertices index identified as belonging to the current face of the domain
c     Debug
            if (debug) then
               open(11,file='./results/v_on_boundary.txt')
               do ivertex=1,Nv_on_boundary
                  write(11,*) vertices(vidx(ivertex),dim1),vertices(vidx(ivertex),dim2)
               enddo            ! ivertex
               close(11)
            endif
c     Debug
c     Sort vertices into groups using a proximity criterion
            Ngroup=0
            do ivertex=1,Nv_on_boundary
               add2group=.false.
               create_new_group=.false.
               if (ivertex.eq.1) then
c     Add first vertex in a first group
                  create_new_group=.true.
               else
                  do j=1,dim
                     P0(j)=vertices(vidx(ivertex),j)
                  enddo         ! j
                  do igroup=1,Ngroup
                     do jvertex=1,Nv_in_group(igroup)
                        do j=1,dim
                           P1(j)=vertices(v_in_group(igroup,jvertex),j)
                        enddo   ! j
                        call distance(dim,P0,P1,d)
                        if (d.le.dmax) then
                           add2group=.true.
                           group_idx=igroup
                           goto 222
                        endif   ! d<dmax
                     enddo      ! jvertex
                  enddo         ! igroup
 222              continue
                  if (.not.add2group) then
                     create_new_group=.true.
                  endif         ! add2group=F
               endif            ! ivertex=1 or not
               if ((.not.add2group).and.(.not.create_new_group)) then
                  call error(label)
                  write(*,*) 'vertex index:',ivertex
                  write(*,*) 'is not added to a existing group, and no new group required'
                  stop
               endif
               if ((add2group).and.(create_new_group)) then
                  call error(label)
                  write(*,*) 'vertex index:',ivertex
                  write(*,*) 'both added to a existing group, and new group required'
                  stop
               endif
               if (add2group) then
c     Debug
c                  write(*,*) 'ivertex:',ivertex,' must be added to group:',group_idx
c     Debug
c     Increase number of vertex in the group
                  Nv_in_group(group_idx)=Nv_in_group(group_idx)+1
                  if (Nv_in_group(group_idx).gt.Nv_in_group_mx) then
                     call error(label)
                     write(*,*) 'Nv_in_group_mx has been reached for group_idx=',group_idx
                     stop
                  endif
c     Add vertex to the group
                  v_in_group(group_idx,Nv_in_group(group_idx))=vidx(ivertex)
c     Update bounding box
                  do jdim=1,dim
                     if (jdim.ne.idim) then
                        if (vertices(vidx(ivertex),jdim).lt.bbox(group_idx,jdim,1)) then
                           bbox(group_idx,jdim,1)=vertices(vidx(ivertex),jdim)
                        endif
                        if (vertices(vidx(ivertex),jdim).gt.bbox(group_idx,jdim,2)) then
                           bbox(group_idx,jdim,2)=vertices(vidx(ivertex),jdim)
                        endif
                     endif      ! jdim.ne.idim
                  enddo         ! jdim
c     
               endif            ! add2group
               if (create_new_group) then
                  Ngroup=Ngroup+1
                  if (Ngroup.gt.Ngroup_mx) then
                     call error(label)
                     write(*,*) 'Ngroup_mx has been reached'
                     stop
                  endif
c     Debug
c                  write(*,*) 'ivertex:',ivertex,' create new group, group index:',Ngroup
c     Debug
c     create group
                  Nv_in_group(Ngroup)=1
c     add a first vertex to the group
                  v_in_group(Ngroup,Nv_in_group(Ngroup))=vidx(ivertex)
c     initialize bounding box
                  do jdim=1,dim
                     do jside=1,2
                        bbox(Ngroup,jdim,jside)=vertices(vidx(ivertex),jdim)
                     enddo      ! jside
                  enddo         ! jdim
c     
               endif            ! create_new_group
            enddo               ! ivertex
c     thicken bounding boxes
            do igroup=1,Ngroup
               do jdim=1,dim
                  if (jdim.eq.idim) then
                     if (iside.eq.1) then
                        bbox(igroup,jdim,1)=-L(idim)*1.10D+0
                        bbox(igroup,jdim,2)=-L(idim)*0.90D+0
                     else
                        bbox(igroup,jdim,1)=L(idim)*0.90D+0
                        bbox(igroup,jdim,2)=L(idim)*1.10D+0
                     endif
                  else          ! jdim.ne.idim
                     bbox(igroup,jdim,1)=bbox(igroup,jdim,1)-dmax
                     bbox(igroup,jdim,2)=bbox(igroup,jdim,2)+dmax
                  endif         ! jdim=idim
               enddo            ! jdim
            enddo               ! igroup
c     Debug
            if (debug) then
               open(12,file='./results/bounding_boxes1.txt')
               do igroup=1,Ngroup
                  write(12,*) bbox(igroup,dim1,1),bbox(igroup,dim2,1)
                  write(12,*) bbox(igroup,dim1,2),bbox(igroup,dim2,1)
                  write(12,*) bbox(igroup,dim1,2),bbox(igroup,dim2,2)
                  write(12,*) bbox(igroup,dim1,1),bbox(igroup,dim2,2)
                  write(12,*) bbox(igroup,dim1,1),bbox(igroup,dim2,1)
                  write(12,*)
               enddo            ! igroop
               close(12)
            endif
c     Debug
c     
c     Fusion of groups
            keep_going=.true.
            igroup=1
            jgroup=2
            do while (keep_going)
               do j=1,2
                  box1(1,j)=bbox(igroup,dim1,j)
                  box1(2,j)=bbox(igroup,dim2,j)
                  box2(1,j)=bbox(jgroup,dim1,j)
                  box2(2,j)=bbox(jgroup,dim2,j)
               enddo            ! j
               call is_fusion_required(dim,box1,box2,fusion)
c     Debug
c               write(*,*) 'fusion required between groups:',igroup,jgroup
c               write(*,*) 'Nv_in_group(',igroup,')=',nv_in_group(igroup)
c               write(*,*) 'Nv_in_group(',jgroup,')=',nv_in_group(jgroup)
c     Debug
               if (fusion) then
c     Adjust bounding box for group "igroup"
                  xmin=dmin1(box1(1,1),box2(1,1))
                  xmax=dmax1(box1(1,2),box2(1,2))
                  ymin=dmin1(box1(2,1),box2(2,1))
                  ymax=dmax1(box1(2,2),box2(2,2))
                  bbox(igroup,dim1,1)=xmin
                  bbox(igroup,dim1,2)=xmax
                  bbox(igroup,dim2,1)=ymin
                  bbox(igroup,dim2,2)=ymax
c     Put vertices of jgroup into igroup
                  Nv0=Nv_in_group(igroup)
                  Nv_in_group(igroup)=Nv_in_group(igroup)+Nv_in_group(jgroup)
                  if (Nv_in_group(igroup).gt.Nv_in_group_mx) then
                     call error(label)
                     write(*,*) 'Nv_in_group_mx has been reached for group_idx=',igroup
                     stop
                  endif
                  do i=1,Nv_in_group(jgroup)
                     v_in_group(igroup,Nv0+i)=v_in_group(jgroup,i)
                  enddo         ! i
c     Remove group "jgroup"
                  if (jgroup.lt.Ngroup) then
                     call remove_group(dim,Ngroup,Nv_in_group,v_in_group,bbox,jgroup) ! Ngroup is decreased by the routine
                  else
                     Ngroup=Ngroup-1 ! must be done also when jgroup=Ngroup
                  endif
               else             ! fusion=F
                  jgroup=jgroup+1
                  if (jgroup.eq.igroup) then
                     jgroup=jgroup+1
                  endif
               endif            ! fusion
               if (jgroup.gt.Ngroup) then
                  if (igroup.lt.Ngroup) then
                     igroup=igroup+1
                     jgroup=1
                  else
                     keep_going=.false.
                  endif
               endif            ! jgroup < Ngroup
            enddo               ! while (keep_going)
c     remove groups of 1 vertex
            do igroup=1,Ngroup
               if (Nv_in_group(igroup).le.1) then
                  call remove_group(dim,Ngroup,Nv_in_group,v_in_group,bbox,igroup)
               endif
            enddo               ! igroup
c     
c     Debug
            if (debug) then
               open(12,file='./results/bounding_boxes.txt')
               do igroup=1,Ngroup
                  write(12,*) bbox(igroup,dim1,1),bbox(igroup,dim2,1)
                  write(12,*) bbox(igroup,dim1,2),bbox(igroup,dim2,1)
                  write(12,*) bbox(igroup,dim1,2),bbox(igroup,dim2,2)
                  write(12,*) bbox(igroup,dim1,1),bbox(igroup,dim2,2)
                  write(12,*) bbox(igroup,dim1,1),bbox(igroup,dim2,1)
                  write(12,*)
               enddo            ! igroop
               close(12)
            endif
c     Debug
c     Add every group to the "vg" lists
            loops_only=.true.
            do igroup=1,Ngroup
c     Debug
c               if ((idim.eq.1).and.(iside.eq.2).and.(igroup.eq.1)) then
c                  debug=.true.
c               else
               debug=.false.
c               endif
c     Debug
c     Debug
c               write(*,*) 'idim=',idim,' iside=',iside,' igroup=',igroup
c     Debug
               Nvg=Nvg+1
               if (Nvg.gt.Ngroup_mx) then
                  call error(label)
                  write(*,*) 'Ngroup_mx has been reached'
                  stop
               endif
               vg_dim_side(Nvg,1)=idim
               vg_dim_side(Nvg,2)=iside
               do jdim=1,dim
                  do j=1,2
                     vg_bbox(Nvg,jdim,j)=bbox(igroup,jdim,j)
                  enddo         ! j
               enddo            ! jdim
c     Debug
c               if (debug) then
c                  write(*,*) 'group index:',igroup
c                  write(*,*) 'Nv_in_group(',igroup,')=',Nv_in_group(igroup)
c                  do i=1,Nv_in_group(igroup)
c                     write(*,*) 'v_in_group(',igroup,',',i,')=',v_in_group(igroup,i)
c                  enddo         ! i
c               endif
c     Debug
c     Detect duplicate groups
c     Debug
               if (debug) then
                  write(*,*) 'is_even(',Nv_in_group(igroup),')=',is_even(Nv_in_group(igroup))
               endif
c     Debug
               if (is_even(Nv_in_group(igroup))) then
                  duplicate=.true.
                  do i=1,Nv_in_group(igroup)/2
                     do j=1,dim
                        P0(j)=vertices(v_in_group(igroup,i),j)
                        P1(j)=vertices(v_in_group(igroup,Nv_in_group(igroup)/2+i),j)
                     enddo      ! i
                     call distance(dim,P0,P1,d)
c     Debug
                     if (debug) then
                        write(*,*) 'P0=',P0
                        write(*,*) 'P1=',P1
                        write(*,*) 'Distance between points ',i,' and ',Nv_in_group(igroup)/2+i,':',d
                     endif
c     Debug
                     if (d.gt.epsilon) then
                        duplicate=.false.
                        goto 124
                     endif
                  enddo         ! i
 124              continue
                  if (duplicate) then
                     Nv_in_group(igroup)=Nv_in_group(igroup)/2
c     Debug
                     if (debug) then
                        write(*,*) 'duplicate group found and corrected'
                     endif
c     Debug
                  endif
               endif            ! is_even(Nv_in_group(igroup))
               
               do i=1,Nv_in_group(igroup)
                  group_vidx(i)=v_in_group(igroup,i)
               enddo            ! i
               call analyze_group(debug,dim,Nv,vertices,Nv_in_group(igroup),group_vidx,loop,vgroup)
c     Debug
               if (debug) then
                  write(*,*) 'loop=',loop
                  stop
               endif
c     Debug
               if (.not.loop) then
                  loops_only=.false.
               endif
c     
            enddo               ! igroup
         enddo                  ! iside
      enddo                     ! idim
      
      return
      end



      subroutine remove_group(dim,Ngroup,Nv_in_group,v_in_group,bbox,igroup)
      implicit none
      include 'max.inc'
c     
c     Purpose: to remove a group; this will only remove all data associated to
c     a given group; this information will not be propagated into other groups,
c     meaning vertices and bounding box are lost
c     
c     Input:
c       + dim: dimension of space
c       + Ngroup: number of groups
c       + Nv_in_group: number of vertices into each group
c       + v_in_group: indexes of the Nv_in_group vertices in each group
c       + bbox: bounding box for each group
c       + igroup: index of the group to remove
c     
c     Outut:
c       + Ngroup, Nv_in_group, v_in_group, bbox: updated
c     
c     I/O
      integer dim
      integer Ngroup
      integer Nv_in_group(1:Ngroup_mx)
      integer v_in_group(1:Ngroup_mx,1:Nv_in_group_mx)
      double precision bbox(1:Ngroup_mx,1:Ndim_mx,1:2)
      integer igroup
c     temp
      integer jgroup,i,j,jdim
c     label
      character*(Nchar_mx) label
      label='subroutine remove_group'

c     Remove group "igroup"
      if (jgroup.lt.Ngroup) then
         do jgroup=igroup+1,Ngroup
            Nv_in_group(jgroup-1)=Nv_in_group(jgroup)
            do i=1,Nv_in_group(jgroup-1)
               v_in_group(jgroup-1,i)=v_in_group(jgroup,i)
            enddo               ! i
            do jdim=1,dim
               do j=1,2
                  bbox(jgroup-1,jdim,j)=bbox(jgroup,jdim,j)
               enddo            ! j
            enddo               ! jdim
         enddo                  ! jgroup
      endif                     ! jgroup < Ngroup
      Ngroup=Ngroup-1

      return
      end


      
      subroutine is_fusion_required(dim,box1,box2,fusion)
      implicit none
      include 'max.inc'
c     
c     Purpose: to determine whether or not a fusion is required between two bounding boxes
c     
c     Input:
c       + dim: dimension of space
c       + box1: bounding box 1
c       + box2: bounding box 2
c     
c     Output:
c       + fusion: T if fusion is required
c     
c     I/O
      integer dim
      double precision box1(1:2,1:2)
      double precision box2(1:2,1:2)
      logical fusion
c     temp
      double precision P1(1:2)
      double precision P2(1:2)
      double precision P3(1:2)
      double precision P4(1:2)
      logical P1_in_box1
      logical P2_in_box1
      logical P3_in_box1
      logical P4_in_box1
      logical P1_in_box2
      logical P2_in_box2
      logical P3_in_box2
      logical P4_in_box2
c     label
      character*(Nchar_mx) label
      label='subroutine is_fusion_required'

      fusion=.false.
c     P1, P2, P3 and P4 are the 4 corners of box2
      P1(1)=box2(1,1)
      P1(2)=box2(2,1)
      P2(1)=box2(1,2)
      P2(2)=box2(2,1)
      P3(1)=box2(1,2)
      P3(2)=box2(2,2)
      P4(1)=box2(1,1)
      P4(2)=box2(2,2)
      call is_P_in_box(dim,P1,box1,P1_in_box1)
      call is_P_in_box(dim,P2,box1,P2_in_box1)
      call is_P_in_box(dim,P3,box1,P3_in_box1)
      call is_P_in_box(dim,P4,box1,P4_in_box1)
      if ((P1_in_box1).or.(P2_in_box1).or.(P3_in_box1).or.(P4_in_box1)) then
         fusion=.true.
         goto 123
      endif
c     P1, P2, P3 and P4 are the 4 corners of box1
      P1(1)=box1(1,1)
      P1(2)=box1(2,1)
      P2(1)=box1(1,2)
      P2(2)=box1(2,1)
      P3(1)=box1(1,2)
      P3(2)=box1(2,2)
      P4(1)=box1(1,1)
      P4(2)=box1(2,2)
      call is_P_in_box(dim,P1,box2,P1_in_box2)
      call is_P_in_box(dim,P2,box2,P2_in_box2)
      call is_P_in_box(dim,P3,box2,P3_in_box2)
      call is_P_in_box(dim,P4,box2,P4_in_box2)
      if ((P1_in_box2).or.(P2_in_box2).or.(P3_in_box2).or.(P4_in_box2)) then
         fusion=.true.
         goto 123
      endif
c     
 123  continue
      return
      end


      
      subroutine is_P_in_box(dim,P,box,is_inside)
      implicit none
      include 'max.inc'
c     
c     Purpose: to check whether or not a given position is inside a box
c     
c     Input:
c       + dim: dimension of space
c       + P: position
c       + box: box
c     
c     Output:
c       + is_inside: T if P is in box
c     
c     I/O
      integer dim
      double precision P(1:2)
      double precision box(1:2,1:2)
      logical is_inside
c     temp
c     label
      character*(Nchar_mx) label
      label='subroutine is_P_in_box'

      if ((P(1).ge.box(1,1)).and.(P(1).le.box(1,2)).and.(P(2).ge.box(2,1)).and.(P(2).le.box(2,2))) then
         is_inside=.true.
      else
         is_inside=.false.
      endif

      return
      end
