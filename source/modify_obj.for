c     Copyright (C) 2023 |Meso|Star> (contact@meso-star.com)
      subroutine copy_obj(dim,Nv01,Nf01,v01,f01,Nv02,Nf02,v02,f02)
      implicit none
      include 'max.inc'
      include 'size_params.inc'
c
c     Purpose: to duplicate a object
c
c     Input:
c       + dim: dimension of space
c       + Nv01, Nf01, v01, f01: object to duplicate
c
c     Output:
c       + Nv02, Nf02, v02, f02: duplicated object
c
c     I/O
      integer dim
      integer Nv01
      integer Nf01
      double precision v01(1:Nv_mx,1:Ndim_mx)
      integer f01(1:Nf_mx,1:Nvinface)
      integer Nv02
      integer Nf02
      double precision v02(1:Nv_mx,1:Ndim_mx)
      integer f02(1:Nf_mx,1:Nvinface)
c     temp
      integer iface,ivertex,j
c     label
      character*(Nchar_mx) label
      label='subroutine copy_obj'

      Nv02=Nv01
      do ivertex=1,Nv01
         do j=1,dim
            v02(ivertex,j)=v01(ivertex,j)
         enddo                  ! j
      enddo                     ! ivertex
      Nf02=Nf01
      do iface=1,Nf01
         do j=1,Nvinface
            f02(iface,j)=f01(iface,j)
         enddo                  ! j
      enddo                     ! iface

      return
      end


      
      subroutine copy_sobj(dim,Nv01,Nf01,v01,f01,Nv02,Nf02,v02,f02)
      implicit none
      include 'max.inc'
      include 'size_params.inc'
c
c     Purpose: to duplicate a single object
c
c     Input:
c       + dim: dimension of space
c       + Nv01, Nf01, v01, f01: object to duplicate
c
c     Output:
c       + Nv02, Nf02, v02, f02: duplicated object
c
c     I/O
      integer dim
      integer Nv01
      integer Nf01
      double precision v01(1:Nv_so_mx,1:Ndim_mx)
      integer f01(1:Nf_so_mx,1:Nvinface)
      integer Nv02
      integer Nf02
      double precision v02(1:Nv_so_mx,1:Ndim_mx)
      integer f02(1:Nf_so_mx,1:Nvinface)
c     temp
      integer iface,ivertex,j
c     label
      character*(Nchar_mx) label
      label='subroutine copy_sobj'

      Nv02=Nv01
      do ivertex=1,Nv01
         do j=1,dim
            v02(ivertex,j)=v01(ivertex,j)
         enddo                  ! j
      enddo                     ! ivertex
      Nf02=Nf01
      do iface=1,Nf01
         do j=1,Nvinface
            f02(iface,j)=f01(iface,j)
         enddo                  ! j
      enddo                     ! iface

      return
      end


      
      subroutine check_vertex_indexes(Nf_so,f_so)
      implicit none
      include 'max.inc'
      include 'size_params.inc'
c
c     Purpose: to check for inconsistencies in the vertex indexes
c     for each face
c
c     Input:
c       + Nf_so: number of faces
c       + f_so: faces
c
c     Output:
c       + f_so: updated
c
c     I/O
      integer Nf_so
      integer f_so(1:Nf_so_mx,1:Nvinface)
c     temp
      integer if
c     label
      character*(Nchar_mx) label
      label='subroutine check_vertex_indexes'

      do if=1,Nf_so
         if (f_so(if,1).eq.f_so(if,2)) then
            call error(label)
            write(*,*) 'f_so(',if,',1)=',f_so(if,1)
            write(*,*) 'f_so(',if,',2)=',f_so(if,2)
            stop
         endif
         if (f_so(if,1).eq.f_so(if,3)) then
            call error(label)
            write(*,*) 'f_so(',if,',1)=',f_so(if,1)
            write(*,*) 'f_so(',if,',3)=',f_so(if,3)
            stop
         endif
         if (f_so(if,2).eq.f_so(if,3)) then
            call error(label)
            write(*,*) 'f_so(',if,',2)=',f_so(if,2)
            write(*,*) 'f_so(',if,',3)=',f_so(if,3)
            stop
         endif
      enddo ! i

      return
      end


      
      subroutine rotate_sobj_complete(dim,Nv_so,v_so,rotation_center,rotation_axis,rotation_angle)
      implicit none
      include 'max.inc'
c
c     Purpose: to rotate an object
c
c     Input:
c       + dim: dimension of space
c       + Nv_so: number of vertices
c       + v_so: vertices
c       + rotation_center: position used as the rotation center
c       + rotation_axis: rotation axis
c       + rotation_angle: rotation angle [rad]
c
c     Output:
c       + v_so: updated
c
c     I/O
      integer dim
      integer Nv_so
      double precision v_so(1:Nv_so_mx,1:Ndim_mx)
      double precision rotation_center(1:Ndim_mx)
      double precision rotation_axis(1:Ndim_mx)
      double precision rotation_angle
c     temp
      integer iv,i
      double precision x1(1:Ndim_mx)
      double precision x2(1:Ndim_mx)
c     label
      character*(Nchar_mx) label
      label='subroutine rotate_sobj_complete'

      do iv=1,Nv_so
         do i=1,dim
            x1(i)=v_so(iv,i)
         enddo                  ! i
         call rotation(dim,x1,rotation_angle,rotation_center,rotation_axis,x2)
         do i=1,dim
            v_so(iv,i)=x2(i)
         enddo ! i
      enddo ! iv

      return
      end


      
      subroutine rotate_sobj(dim,Nv_so,v_so,M)
      implicit none
      include 'max.inc'
c
c     Purpose: to rotate an object
c
c     Input:
c       + dim: dimension of space
c       + Nv_so: number of vertices
c       + v_so: vertices
c       + M: rotation matrix
c
c     Output:
c       + v_so: updated
c
c     I/O
      integer dim
      integer Nv_so
      double precision v_so(1:Nv_so_mx,1:Ndim_mx)
      double precision M(1:Ndim_mx,1:Ndim_mx)
c     temp
      integer iv,i
      double precision x1(1:Ndim_mx)
      double precision x2(1:Ndim_mx)
c     label
      character*(Nchar_mx) label
      label='subroutine rotate_sobj'

      do iv=1,Nv_so
         do i=1,dim
            x1(i)=v_so(iv,i)
         enddo ! i
         call matrix_vector(dim,M,x1,x2)
         do i=1,dim
            v_so(iv,i)=x2(i)
         enddo ! i
      enddo ! iv

      return
      end



      subroutine move_obj(dim,Nv_so,v_so,center)
      implicit none
      include 'max.inc'
c
c     Purpose: to move an object
c
c     Input:
c       + dim: dimension of space
c       + Nv_so: number of vertices
c       + v_so: vertices
c       + center: cartesian coordinates of the new center
c
c     Output:
c       + v_so: updated
c
c     I/O
      integer dim
      integer Nv_so
      double precision v_so(1:Nv_so_mx,1:Ndim_mx)
      double precision center(1:Ndim_mx)
c     temp
      integer iv,i
c     label
      character*(Nchar_mx) label
      label='subroutine move_obj'

      do iv=1,Nv_so
         do i=1,dim
            v_so(iv,i)=v_so(iv,i)+center(i)
         enddo ! i
      enddo ! iv

      return
      end

      

      subroutine translate_sobj(dim,Nv_so,v_so,displacement)
      implicit none
      include 'max.inc'
c
c     Purpose: to rotate an object
c
c     Input:
c       + dim: dimension of space
c       + Nv_so: number of vertices
c       + v_so: vertices
c       + displacement: translation vector
c
c     Output:
c       + v_so: updated
c
c     I/O
      integer dim
      integer Nv_so
      double precision v_so(1:Nv_so_mx,1:Ndim_mx)
      double precision displacement(1:Ndim_mx)
c     temp
      integer iv,i
      double precision x1(1:Ndim_mx)
      double precision x2(1:Ndim_mx)
c     label
      character*(Nchar_mx) label
      label='subroutine translate_sobj'

      do iv=1,Nv_so
         do i=1,dim
            x1(i)=v_so(iv,i)
         enddo                  ! i
         call add_vectors(dim,x1,displacement,x2)
         do i=1,dim
            v_so(iv,i)=x2(i)
         enddo ! i
      enddo ! iv

      return
      end

      

      subroutine scale_obj(dim,Nv,v,scale)
      implicit none
      include 'max.inc'
c
c     Purpose: to scale an object
c
c     Input:
c       + dim: dimension of space
c       + Nv: number of vertices
c       + v: vertices
c       + scale: scaling ratio
c
c     Output:
c       + v: updated
c
c     I/O
      integer dim
      integer Nv
      double precision v(1:Nv_mx,1:Ndim_mx)
      double precision scale
c     temp
      integer iv,i
c     label
      character*(Nchar_mx) label
      label='subroutine scale_obj'

      if (scale.le.0.0D+0) then
         call error(label)
         write(*,*) 'scale=',scale
         write(*,*) 'should be positive'
         stop
      endif

      do iv=1,Nv
         do i=1,dim
            v(iv,i)=v(iv,i)*scale
         enddo ! i
      enddo ! iv

      return
      end



      subroutine scale_sobj(dim,Nv_so,v_so,scale)
      implicit none
      include 'max.inc'
c
c     Purpose: to scale an object
c
c     Input:
c       + dim: dimension of space
c       + Nv_so: number of vertices
c       + v_so: vertices
c       + scale: scaling ratio
c
c     Output:
c       + v_so: updated
c
c     I/O
      integer dim
      integer Nv_so
      double precision v_so(1:Nv_so_mx,1:Ndim_mx)
      double precision scale
c     temp
      integer iv,i
c     label
      character*(Nchar_mx) label
      label='subroutine scale_obj'

      if (scale.le.0.0D+0) then
         call error(label)
         write(*,*) 'scale=',scale
         write(*,*) 'should be positive'
         stop
      endif

      do iv=1,Nv_so
         do i=1,dim
            v_so(iv,i)=v_so(iv,i)*scale
         enddo ! i
      enddo ! iv

      return
      end


      
      subroutine invert_normals_so(Nf,f01)
      implicit none
      include 'max.inc'
      include 'size_params.inc'
c     
c     Purpose: invert the normal for all faces of a given single object
c     
c     Input:
c       + Nf: number of faces of the object
c       + f01: definition of faces
c     
c     Output:
c       + f01: updated
c     
c     I/O
      integer Nf
      integer f01(1:Nf_so_mx,1:Nvinface)
c     temp
      integer iface,tmp1
c     label
      character*(Nchar_mx) label
      label='subroutine invert_normals_so'

      do iface=1,Nf
         tmp1=f01(iface,2)
         f01(iface,2)=f01(iface,3)
         f01(iface,3)=tmp1
      enddo                     ! iface

      return
      end


      
      subroutine invert_normals(Nf,f01)
      implicit none
      include 'max.inc'
      include 'size_params.inc'
c     
c     Purpose: invert the normal for all faces of a given object
c     
c     Input:
c       + Nf: number of faces of the object
c       + f01: definition of faces
c     
c     Output:
c       + f01: updated
c     
c     I/O
      integer Nf
      integer f01(1:Nf_mx,1:Nvinface)
c     temp
      integer iface,tmp1
c     label
      character*(Nchar_mx) label
      label='subroutine invert_normals'

      do iface=1,Nf
         tmp1=f01(iface,2)
         f01(iface,2)=f01(iface,3)
         f01(iface,3)=tmp1
      enddo                     ! iface

      return
      end



      subroutine remove_face_list_from_object(dim,Nf,faces,material_index,Nf_tr,f_tr)
      implicit none
      include 'max.inc'
      include 'size_params.inc'
c     
c     Purpose: to remove a list of triangular faces from a object
c     
c     Input:
c       + dim: dimension of space
c       + Nf, faces, material_index: definition of the object
c       + Nf_tr,f_tr: list of faces to remove
c     
c     Output:
c       + Nv, Nf, vertices, faces: updated
c     
c     I/O
      integer dim
      integer Nf
      integer faces(1:Nf_mx,1:Nvinface)
      integer material_index(1:Nf_mx)
      integer Nf_tr
      integer f_tr(1:Nf_so_mx)
c     temp
      logical face_sorted(1:Nf_mx)
      integer f_tr_sorted(1:Nf_mx)
      integer iface,face_index,i,j,iface_max,idx_face_max
c     label
      character*(Nchar_mx) label
      label='subroutine remove_face_list_from_object'

c     Sort by descending order
      do i=1,Nf_tr
         face_sorted(i)=.false.
      enddo                     ! iface
      do i=1,Nf_tr
         iface_max=0
         do iface=1,Nf_tr
            if ((f_tr(iface).gt.iface_max).and.(.not.face_sorted(iface))) then
               idx_face_max=iface
               iface_max=f_tr(iface)
            endif
         enddo                  ! iface
         if (iface_max.eq.0) then
            call error(label)
            write(*,*) 'iface_max=',iface_max
            stop
         endif
         f_tr_sorted(i)=iface_max
         face_sorted(idx_face_max)=.true.
      enddo                     ! i
c     Debug
c      do i=1,Nf_tr
c         write(*,*) 'f_tr_sorted(',i,')=',f_tr_sorted(i)
c      enddo                     ! i
c     Debug

      do i=1,Nf_tr
         face_index=f_tr_sorted(i)
         do iface=face_index,Nf-1
            do j=1,Nvinface
               faces(iface,j)=faces(iface+1,j)
            enddo               ! j
            material_index(iface)=material_index(iface+1)
         enddo                  ! iface
         Nf=Nf-1
      enddo                     ! i

      return
      end
      

      
      subroutine add_face_to_object(dim,Nv,Nf,vertices,faces,x1,x2,x3)
      implicit none
      include 'max.inc'
      include 'size_params.inc'
c     
c     Purpose: to add a triangular face to a object
c     
c     Input:
c       + dim: dimension of space
c       + Nv, Nf, vertices, faces: definition of the object
c       + x1, x2, x3: cartesian coordinates of the 3 nodes that define the face to add
c     
c     Output:
c       + Nv, Nf, vertices, faces: updated
c     
c     I/O
      integer dim
      integer Nv,Nf
      double precision vertices(1:Nv_so_mx,1:Ndim_mx)
      integer faces(1:Nf_so_mx,1:Nvinface)
      double precision x1(1:Ndim_mx)
      double precision x2(1:Ndim_mx)
      double precision x3(1:Ndim_mx)
c     temp
      integer iv,iface,j
      double precision d1,d2,d3
      double precision x(1:Ndim_mx)
      logical identical_vertex_found
c     parameters
      double precision d_min
      parameter(d_min=1.0D-3)   ! [m]
c     label
      character*(Nchar_mx) label
      label='subroutine add_face_to_object'
c
      if (Nv.eq.0) then
         Nv=1
         do j=1,dim
            vertices(1,j)=x1(j)
         enddo                  ! j
      endif
c     
      Nf=Nf+1
      if (Nf.gt.Nf_so_mx) then
         call error(label)
         write(*,*) 'Nf_so_mx has been reached'
         stop
      endif
c
c     x1
      identical_vertex_found=.false.
      do iv=1,Nv
         do j=1,dim
            x(j)=vertices(iv,j)
         enddo                  ! j
         call distance(dim,x1,x,d1)
         if (d1.le.d_min) then  ! point x1 already exists in the "vertices" list
            faces(Nf,1)=iv
            identical_vertex_found=.true.
            goto 111
         endif
      enddo                     ! iv
 111  continue
      if (.not.identical_vertex_found) then
         Nv=Nv+1
         if (Nv.gt.Nv_so_mx) then
            call error(label)
            write(*,*) 'Nv_so_mx has been reached'
            stop
         endif
         do j=1,dim
            vertices(Nv,j)=x1(j)
         enddo                  ! j
         faces(Nf,1)=Nv
      endif
c     x2
      identical_vertex_found=.false.
      do iv=1,Nv
         do j=1,dim
            x(j)=vertices(iv,j)
         enddo                  ! j
         call distance(dim,x2,x,d2)
         if (d2.le.d_min) then  ! point x2 already exists in the "vertices" list
            faces(Nf,2)=iv
            identical_vertex_found=.true.
            goto 112
         endif
      enddo                     ! iv
 112  continue
      if (.not.identical_vertex_found) then
         Nv=Nv+1
         if (Nv.gt.Nv_so_mx) then
            call error(label)
            write(*,*) 'Nv_so_mx has been reached'
            stop
         endif
         do j=1,dim
            vertices(Nv,j)=x2(j)
         enddo                  ! j
         faces(Nf,2)=Nv
      endif
c     x3
      identical_vertex_found=.false.
      do iv=1,Nv
         do j=1,dim
            x(j)=vertices(iv,j)
         enddo                  ! j
         call distance(dim,x3,x,d3)
         if (d3.le.d_min) then  ! point x2 already exists in the "vertices" list
            faces(Nf,3)=iv
            identical_vertex_found=.true.
            goto 113
         endif
      enddo                     ! iv
 113  continue
      if (.not.identical_vertex_found) then
         Nv=Nv+1
         if (Nv.gt.Nv_so_mx) then
            call error(label)
            write(*,*) 'Nv_so_mx has been reached'
            stop
         endif
         do j=1,dim
            vertices(Nv,j)=x3(j)
         enddo                  ! j
         faces(Nf,3)=Nv
      endif
c     
      return
      end

      

      subroutine remove_duplicate_vertices(dim,Nv,Nf,vertices,faces)
      implicit none
      include 'max.inc'
      include 'size_params.inc'
c     
c     Purpose: to identify and remove vertices that are considered as identical
c     
c     Input:
c       + dim: dimension of space
c       + Nv, Nf, vertices, faces: definition of the object
c       + x1, x2, x3: cartesian coordinates of the 3 nodes that define the face to add
c     
c     Output:
c       + Nv, Nf, vertices, faces: updated
c     
c     I/O
      integer dim
      integer Nv,Nf
      double precision vertices(1:Nv_mx,1:Ndim_mx)
      integer faces(1:Nf_mx,1:Nvinface)
c     temp
      logical keep_running
      integer ivertex,iface,i,j
      double precision P0(1:Ndim_mx)
      double precision P(1:Ndim_mx)
      integer Nelem
      integer elem_list(1:Nelem_mx)
      logical identical
      integer Nf_tr
      integer f_tr(1:Nf_mx)
      integer material_index(1:Nf_mx)
c     progress display
      integer len
      integer*8 ntot,ndone
      double precision fdone,fdone0
      integer ifdone,pifdone
      character*(Nchar_mx) str,source_str
      character*(Nchar_mx) fmt,fmt0
      logical err_found,found
c     label
      character*(Nchar_mx) label
      label='subroutine remove_duplicate_vertices'

c     --- progress display
      ntot=Nv
      ndone=0
      pifdone=0
      len=6
      call num2str(len,str,err_found)
      if (err_found) then
         call error(label)
         write(*,*) 'Could not convert to str:'
         write(*,*) 'len=',len
         stop
      endif
      fmt0='(a,i'//trim(str)//',a)'
      fmt='(i'//trim(str)//',a)'
      fdone0=0.0D+0
      write(*,trim(fmt0),advance='no') 'Done:   ',floor(fdone0),' %'
c     progress display ---
      ivertex=0
      keep_running=.true.
      do while (keep_running)
         ivertex=ivertex+1
c     P0: current reference vertex
         do j=1,dim
            P0(j)=vertices(ivertex,j)
         enddo                  ! j
         Nelem=0
         do i=ivertex+1,Nv
c     P: vertex checked against the reference vertex
            do j=1,dim
               P(j)=vertices(i,j)
            enddo               ! j
c     compare P0 and P
            call identical_positions(dim,P0,P,identical)
            if ((identical).and.(i.ne.ivertex)) then
               call check_faces_for_references(dim,Nf,faces,ivertex,i,Nf_tr,f_tr)
c     remove faces that use both vertices 'ivertex' and 'i'
               call remove_face_list_from_object(dim,Nf,faces,material_index,Nf_tr,f_tr)
c     there should only remain faces that do not use both 'ivertex' and 'i' => add 'i' to the list of vertices that are identical to 'ivertex'
               Nelem=Nelem+1
               if (Nelem.gt.Nelem_mx) then
                  call error(label)
                  write(*,*) 'Nelem_mx has been reached'
                  stop
               endif
c     vertex index "i" now is referenced as identical to vertex index "ivertex"
               elem_list(Nelem)=i
            endif               ! P0 and P are identical
         enddo                  ! i
         if (Nelem.gt.0) then
c     modify all references of vertices that have to be removed from the definition of faces
            call modify_vertex_references(dim,Nf,faces,ivertex,Nelem,elem_list)
c     remove vertices from the list of vertices
            call remove_vertices(dim,Nv,vertices,Nelem,elem_list)
            ntot=Nv
         endif                  ! Nelem > 0
c     --- progress display
         ndone=ndone+1
         fdone=dble(ndone)/dble(ntot)*1.0D+2
         ifdone=floor(fdone)
         if (ifdone.gt.pifdone) then
            do j=1,len+2
               write(*,"(a)",advance='no') "\b"
            enddo               ! j
            write(*,trim(fmt),advance='no') floor(fdone),' %'
            pifdone=ifdone
         endif
c     progress display ---
         if (ivertex.ge.Nv) then
            keep_running=.false.
         endif
      enddo                     ! while (keep_running)

      return
      end



      subroutine check_faces_for_references(dim,Nf,faces,idx1,idx2,Nface,face_idx)
      implicit none
      include 'max.inc'
      include 'size_params.inc'
c     
c     Purpose: check whether or not two vertex indexes are present in the definition of any given face
c     Identify faces that are affected
c     
c     Input:
c       + dim: dimension of space
c       + Nf: number of faces
c       + faces: definition of faces
c       + idx1, idx2: indexes of the two vertexes that are looked up in the same face
c     
c     Output:
c       + Nface: number of affected faces
c       + face_idx: indexes of the 'Nface' faces
c     
c     I/O
      integer dim
      integer Nf
      integer faces(1:Nf_mx,1:Nvinface)
      integer idx1
      integer idx2
      integer Nface
      integer face_idx(1:Nf_so_mx)
c     temp
      integer iface,j
      logical keep_running,idx1_found,idx2_found
c     label
      character*(Nchar_mx) label
      label='subroutine check_faces_for_references'

      Nface=0
      iface=0
      keep_running=.true.
      do while (keep_running)
         iface=iface+1
         idx1_found=.false.
         idx2_found=.false.
         do j=1,Nvinface
            if (faces(iface,j).eq.idx1) then
               idx1_found=.true.
            endif
            if (faces(iface,j).eq.idx2) then
               idx2_found=.true.
            endif
         enddo                  ! j
         if (idx1_found.and.idx2_found) then
            Nface=Nface+1
            if (Nface.gt.Nf_so_mx) then
               call error(label)
               write(*,*) 'Nf_so_mx has been reached'
               stop
            endif
            face_idx(Nface)=iface
         endif                  ! idx1_found and idx2_found
         if (iface.ge.Nf) then
            keep_running=.false.
         endif
      enddo                     ! while (keep_running)

      return
      end
      


      subroutine modify_vertex_references(dim,Nf,faces,ivertex,Nelem,elem_list)
      implicit none
      include 'max.inc'
      include 'size_params.inc'
c     
c     Purpose: to change the reference to a given number of vertices in the
c     definition of faces
c     
c     Input:
c       + dim: dimension of space
c       + Nf: number of faces
c       + faces: definition of faces
c       + ivertex: references should be replaced by this one
c       + Nelem: number of references to change
c       + elem_list: list of references to change
c     
c     Output:
c       + faces: modified
c     
c     I/O
      integer dim
      integer Nf
      integer faces(1:Nf_mx,1:Nvinface)
      integer ivertex
      integer Nelem
      integer elem_list(1:Nelem_mx)
c     temp
      integer iface,i,j,elem,k
      logical duplicates_found
      integer Nf_tr
      integer f_tr(1:Nf_so_mx)
      logical face_marked_for_deletion
      integer material_index(1:Nf_mx)
c     label
      character*(Nchar_mx) label
      label='subroutine modify_vertex_references'

      Nf_tr=0      
      do iface=1,Nf
 001     continue
         face_marked_for_deletion=.false.
         if (Nf_tr.gt.0) then
            do i=1,Nf_tr
               if (f_tr(i).eq.iface) then
                  face_marked_for_deletion=.true.
                  goto 111
               endif
            enddo               ! i
         endif                  ! Nf_tr > 0
 111     continue
         if (.not.face_marked_for_deletion) then            
            do j=1,Nvinface
               do elem=1,Nelem
c     replace all references that match the elem_list by the "ivertex" reference
                  if (faces(iface,j).eq.elem_list(elem)) then
                     faces(iface,j)=ivertex
                     call duplicate_vindex(dim,Nf,faces,iface,duplicates_found)
                     if (duplicates_found) then
c     This means at least two references in the same face have been replaced to the reference "ivertex"
c     This in only possible if two vertices of the face have been considered as identical positions
c     => face has to be removed
                        Nf_tr=Nf_tr+1
                        if (Nf_tr.gt.Nf_so_mx) then
                           call error(label)
                           write(*,*) 'Nf_tr=',Nf_tr
                           write(*,*) '> Nf_so_mx=',Nf_so_mx
                           stop
                        endif
                        f_tr(Nf_tr)=iface
                        goto 001
                     endif      ! dupicates_found
                  endif         ! faces(iface,j)=elem_list(elem)
               enddo            ! elem
c     but also, all references that are greater than any element of elem_list have to be modified
               do elem=1,Nelem
                  if (faces(iface,j).gt.elem_list(elem)) then
                     if ((elem.lt.Nelem).and.(faces(iface,j).lt.elem_list(elem+1))) then
                        faces(iface,j)=faces(iface,j)-elem
c     Debug
                        call duplicate_vindex(dim,Nf,faces,iface,duplicates_found)
                        if (duplicates_found) then
                           call error(label)
                           write(*,*) 'Identical vertex indexes found for face index:',iface
                           write(*,*) 'while decreasing index j=',j,' by elem=',elem
                           do k=1,Nvinface
                              write(*,*) 'faces(',iface,',',k,')=',faces(iface,k)
                           enddo ! k
                           stop
                        endif
c     Debug
                     endif
                     if (elem.eq.Nelem) then
                        faces(iface,j)=faces(iface,j)-Nelem
c     Debug
                        call duplicate_vindex(dim,Nf,faces,iface,duplicates_found)
                        if (duplicates_found) then
                           call error(label)
                           write(*,*) 'Identical vertex indexes found for face index:',iface
                           write(*,*) 'while decreasing index j=',j,' by Nelem=',Nelem
                           do k=1,Nvinface
                              write(*,*) 'faces(',iface,',',k,')=',faces(iface,k)
                           enddo ! k
                           stop
                        endif
c     Debug
                     endif
                  endif         ! faces(iface,j) > elem_list(elem)
               enddo            ! elem
            enddo               ! j
         endif                  ! face_marked_for_deletion=F
      enddo                     ! iface
c     
c     Remove faces that have been marked for deletion
      if (Nf_tr.gt.0) then
         call remove_face_list_from_object(dim,Nf,faces,material_index,Nf_tr,f_tr)
      endif                     ! Nf_tr > 0

      return
      end



      subroutine duplicate_vindex(dim,Nf,faces,iface,duplicates_found)
      implicit none
      include 'max.inc'
      include 'size_params.inc'
c     
c     Purpose: to check for identical vertex indexes for a given face
c     
c     Input:
c       + dim: dimension of space
c       + Nf: number of faces
c       + faces: definition of faces
c       + iface: index of the face to check
c     
c     Output:
c       + duplicates_found: T if at least two vertex indexes are identical for the face
c     
c     I/O
      integer dim
      integer Nf
      integer faces(1:Nf_mx,1:Nvinface)
      integer iface
      logical duplicates_found
c     temp
      integer i,j
      logical i1,i2
c     label
      character*(Nchar_mx) label
      label='subroutine duplicate_vindex'

      duplicates_found=.false.
      do i=1,Nvinface-1
         do j=i+1,Nvinface
            if (faces(iface,i).eq.faces(iface,j)) then
               duplicates_found=.true.
               goto 111
            endif
         enddo                  ! j
      enddo                     ! i
 111  continue

      return
      end
      


      subroutine remove_vertices(dim,Nv,vertices,Nelem,elem_list)
      implicit none
      include 'max.inc'
c     
c     Purpose: to remove a list of vertices from the list of vertices that defines a object
c     
c     Input:
c       + dim: dimension of space
c       + Nv: number of vertices
c       + vertices: list of vertices
c       + Nelem: number of elements to remove
c       + elem_list: list of elements to remove
c     
c     Output:
c       + Nv, vertices: modified
c     
c     I/O
      integer dim
      integer Nv
      double precision vertices(1:Nv_mx,1:Ndim_mx)
      integer Nelem
      integer elem_list(1:Nelem_mx)
c     temp
      integer elem,iv,i,j
c     label
      character*(Nchar_mx) label
      label='subroutine remove_vertices'

      do elem=1,Nelem
         do iv=elem_list(elem),Nv-1
            do j=1,dim
               vertices(iv,j)=vertices(iv+1,j)
            enddo               ! j
         enddo                  ! ivertex
         Nv=Nv-1
         if (elem.lt.Nelem) then
            do i=elem+1,Nelem
               elem_list(i)=elem_list(i)-1
            enddo               ! i
         endif
      enddo                     ! elem

      return
      end



      subroutine adjust_normals(dim,Nv,Nf,vertices,faces,Ncf,fct)
      implicit none
      include 'max.inc'
      include 'size_params.inc'
c     
c     Purpose: to adjust face normals in a coherent fashion
c     
c     Input:
c       + dim: dimension of space
c       + Nv, Nf, vertices, faces: definition of the object
c       + Ncf: number of connecting faces, per face
c       + fct: face connectivity table
c         fct(i,j), j=1,Ncf(i): indexes of the Ncf faces that connect to face i
c     
c     Output:
c       + faces: updated
c     
c     I/O
      integer dim
      integer Nv,Nf
      double precision vertices(1:Nv_mx,1:Ndim_mx)
      integer faces(1:Nf_mx,1:Nvinface)
      integer Ncf(1:Nf_mx)
      integer fct(1:Nf_mx,1:Ncf_mx)
c     temp
      integer Nfig
      integer fgroup(1:Nfig_mx)
      integer Nadjusted
      logical adjusted(1:Nf_mx)
      logical keep_going
      integer iface,i,j,face_idx,ref_face_idx,jface
      integer Nfig2
      integer fgroup2(1:Nfig_mx)
      integer Niter
      logical face_already_in_group
      double precision ref_normal(1:Ndim_mx)
      double precision normal(1:Ndim_mx)
      double precision prod
      integer tmp1
      logical first_non_adjusted_face_found
      integer first_non_adjusted_face_index
c     label
      character*(Nchar_mx) label
      label='subroutine adjust_normals'

c     Debug
c      write(*,*) 'Nf=',Nf
c     Debug

c     Initialization
      Nadjusted=0
      do iface=1,Nf
         adjusted(iface)=.false.
      enddo                     ! iface
      
c     First group of faces is made from face 1 only
      Nfig=1
      fgroup(1)=1
      Nadjusted=1
      adjusted(1)=.true.        ! keep the normal of face 1 as it is
c
      Niter=0
      keep_going=.true.
      do while (keep_going)
         Niter=Niter+1
c     Current group: identify all faces that are in contact with every face in the previous group, and that have not been adjusted
         Nfig2=0
         do iface=1,Nfig
            ref_face_idx=fgroup(iface)
c     get normal of (reference) face 1
            call face_normal(dim,Nv,Nf,vertices,faces,ref_face_idx,ref_normal)
c     
            do j=1,Ncf(fgroup(iface))
               face_idx=fct(fgroup(iface),j)
               call is_faceidx_in_fgroup(dim,Nfig2,fgroup2,face_idx,face_already_in_group)
               if ((.not.adjusted(face_idx)).and.(.not.face_already_in_group)) then
                  Nfig2=Nfig2+1
                  if (Nfig2.gt.Nfig_mx) then
                     call error(label)
                     write(*,*) 'Nfig_mx has been reached'
                     stop
                  endif
                  fgroup2(Nfig2)=face_idx
c     Get the normal of face index "face_idx", adjacent to face index "ref_face_idx"
                  call face_normal(dim,Nv,Nf,vertices,faces,face_idx,normal)
                  call scalar_product(dim,ref_normal,normal,prod)
                  if (prod.lt.0.0D+0) then
c     Invert the normal for the face if not coherent
                     tmp1=faces(face_idx,2)
                     faces(face_idx,2)=faces(face_idx,3)
                     faces(face_idx,3)=tmp1
                  endif         ! prod < 0
               endif            ! face index "face_idx" has not already been adjusted AND it is not already in the current group
            enddo               ! j
         enddo                  ! iface
c     Debug
c         write(*,*) 'Interation:',Niter,' number of faces in group:',Nfig2
c         if (Niter.le.3) then
c            do i=1,Nfig2
c               write(*,*) i,fgroup2(i)
c            enddo               ! i
c         endif
c     Debug
c     Declare adjacent faces as adjusted
         Nadjusted=Nadjusted+Nfig2
         do iface=1,Nfig2
            adjusted(fgroup2(iface))=.true.
         enddo ! iface

c     copy current group into preivous group
         Nfig=Nfig2
         do iface=1,Nfig2
            fgroup(iface)=fgroup2(iface)
         enddo                  ! iface
c     stop when the group has no face left AND all faces have been adjusted
         if (Nfig.le.0) then
            if (Nadjusted.eq.Nf) then
               keep_going=.false.
            else
c     most probably there are serveral non-contiguous blobs of faces
c     then you have to detect the first non-adjusted face
               first_non_adjusted_face_found=.false.
               do jface=1,Nf
                  if (.not.adjusted(jface)) then
                     first_non_adjusted_face_found=.true.
                     first_non_adjusted_face_index=jface
                     goto 123
                  endif
               enddo            ! jface
 123           continue
               if (.not.first_non_adjusted_face_found) then
                  call error(label)
                  write(*,*) 'Nadjusted=',Nadjusted
                  write(*,*) 'while Nf=',Nf
                  write(*,*) 'and yet no non-adjusted face was found !'
                  stop
               endif
               Nfig=1
               fgroup(1)=first_non_adjusted_face_index
               Nadjusted=Nadjusted+1
               adjusted(first_non_adjusted_face_index)=.true.
            endif               ! Nadjusted=Nf
         endif                  ! Nfig <= 0
      enddo                     ! while (keep_going)

      return
      end



      subroutine is_faceidx_in_fgroup(dim,Nfig,fgroup,face_idx,found)
      implicit none
      include 'max.inc'
c     
c     Purpose: to check whether or not a face index is present in a group list
c     
c     Input:
c       + dim: dimension of space
c       + Nfig: number of elements in the face group
c       + fgroup: indexes of faces in the group
c       + face_idx: index of the face to test
c     
c     Output:
c       + found: true if face index "face_idx" has been found in the face group
c     
c     I/O
      integer dim
      integer Nfig
      integer fgroup(1:Nfig_mx)
      integer face_idx
      logical found
c     temp
      integer i
c     label
      character*(Nchar_mx) label
      label='subroutine is_faceidx_in_fgroup'

      found=.false.
      do i=1,Nfig
         if (fgroup(i).eq.face_idx) then
            found=.true.
            goto 111
         endif
      enddo                     ! i
 111  continue

      return
      end



      subroutine node_normals(dim,L,Lx,Ly,Lz,t,Nv,Nf,vertices,faces,nNcf,nct,record_normals,node_normal,node_is_on_edge,Nedge,edge_idx)
      implicit none
      include 'max.inc'
      include 'formats.inc'
      include 'size_params.inc'
c     
c     Purpose: to compute normals @ nodes
c     
c     Input:
c       + dim: dimension of space
c       + L: dimension of the domain over each direction [m]
c       + Lx: value of Lx [m]
c       + Ly: value of Ly [m]
c       + Lz: value of Lz [m]
c       + t: target
c       + Nv, Nf, vertices, faces: definition of the object
c       + Ncf: number of connecting faces, per face
c       + nct: node connectivity table
c         nct(i,j), j=1,Ncf(i): indexes of the Ncf faces that connect to node i
c       + record_normals: if T, normals will be recorded into "results/node_normals.txt"
c     
c     Output:
c       + node_normal: normal @ each node
c       + node_is_on_edge: T if node is on a edge of the domain
c       + Nedge: number of edges checked (1-3)
c       + egde_idx: edge index
c         - edge_idx(ivertex,iedge,1)=1-3 (direction)
c         - edge_idx(ivertex,iedge,2)=1 or 2 (negative / positive side)
c     
c     I/O
      integer dim
      double precision L(1:Ndim_mx)
      double precision Lx
      double precision Ly
      double precision Lz
      double precision t
      integer Nv,Nf
      double precision vertices(1:Nv_mx,1:Ndim_mx)
      integer faces(1:Nf_mx,1:Nvinface)
      integer nNcf(1:Nv_mx)
      integer nct(1:Nv_mx,1:Ncf_mx)
      logical record_normals
      double precision node_normal(1:Nv_mx,1:Ndim_mx)
      logical node_is_on_edge(1:Nv_mx)
      integer Nedge(1:Nv_mx)
      integer edge_idx(1:Nv_mx,1:Ndim_mx,1:2)
c     temp
      double precision fnormal(1:Ndim_mx)
      double precision sum_fnormal(1:Ndim_mx)
      double precision normal1(1:Ndim_mx)
      double precision normal(1:Ndim_mx)
      integer iface,face_idx,j,ivertex
      double precision x(1:Ndim_mx)
      double precision delta_x(1:Ndim_mx)
      double precision prod
      double precision v1(1:Ndim_mx)
      double precision v2(1:Ndim_mx)
      logical solution_found1,solution_found2
c     Debug
      character*(Nchar_mx) results_file
c     Debug
c     parameters
      double precision dx,dy,epsilon
      parameter(dx=1.0D-4)
      parameter(dy=1.0D-4)
      parameter(epsilon=1.0D-6)
c     label
      character*(Nchar_mx) label
      label='subroutine node_normals'

      do ivertex=1,Nv
c     check whether of not node is on a edge
         node_is_on_edge(ivertex)=.false.
         Nedge(ivertex)=0
         do j=1,dim
            if ((dabs(vertices(ivertex,j)-L(j)).le.epsilon).or.(dabs(vertices(ivertex,j)+L(j)).le.epsilon)) then
               node_is_on_edge(ivertex)=.true.
               Nedge(ivertex)=Nedge(ivertex)+1
               if ((Nedge(ivertex).lt.1).or.(Nedge(ivertex).gt.dim)) then
                  call error(label)
                  write(*,*) 'Nedge(',ivertex,')=',Nedge(ivertex)
                  write(*,*) 'should be in the [1,',dim,'] range'
                  stop
               endif
               edge_idx(ivertex,Nedge(ivertex),1)=j
               if (dabs(vertices(ivertex,j)+L(j)).le.epsilon) then
                  edge_idx(ivertex,Nedge(ivertex),2)=1
               else
                  edge_idx(ivertex,Nedge(ivertex),2)=2
               endif
            endif
         enddo                  ! j
c     the normal to the current vertex is approximated by the average of normals for each face using the vertex
         do j=1,dim
            sum_fnormal(j)=0.0D+0
         enddo                  ! j
         do iface=1,nNcf(ivertex)
            face_idx=nct(ivertex,iface)
            call face_normal(dim,Nv,Nf,vertices,faces,face_idx,fnormal)
            call add_vectors(dim,sum_fnormal,fnormal,sum_fnormal)
         enddo                  ! iface
         call scalar_vector(dim,1.0D+0/dble(nNcf(ivertex)),sum_fnormal,normal1)
         call normalize_vector(dim,normal1,normal1)
c     Trying to evaluate more accurately the normal from derivatives of the function
         do j=1,dim
            x(j)=vertices(ivertex,j)
         enddo                  ! j
         delta_x(1)=dx
         delta_x(2)=0.0D+0
         delta_x(3)=0.0D+0
         call find_derivative(dim,Lx,Ly,Lz,t,x,delta_x,solution_found1,v1)
c     Debug
c         if (.not.solution_found1) then
c            call error(label)
c            write(*,*) 'derivative not found for:'
c            write(*,*) 'x=',x
c            write(*,*) 'dx=',dx
c            stop
c         endif
c     Debug
         delta_x(1)=0.0D+0
         delta_x(2)=dy
         delta_x(3)=0.0D+0
         call find_derivative(dim,Lx,Ly,Lz,t,x,delta_x,solution_found2,v2)
c     Debug
c         if (.not.solution_found2) then
c            call error(label)
c            write(*,*) 'derivative not found for:'
c            write(*,*) 'x=',x
c            write(*,*) 'dx=',dx
c            stop
c         endif
c     Debug
c
         if ((solution_found1).and.(solution_found2)) then
            call vector_product_normalized(dim,v1,v2,normal)
            call scalar_product(dim,normal,normal1,prod)
            if (prod.le.0.0D+0) then
               call scalar_vector(dim,-1.0D+0,normal,normal)
            endif
         else
            call copy_vector(dim,normal1,normal)
         endif
c     Additionnal trick: if node is on the edge of the domain, then
c     nullify the component of the normal in the direction x/y/z=cst
c     so that the normal lies in the x/y/z=cst plane (edge)
         if (node_is_on_edge(ivertex)) then
            do j=1,Nedge(ivertex)
               normal(edge_idx(ivertex,Nedge(ivertex),1))=0.0D+0
            enddo               ! j
c     and normalize
            if (Nedge(ivertex).lt.dim) then
               call normalize_vector(dim,normal,normal)
            endif
c     check the 
         endif                  ! node_is_on_edge(ivertex)
c     set the normal for current vertex
        do j=1,dim
            node_normal(ivertex,j)=normal(j)
         enddo                  ! j
      enddo                     ! ivertex
c     Debug
      if (record_normals) then
         results_file='./results/node_normals.txt'
         open(14,file=trim(results_file))
         write(14,*) Nv
         do ivertex=1,Nv
            if (node_is_on_edge(ivertex)) then
               write(14,*) (node_normal(ivertex,j),j=1,dim),node_is_on_edge(ivertex),Nedge(ivertex),(edge_idx(ivertex,j,1),j=1,Nedge(ivertex))
            else
               write(14,*) (node_normal(ivertex,j),j=1,dim),node_is_on_edge(ivertex)
            endif
         enddo                  ! ivertex
         close(14)
         write(*,*) 'File has been recorded: ',trim(results_file)
      endif                     ! record_normals
c     Debug

      return
      end



      subroutine find_derivative(dim,Lx,Ly,Lz,t,x,dx,solution_found,v)
      implicit none
      include 'max.inc'
c     
c     Purpose: to compute the augmentation of z for a given augmentation of position
c     
c     Input:
c       + dim: dimension of space
c       + Lx: value of Lx [m]
c       + Ly: value of Ly [m]
c       + Lz: value of Lz [m]
c       + t: target
c       + x: position
c       + dx: variation of position
c     
c     Output:
c       + solution_found: T when  a valid solution was found
c       + v: variation of z
c     
c     I/O
      integer dim
      double precision Lx
      double precision Ly
      double precision Lz
      double precision t
      double precision x(1:Ndim_mx)
      double precision dx(1:Ndim_mx)
      logical solution_found
      double precision v(1:Ndim_mx)
c     temp
      double precision z_range(1:Ndim_mx-1)
      integer Nsol,j
      double precision sigma3(1:Nsol_mx)
      double precision z1,z2
      integer Niter
      logical keep_running
      logical try_negative_dx
c     parameters
      double precision errf,dz
      parameter(dz=1.0D-3)
      parameter(errf=1.0D-8)
c     label
      character*(Nchar_mx) label
      label='subroutine find_derivative'

c     Trying to evaluate more accurately the normal from derivatives of the function
      z1=x(3)
c     z2 is obtained for (x+dx,y)
      z_range(1)=z1-dz
      z_range(2)=z1+dz
      Niter=0
      keep_running=.true.
      solution_found=.false.
      do while (keep_running)
         Niter=Niter+1
         call solve_gyroid(dim,Lx,Ly,Lz,3,x(1)+dx(1),x(2)+dx(2),z_range,t,errf,Nsol,sigma3)
         if (Nsol.eq.0) then
            z_range(1)=z_range(1)-dz
            z_range(2)=z_range(2)+dz
         endif
         if (Nsol.eq.1) then
            solution_found=.true.
            z2=sigma3(1)
            keep_running=.false.
         endif
         if (Nsol.ge.2) then
c            solution_found=.true.
c            z2=(sigma3(1)+sigma3(2))/2.0D+0
            solution_found=.false.
            keep_running=.false.
         endif
         if (Niter.gt.Niter_mx) then
            solution_found=.false.
            keep_running=.false.
         endif
      enddo                     ! while (keep_running)
      if (.not.solution_found) then
c     Try again with negative dx
         try_negative_dx=.true.
      else
         try_negative_dx=.false.
      endif
      if (try_negative_dx) then
         do j=1,dim
            dx(j)=-dx(j)
         enddo                  ! j
         z_range(1)=z1-dz
         z_range(2)=z1+dz
         Niter=0
         keep_running=.true.
         solution_found=.false.
         do while (keep_running)
            Niter=Niter+1
            call solve_gyroid(dim,Lx,Ly,Lz,3,x(1)+dx(1),x(2)+dx(2),z_range,t,errf,Nsol,sigma3)
            if (Nsol.eq.0) then
               z_range(1)=z_range(1)-dz
               z_range(2)=z_range(2)+dz
            endif
            if (Nsol.eq.1) then
               solution_found=.true.
               z2=sigma3(1)
               keep_running=.false.
            endif
            if (Nsol.ge.2) then
c               solution_found=.true.
c               z2=(sigma3(1)+sigma3(2))/2.0D+0
               solution_found=.false.
               keep_running=.false.
            endif
            if (Niter.gt.Niter_mx) then
               solution_found=.false.
               keep_running=.false.
            endif
         enddo                  ! while (keep_running)
      endif                     ! try_negative_dx
c
      if (solution_found) then
         v(1)=dx(1)
         v(2)=dx(2)
         v(3)=z2-z1
      endif                     ! solution_found

      return
      end
      


      subroutine thicken_gyroid_surface(dim,L,Nv,Nf,vertices,faces,node_normal,d,node_is_on_edge,Nedge,edge_idx)
      implicit none
      include 'max.inc'
      include 'size_params.inc'
c     
c     Purpose: duplicate the surface of the gyroid at a distance from the original
c     This is the original version, where the reference surface is not modified
c     
c     Input:
c       + dim: dimension of space
c       + L: typical length over each direction [m]
c       + Nv, Nf, vertices, faces: definition of the object
c       + node_normal: normal @ each node
c       + d: displacement [m]
c       + node_is_on_edge: T if node is on a edge of the bounding box
c       + Nedge: number of edges checked (1-3)
c       + egde_idx: edge index
c         - edge_idx(ivertex,iedge,1)=1-3 (direction)
c         - edge_idx(ivertex,iedge,2)=1 or 2 (negative / positive side)
c     
c     Output:
c       + Nv, Nf, vertices, faces: modified
c       + node_is_on_edge,Nedge,edge_idx: modified
c     
c     I/O
      integer dim
      double precision L(1:Ndim_mx)
      integer Nv,Nf
      double precision vertices(1:Nv_mx,1:Ndim_mx)
      integer faces(1:Nf_mx,1:Nvinface)
      double precision node_normal(1:Nv_mx,1:Ndim_mx)
      double precision d
      logical node_is_on_edge(1:Nv_mx)
      integer Nedge(1:Nv_mx)
      integer edge_idx(1:Nv_mx,1:Ndim_mx,1:2)
c     temp
      integer Nv0,Nf0
      integer ivertex,iface,i,j
      double precision normal(1:Ndim_mx)
      double precision u(1:Ndim_mx)
      double precision x0(1:Ndim_mx)
      double precision x(1:Ndim_mx)
c     label
      character*(Nchar_mx) label
      label='subroutine thicken_gyroid_surface'

      Nv0=Nv
      Nf0=Nf
c     Add a vertex at distance d from each initial vertex
      do ivertex=1,Nv
         do j=1,dim
            x0(j)=vertices(ivertex,j)
         enddo                  ! j
         Nv=Nv+1
         if (Nv.gt.Nv_mx) then
            call error(label)
            write(*,*) 'Nv_mx has been reached'
            stop
         endif
         do j=1,dim
            normal(j)=node_normal(ivertex,j)
         enddo                  ! j
c     displace in the opposite direction to the normal, of distance d
         call scalar_vector(dim,-d,normal,u)
         call add_vectors(dim,x0,u,x)
c     duplicate "node_is_on_edge" status of vertex "ivertex" on new vertex
         node_is_on_edge(Nv)=node_is_on_edge(ivertex)
         if (node_is_on_edge(Nv)) then
            Nedge(Nv)=Nedge(ivertex)
            do j=1,dim
               do i=1,2
                  edge_idx(Nv,j,i)=edge_idx(ivertex,j,i)
               enddo            ! i
            enddo               ! j
         endif
c     Add the new vertex
         do j=1,dim
            vertices(Nv,j)=x(j)
         enddo                  ! j
      enddo                     ! ivertex
c     Add a face for each initial face
      do iface=1,Nf
         Nf=Nf+1
         if (Nf.gt.Nf_mx) then
            call error(label)
            write(*,*) 'Nf_mx has been reached'
            stop
         endif
c     reverse normal
         faces(Nf,1)=faces(iface,1)+Nv0
         faces(Nf,2)=faces(iface,3)+Nv0
         faces(Nf,3)=faces(iface,2)+Nv0
      enddo                     ! iface

      return
      end



      subroutine thicken_gyroid_surface2(dim,L,Nv,Nf,vertices,faces,node_normal,d,node_is_on_edge,Nedge,edge_idx)
      implicit none
      include 'max.inc'
      include 'size_params.inc'
c     
c     Purpose: duplicate the surface of the gyroid at a distance from the original
c     This is a variant of the original subroutine: now the original surface is
c     displaced of d/2, and a duplicate is produced at a distance of d/2 in the
c     other direction.
c     
c     Input:
c       + dim: dimension of space
c       + L: dimension of the domain over each direction [m]
c       + Nv, Nf, vertices, faces: definition of the object
c       + node_normal: normal @ each node
c       + d: displacement [m]
c       + node_is_on_edge: T if node is on a edge of the bounding box
c       + Nedge: number of edges checked (1-3)
c       + egde_idx: edge index
c         - edge_idx(ivertex,iedge,1)=1-3 (direction)
c         - edge_idx(ivertex,iedge,2)=1 or 2 (negative / positive side)
c     
c     Output:
c       + Nv, Nf, vertices, faces: modified
c       + node_is_on_edge,Nedge,edge_idx: modified
c     
c     I/O
      integer dim
      double precision L(1:Ndim_mx)
      integer Nv,Nf
      double precision vertices(1:Nv_mx,1:Ndim_mx)
      integer faces(1:Nf_mx,1:Nvinface)
      double precision node_normal(1:Nv_mx,1:Ndim_mx)
      double precision d
      logical node_is_on_edge(1:Nv_mx)
      integer Nedge(1:Nv_mx)
      integer edge_idx(1:Nv_mx,1:Ndim_mx,1:2)
c     temp
      integer Nv0,Nf0
      integer ivertex,iface,i,j
      double precision normal(1:Ndim_mx)
      double precision u(1:Ndim_mx)
      double precision x0(1:Ndim_mx)
      double precision x1(1:Ndim_mx)
      double precision x2(1:Ndim_mx)
c     label
      character*(Nchar_mx) label
      label='subroutine thicken_gyroid_surface2'

      Nv0=Nv
      Nf0=Nf
c     Add a vertex at distance d from each initial vertex
      do ivertex=1,Nv
         do j=1,dim
            x0(j)=vertices(ivertex,j)
         enddo                  ! j
         Nv=Nv+1
         if (Nv.gt.Nv_mx) then
            call error(label)
            write(*,*) 'Nv_mx has been reached'
            stop
         endif
         do j=1,dim
            normal(j)=node_normal(ivertex,j)
         enddo                  ! j
c     displace the original position "x0" of a distance d/2 in the normal direction
         call scalar_vector(dim,d/2.0D+0,normal,u)
         call add_vectors(dim,x0,u,x1)
c     clamp position at the edges of the domain
         call clamp_at_edge(dim,L,x1)
c     displace "x0" in the opposite direction to the normal, of distance d/2
         call scalar_vector(dim,-d/2.0D+0,normal,u)
         call add_vectors(dim,x0,u,x2)
c     clamp position at the edges of the domain
         call clamp_at_edge(dim,L,x2)
c     duplicate "node_is_on_edge" status of vertex "ivertex" on new vertex
         node_is_on_edge(Nv)=node_is_on_edge(ivertex)
         if (node_is_on_edge(Nv)) then
            Nedge(Nv)=Nedge(ivertex)
            do j=1,dim
               do i=1,2
                  edge_idx(Nv,j,i)=edge_idx(ivertex,j,i)
               enddo            ! i
            enddo               ! j
         endif
c     Replace the original vertex by "x1"
         do j=1,dim
            vertices(ivertex,j)=x1(j)
         enddo                  ! j
c     Add the new vertex "x2"
         do j=1,dim
            vertices(Nv,j)=x2(j)
         enddo                  ! j
c     
      enddo                     ! ivertex
c     Add a face for each initial face
      do iface=1,Nf
         Nf=Nf+1
         if (Nf.gt.Nf_mx) then
            call error(label)
            write(*,*) 'Nf_mx has been reached'
            stop
         endif
c     reverse normal
         faces(Nf,1)=faces(iface,1)+Nv0
         faces(Nf,2)=faces(iface,3)+Nv0
         faces(Nf,3)=faces(iface,2)+Nv0
      enddo                     ! iface

      return
      end



      subroutine clamp_at_edge(dim,L,x)
      implicit none
      include 'max.inc'
c     
c     Purpose: to clamp a position on the edges of the spatial domain
c     
c     Input:
c       + dim: dimension of space 
c       + L: size of spatial domain
c       + x: position
c     
c     Output:
c       + x: clamped position
c     
c     I/O
      integer dim
      double precision L(1:Ndim_mx)
      double precision x(1:Ndim_mx)
c     temp
      integer j
c     label
      character*(Nchar_mx) label
      label='subroutine clamp_at_edge'

      do j=1,dim
         if (x(j).lt.-L(j)) then
            x(j)=-L(j)
         endif
         if (x(j).gt.L(j)) then
            x(j)=L(j)
         endif
      enddo                     ! j

      return
      end



      subroutine identify_edge_nodes(debug,dim,L,Nv,vertices,node_is_on_edge,Nedge,edge_idx,edge_index,bounding_box,use_group2,Ngroup1,vgroup1,Ngroup2,vgroup2)
      implicit none
      include 'max.inc'
c     
c     Purpose: identify vertices on a edge; they should belong to 2 groups:
c     - those who match the edge exactly (original shape)
c     - those who are not on a edge, but still (duplicates shape)
c     
c     Input:
c       + dim: dimension of space
c       + L: typical length over each direction [m]
c       + Nv, vertices: definition of the vertices
c       + node_is_on_edge: T if node is on a edge of the bounding box
c       + Nedge: number of edges checked (1-3)
c       + egde_idx: edge index
c         - edge_idx(ivertex,iedge,1)=1-3 (direction)
c         - edge_idx(ivertex,iedge,2)=1 or 2 (negative / positive side)
c       + edge_index:
c         - edge_indexx(1)=1-3 (direction)
c         - edge_index(2)=1 or 2 (negative / positive side)
c       + bounding_box: bounding box for the required group of nodes
c       + use_group2: generate group 2 only when T
c     
c     Output
c       + Ngroup1: number of nodes in group 1 (exactly on the required edge)
c       + vgroup1: coordinates o each vertex in group 1
c       + Ngroup2: number of nodes in group 2
c       + vgroup2: coordinates o each vertex in group 2
c     
c     I/O
      logical debug
      integer dim
      double precision L(1:Ndim_mx)
      integer Nv
      double precision vertices(1:Nv_mx,1:Ndim_mx)
      logical node_is_on_edge(1:Nv_mx)
      integer Nedge(1:Nv_mx)
      integer edge_idx(1:Nv_mx,1:Ndim_mx,1:2)
      integer edge_index(1:2)
      double precision bounding_box(1:Ndim_mx,1:2)
      logical use_group2
      integer Ngroup1
      double precision vgroup1(1:Nv_so_mx,1:Ndim_mx)
      integer Ngroup2
      double precision vgroup2(1:Nv_so_mx,1:Ndim_mx)
c     temp
      integer i,j,ivertex,iedge
      logical same_edge,is_in_box
      integer group1_idx(1:Nv_so_mx)
      integer group2_idx(1:Nv_so_mx)
      logical loop1,loop2
c     label
      character*(Nchar_mx) label
      label='subroutine identify_edge_nodes'

      Ngroup1=0
      Ngroup2=0
      do ivertex=1,Nv
         if (node_is_on_edge(ivertex)) then
c     same edge as the one required ?
            same_edge=.false.
            do iedge=1,Nedge(ivertex)
               if ((edge_idx(ivertex,iedge,1).eq.edge_index(1)).and.(edge_idx(ivertex,iedge,2).eq.edge_index(2))) then
                  same_edge=.true.
                  goto 111
               endif
            enddo               ! iedge
 111        continue
            if (same_edge) then
               is_in_box=.true.
               do j=1,dim
                  if ((vertices(ivertex,j).lt.bounding_box(j,1)).or.(vertices(ivertex,j).gt.bounding_box(j,2))) then
                     is_in_box=.false.
                     goto 112
                  endif
               enddo            ! j
 112           continue
               if (is_in_box) then
c     sort vertices into 2 groups
                  if ((.not.use_group2).or.((use_group2).and.(ivertex.le.Nv/2))) then
                     Ngroup1=Ngroup1+1
                     if (Ngroup1.gt.Nv_so_mx) then
                        call error(label)
                        write(*,*) 'Nv_so_mx has been reached'
                        stop
                     endif
                     group1_idx(Ngroup1)=ivertex
c     Debug
                     if (debug) then
                        write(*,*) 'Belongs to group 1: ivertex=',ivertex,' coord=',(vertices(ivertex,j),j=1,dim)
                     endif
c     Debug
                  else if ((use_group2).and.(ivertex.gt.Nv/2)) then
                     Ngroup2=Ngroup2+1
                     if (Ngroup2.gt.Nv_so_mx) then
                        call error(label)
                        write(*,*) 'Nv_so_mx has been reached'
                        stop
                     endif
                     group2_idx(Ngroup2)=ivertex
                  endif
               endif            ! is_in_box
            endif               ! same_edge
         endif                  ! is_on_edge(ivertex)
      enddo                     ! ivertex
c      call sort_group(dim,Nv,vertices,Ngroup1,group1_idx,sorting_dir,vgroup1)
c      call sort_group(dim,Nv,vertices,Ngroup2,group2_idx,sorting_dir,vgroup2)

c     Debug
      if (debug) then
         write(*,*) 'running analyze_group on group 1, Ngroup1=',Ngroup1
      endif
c     Debug
      call analyze_group(debug,dim,Nv,vertices,Ngroup1,group1_idx,loop1,vgroup1)
c     Debug
c      write(*,*) 'loop1=',loop1
c      open(13,file='./results/vgroup1.txt')
c      do i=1,Ngroup1
c         write(13,*) (vgroup1(i,j),j=2,3)
c      enddo                     ! i
c      if (loop) then
c         write(13,*) (vgroup1(1,j),j=2,3)
c      endif
c      close(13)
c      stop
c     Debug
      if (use_group2) then
         call analyze_group(.false.,dim,Nv,vertices,Ngroup2,group2_idx,loop2,vgroup2)
         if ((loop1).and.(.not.loop2)) then
            call error(label)
            write(*,*) 'group 1 is on a lopp'
            write(*,*) 'while group 2 is not'
            stop
         endif
      endif
      
      if (loop1) then
         Ngroup1=Ngroup1+1
         if (Ngroup1.gt.Nv_so_mx) then
            call error(label)
            write(*,*) 'Nv_so_mx has been reached'
            stop
         endif
         do j=1,dim
            vgroup1(Ngroup1,j)=vgroup1(1,j)
         enddo                  ! j
      endif                     ! loop1

      if ((use_group2).and.(loop2)) then
         Ngroup2=Ngroup2+1
         if (Ngroup2.gt.Nv_so_mx) then
            call error(label)
            write(*,*) 'Nv_so_mx has been reached'
            stop
         endif
         do j=1,dim
            vgroup2(Ngroup2,j)=vgroup2(1,j)
         enddo                  ! j
      endif                     ! use_group2 and loop2
      
c     Debug
c$$$      write(*,*) 'Ngroup1=',Ngroup1
c$$$      do i=1,Ngroup1
c$$$         write(*,*) i,(vgroup1(i,j),j=1,dim)
c$$$      enddo                     ! i
c$$$      write(*,*) 'Ngroup2=',Ngroup2
c$$$      do i=1,Ngroup2
c$$$         write(*,*) i,(vgroup2(i,j),j=1,dim)
c$$$      enddo                     ! i
c     Debug

      return
      end


      
      subroutine analyze_group(debug,dim,Nv,vertices,Nvg,group_idx,loop,vgroup)
      implicit none
      include 'max.inc'
c     
c     Purpose: extract the vertices for a group, and sort them by proximity
c     
c     Input:
c       + dim: dimension of space
c       + Nv, vertices: definition of the vertices
c       + Nvg: number of vertices in the group
c       + group_idx: (absolute) index of each vertex in the group [1, Nv]
c     
c     Output:
c       + loop: true if the group of points is on a closed contour
c       + vgroup: coordinates of each vertex in the group, sorted by proximity
c     
c     I/O
      logical debug
      integer dim
      integer Nv
      double precision vertices(1:Nv_mx,1:Ndim_mx)
      integer Nvg
      integer group_idx(1:Nv_so_mx)
      logical loop
      double precision vgroup(1:Nv_so_mx,1:Ndim_mx)
c     temp
      integer i,j,Nsorted
      integer vertex_idx
      integer vertex_to_exclude_idx
      integer closest_vertex_idx
      integer vidx(1:Nv_so_mx)
      logical sorted(1:Nv_so_mx)
      logical keep_looking,success
c     label
      character*(Nchar_mx) label
      label='subroutine analyze_group'

c     Debug
      if (debug) then
         open(14,file='./results/vgroup0.txt')
         do i=1,Nvg
            write(14,*) (vertices(group_idx(i),j),j=2,3)
         enddo                  ! i
         close(14)
         write(*,*) 'Nvg=',Nvg
      endif                     ! debug
c     Debug
      do i=1,Nvg
         sorted(i)=.false.
      enddo                     ! i
c     Start with point 1
      Nsorted=1
      sorted(1)=.true.
      vidx(1)=1
      vertex_idx=1
      vertex_to_exclude_idx=1
c     then find the closest vertex until all are sorted
c     Debug
      if (debug) then
         open(14,file='./results/vgroup01.txt')
         write(14,*) (vertices(group_idx(vidx(Nsorted)),j),j=2,3)
      endif                     ! debug
c     Debug
      keep_looking=.true.
      do while (keep_looking)
c     Debug
         if (debug) then
            write(*,*) 'looking for closest vertex, using:'
            write(*,*) 'vertex_idx=',vertex_idx
            write(*,*) 'vertex_to_exclude_idx=',vertex_to_exclude_idx
         endif                  ! debug
c     Debug
         call find_closest_vertex_in_group(dim,Nv,vertices,Nvg,group_idx,vertex_idx,vertex_to_exclude_idx,success,closest_vertex_idx)
c     Debug
         if (debug) then
            write(*,*) 'success=',success
            if (success) then
               write(*,*) 'closest_vertex_idx=',closest_vertex_idx,' sorted=',sorted(closest_vertex_idx)
            endif
         endif                  ! debug
c     Debug
         if (success) then
            if (sorted(closest_vertex_idx)) then
               if (Nsorted.eq.Nvg) then
                  loop=.true.
               else
                  loop=.false.
               endif            ! Nsorted=Nvg
               keep_looking=.false.
            else
               Nsorted=Nsorted+1
               if (Nsorted.ge.Nv_so_mx) then
                  call error(label)
                  write(*,*) 'Nv_so_mx has been reached'
                  stop
               endif
               sorted(closest_vertex_idx)=.true.
               vidx(Nsorted)=closest_vertex_idx
               vertex_to_exclude_idx=vertex_idx
               vertex_idx=closest_vertex_idx
c     Debug
               if (debug) then
                  write(14,*) (vertices(group_idx(vidx(Nsorted)),j),j=2,3)
               endif            ! debug
c     Debug
            endif               ! sorted(closest_vertex_idx)
         else
            loop=.false.
            keep_looking=.false.
         endif                  ! success
      enddo                     ! while (keep_looking)
c     Debug
      if (debug) then
         close(14)
         write(*,*) '----------------------------------------------------'
         write(*,*) 'End of phase 1: loop=',loop,' Nsorted=',Nsorted
         write(*,*) '----------------------------------------------------'
      endif                     ! debug
c     Debug
c     when a loop has been detected => search is over
c     otherwise, it must start over from the first position
      if (.not.loop) then
         keep_looking=.true.
         vertex_idx=vidx(1)
         vertex_to_exclude_idx=vidx(2)
c     Debug
         if (debug) then
            open(14,file='./results/vgroup02.txt')
            write(14,*) (vertices(group_idx(vidx(1)),j),j=2,3)
         endif                  ! debug
c     Debug
         do while (keep_looking)
c     Debug
            if (debug) then
               write(*,*) 'looking for closest vertex, using:'
               write(*,*) 'vertex_idx=',vertex_idx
               write(*,*) 'vertex_to_exclude_idx=',vertex_to_exclude_idx
            endif               ! debug
c     Debug
            call find_closest_vertex_in_group(dim,Nv,vertices,Nvg,group_idx,vertex_idx,vertex_to_exclude_idx,success,closest_vertex_idx)
c     Debug
            if (debug) then
               write(*,*) 'success=',success
               if (success) then
                  write(*,*) 'closest_vertex_idx=',closest_vertex_idx,' sorted=',sorted(closest_vertex_idx)
               endif
            endif               ! debug
c     Debug
            if (success) then
               if (sorted(closest_vertex_idx)) then
                  keep_looking=.false.
               else
                  Nsorted=Nsorted+1
                  if (Nsorted.ge.Nv_so_mx) then
                     call error(label)
                     write(*,*) 'Nv_so_mx has been reached'
                     stop
                  endif
c     found vertex must be put at position 1
                  do i=Nsorted,2,-1
                     vidx(i)=vidx(i-1)
                  enddo         ! i
                  sorted(closest_vertex_idx)=.true.
                  vidx(1)=closest_vertex_idx
                  vertex_to_exclude_idx=vertex_idx
                  vertex_idx=closest_vertex_idx
c     Debug
                  if (debug) then
                     write(14,*) (vertices(group_idx(vidx(1)),j),j=2,3)
                  endif         ! debug
c     Debug
               endif            ! sorted(closest_vertex_idx)
            else
               keep_looking=.false.
            endif               ! success
         enddo                  ! while (keep_looking)
      endif                     ! loop=F
c     Debug
      if (debug) then
         close(14)
         write(*,*) '----------------------------------------------------'
         write(*,*) 'End of phase 2: Nsorted=',Nsorted
         write(*,*) '----------------------------------------------------'
      endif                     ! debug
c     Debug
      if (Nsorted.ne.Nvg) then
         call error(label)
         write(*,*) 'Nsorted=',Nsorted
         write(*,*) 'should be equal to Nvg=',Nvg
         write(*,*) 'loop=',loop
         stop
      endif
c     produce vgroup
      do i=1,Nvg
c     Debug
         if (debug) then
            write(*,*) 'vidx(',i,')=',vidx(i)
         endif
c     Debug
         do j=1,dim
            vgroup(i,j)=vertices(group_idx(vidx(i)),j)
         enddo                  ! j
      enddo                     ! i

      return
      end



      subroutine find_closest_vertex_in_group(dim,Nv,vertices,Nvg,group_idx,vertex_idx,vertex_to_exclude_idx,success,closest_vertex_idx)
      implicit none
      include 'max.inc'
c     
c     Purpose: to find the closest vertex from a given vertex in a group
c     This routine uses the possibility to exclude a vertex from the research
c     (in addition to the target vertex)
c     
c     
c     Input:
c       + dim: dimension of space
c       + Nv, vertices: definition of the vertices
c       + Nvg: number of vertices in the group
c       + group_idx: index of each vertex in the group
c       + vertex_idx: index (in the group) of the target vertex [1, Nvg]
c       + vertex_to_exclude_idx: index (in the group) of the vertex that must be excluded from the research [1, Nvg]
c     
c     Output:
c       + success: T is a vertex could be found
c       + closest_vertex_idx: index (in the group) of the target vertex [1, Nvg]
c     
c     I/O
      integer dim
      integer Nv
      double precision vertices(1:Nv_mx,1:Ndim_mx)
      integer Nvg
      integer group_idx(1:Nv_so_mx)
      integer vertex_idx
      integer vertex_to_exclude_idx
      logical success
      integer closest_vertex_idx      
c     temp
      integer i,j
      double precision d,dmin
      logical dmin_initialized
      double precision P0(1:Ndim_mx)
      double precision Pe(1:Ndim_mx)
      double precision P0Pe(1:Ndim_mx)
      double precision P1(1:Ndim_mx)
      double precision P0P1(1:Ndim_mx)
      double precision prod
c     label
      character*(Nchar_mx) label
      label='subroutine find_closest_vertex_in_group'

c     P0: target position
      do j=1,dim
         P0(j)=vertices(group_idx(vertex_idx),j)
      enddo                     ! j
c     Pe: position to exclude
      do j=1,dim
         Pe(j)=vertices(group_idx(vertex_to_exclude_idx),j)
      enddo                     ! j
      call substract_vectors(dim,Pe,P0,P0Pe)
c     dmin is the minimum distance found between P0 and any other vertex,
c     with vertices index "vertex_idx" and "vertex_to_exclude_idx" excluded from the research
      dmin_initialized=.false.
      do i=1,Nvg
         if ((i.ne.vertex_idx).and.(i.ne.vertex_to_exclude_idx)) then
c     P1 is the position of the current vertex
            do j=1,dim
               P1(j)=vertices(group_idx(i),j)
            enddo               ! j
            call substract_vectors(dim,P1,P0,P0P1)
            call scalar_product(dim,P0Pe,P0P1,prod)
            if ((vertex_idx.eq.vertex_to_exclude_idx).or.((vertex_idx.ne.vertex_to_exclude_idx).and.(prod.lt.-0.5D+0))) then
               call distance(dim,P0,P1,d)
               if (dmin_initialized) then
                  if (d.lt.dmin) then
                     dmin=d
                     closest_vertex_idx=i                  
                  endif         ! d < dmin
               else
                  closest_vertex_idx=i
                  dmin=d
                  dmin_initialized=.true.
               endif
            endif
         endif                  ! i.ne.vertex_idx and i.ne.vertex_to_exclude_idx
      enddo                     ! i
      success=dmin_initialized

      return
      end



      subroutine sort_group(dim,Nv,vertices,Ngroup,group_idx,coord_index,vgroup)
      implicit none
      include 'max.inc'
c     
c     Purpose: extract the vertices for a group, and sort them by ascending coordinate
c     
c     Input:
c       + dim: dimension of space
c       + Nv, vertices: definition of the vertices
c       + Ngroup: number of vertices in the group
c       + group_idx: index of each vertex in the group
c       + coord_index: index of the coordinate they must be sorted according to
c     
c     Output:
c       + vgroup: coordinates of each vertex in the group, sorted by ascending order of the required coordinate
c     
c     I/O
      integer dim
      integer Nv
      double precision vertices(1:Nv_mx,1:Ndim_mx)
      integer Ngroup
      integer group_idx(1:Nv_so_mx)
      integer coord_index
      double precision vgroup(1:Nv_so_mx,1:Ndim_mx)
c     temp
      integer i,j,jmin,Nsorted
      double precision xmin
      logical sorted(1:Nv_so_mx)
      logical jmin_found,initialized
c     label
      character*(Nchar_mx) label
      label='subroutine sort_group'

      do i=1,Ngroup
         sorted(i)=.false.
      enddo                     ! i
      Nsorted=0
      do i=1,Ngroup
         initialized=.false.
         do j=1,Ngroup
            if (.not.sorted(j)) then
               xmin=vertices(group_idx(j),coord_index)
               initialized=.true.
               goto 111
            endif
         enddo                  ! j
 111     continue
         if (.not.initialized) then
            call error(label)
            write(*,*) 'Could not initialize for i=',i
            write(*,*) 'Ngroup=',Ngroup
            stop
         endif
         jmin_found=.false.
         do j=1,Ngroup
            if ((.not.sorted(j)).and.(vertices(group_idx(j),coord_index).le.xmin)) then
               jmin_found=.true.
               jmin=j
               xmin=vertices(group_idx(j),coord_index)
            endif
         enddo                  ! j
         if (.not.jmin_found) then
            call error(label)
            write(*,*) 'Could not find min for i=',i
            write(*,*) 'Ngroup=',Ngroup
            stop
         endif
         Nsorted=Nsorted+1
         if (Nsorted.gt.Ngroup) then
            call error(label)
            write(*,*) 'Nsorted=',Nsorted
            write(*,*) '> Ngroup=',Ngroup
            stop
         endif
         sorted(jmin)=.true.
         do j=1,dim
            vgroup(Nsorted,j)=vertices(group_idx(jmin),j)
         enddo                  ! j
      enddo                     ! i
      

      return
      end



      subroutine make_patch(debug,dim,Ngroup1,vgroup1,Ngroup2,vgroup2,use_group2,Nv01,Nf01,v01,f01)
      implicit none
      include 'max.inc'
      include 'size_params.inc'
c     
c     Purpose: to produce a path (object) from two group of vertices
c     
c     Input:
c       + dim: dimension of space
c       + Ngroup1: number of vertices in the first group
c       + vgroup1: coordinates of each vertex in the first group, sorted by ascending order of the required coordinate
c       + Ngroup2: number of vertices in the second group
c       + vgroup2: coordinates of each vertex in the second group, sorted by ascending order of the required coordinate
c       + use_group2: when F, only vertices from group 1 have to be used
c     
c     Output:
c       + Nv01,Nf01,v01,f01: definition of the object
c     
c     I/O
      logical debug
      integer dim
      integer Ngroup1
      double precision vgroup1(1:Nv_so_mx,1:Ndim_mx)
      integer Ngroup2
      double precision vgroup2(1:Nv_so_mx,1:Ndim_mx)
      logical use_group2
      integer Nv01,Nf01
      double precision v01(1:Nv_so_mx,1:Ndim_mx)
      integer f01(1:Nf_so_mx,1:Nvinface)
c     temp
      integer ivertex,iface,start_by_group,group_index,i,j
      integer Nused1,Nused2
      logical keep_going,point_available
      double precision normal(1:Ndim_mx)
      double precision n1(1:Ndim_mx)
      double precision P1(1:Ndim_mx)
      double precision u1(1:Ndim_mx)
      double precision prod
      integer tmp1,next_group_index
      double precision bounding_box(1:Ndim_mx,1:2)
      double precision Pc(1:Ndim_mx)
      integer Nf1,Nf2,first_face
      logical face1_convention,inverted
c     label
      character*(Nchar_mx) label
      label='subroutine make_patch'

      Nv01=0
      Nf01=0
      if (use_group2) then
         if ((Ngroup1.lt.2).and.(Ngroup2.lt.2)) then
            call error(label)
            write(*,*) 'Number of vertices in group 1=',Ngroup1
            write(*,*) 'Number of vertices in group 2=',Ngroup2
            write(*,*) 'is not enough to produce a single face'
            stop
         endif
         if (Ngroup1.ge.Ngroup2) then
            start_by_group=1
         else
            start_by_group=2
         endif
         group_index=start_by_group
         Nused1=0
         Nused2=0
         Nused1=Nused1+1
         Nv01=Nv01+1
         do j=1,dim
            v01(Nv01,j)=vgroup1(1,j)
         enddo                  ! j
         Nused2=Nused2+1
         Nv01=Nv01+1
         do j=1,dim
            v01(Nv01,j)=vgroup2(1,j)
         enddo                  ! j
         keep_going=.true.
         do while (keep_going)
            point_available=.false.
            if (group_index.eq.1) then
               if (Nused1.lt.Ngroup1) then
                  point_available=.true.
               endif
            else
               if (Nused2.lt.Ngroup2) then
                  point_available=.true.
               endif
            endif
            if (point_available) then
               Nv01=Nv01+1
               if (Nv01.gt.Nv_so_mx) then
                  call error(label)
                  write(*,*) 'Nv_so_mx has been reached'
                  stop
               endif
               if (group_index.eq.1) then
                  do j=1,dim
                     v01(Nv01,j)=vgroup1(Nused1+1,j)
                  enddo         ! j
                  Nused1=Nused1+1
               else
                  do j=1,dim
                     v01(Nv01,j)=vgroup2(Nused2+1,j)
                  enddo         ! j
                  Nused2=Nused2+1
               endif
               Nf01=Nf01+1
               if (Nf01.gt.Nf_so_mx) then
                  call error(label)
                  write(*,*) 'Nf_so_mx has been reached'
                  stop
               endif
               f01(Nf01,1)=Nv01-2
               f01(Nf01,2)=Nv01-1
               f01(Nf01,3)=Nv01
               if (group_index.eq.1) then
                  next_group_index=2
               else
                  next_group_index=1
               endif
               group_index=next_group_index
            else                ! point_available=F
               keep_going=.false.
            endif               ! point_available
         enddo                  ! while (keep_going)
c     
      else                      ! use_group2 = F
c     Find bounding box for the group
         do j=1,dim
            do i=1,2
               bounding_box(j,i)=vgroup1(1,j)
            enddo               ! i
         enddo                  ! j
         do ivertex=1,Ngroup1
            do j=1,dim
               if (vgroup1(ivertex,j).lt.bounding_box(j,1)) then
                  bounding_box(j,1)=vgroup1(ivertex,j)
               endif
               if (vgroup1(ivertex,j).gt.bounding_box(j,2)) then
                  bounding_box(j,2)=vgroup1(ivertex,j)
               endif
            enddo               ! j
         enddo                  ! vertex
c     assuming the central position is inside the contour !
         do j=1,dim
            Pc(j)=(bounding_box(j,1)+bounding_box(j,2))/2.0D+0
         enddo                  ! j
c     create vertices: all positions of the contour
         do i=1,Ngroup1
            Nv01=Nv01+1
            if (Nv01.gt.Nv_so_mx) then
               call error(label)
               write(*,*) 'Nv_so_mx has been reached'
               stop
            endif
            do j=1,dim
               v01(Nv01,j)=vgroup1(i,j)
            enddo               ! j
         enddo                  ! i
c     and the central position
         Nv01=Nv01+1
         if (Nv01.gt.Nv_so_mx) then
            call error(label)
            write(*,*) 'Nv_so_mx has been reached'
            stop
         endif
         do j=1,dim
            v01(Nv01,j)=Pc(j)
         enddo                  ! j
c     create Ngroup1 facets
         do i=1,Ngroup1-1
            Nf01=Nf01+1
            if (Nf01.gt.Nf_so_mx) then
               call error(label)
               write(*,*) 'Nf_so_mx has been reached'
               stop
            endif
            f01(Nf01,1)=i
            f01(Nf01,2)=Nv01
            if (i.lt.Ngroup1) then
               f01(Nf01,3)=i+1
            else
               f01(Nf01,3)=1
            endif               ! i < Ngroup1
         enddo                  ! i
      endif                     ! use_group2
c
c     Adjust normals
      do iface=1,Nf01
         call face_so_normal(dim,Nv01,Nf01,v01,f01,iface,normal)
         do j=1,dim
            P1(j)=v01(f01(iface,1),j)
         enddo                  ! j
         call normalize_vector(dim,P1,u1)
         call scalar_product(dim,normal,u1,prod)
         if (prod.lt.0.0D+0) then
c     invert face normal
            tmp1=f01(iface,2)
            f01(iface,2)=f01(iface,3)
            f01(iface,3)=tmp1
         endif                  ! normal.u1 < 0
      enddo                     ! iface
c     Despite this, some incorrectly oriented faces may remain (negative scalar product)
c     find number of facets oriented in both conventions
      Nf1=0                     ! number of facets with the convention of face 1
      Nf2=0                     ! number of facets with the opposite convention of face 1
      inverted=.false.
      call face_so_normal(dim,Nv01,Nf01,v01,f01,1,n1)
      do iface=1,Nf01
         call face_so_normal(dim,Nv01,Nf01,v01,f01,iface,normal)
         call scalar_product(dim,normal,n1,prod)
         if (prod.lt.0.0D+0) then
            inverted=(.not.inverted)
         endif
         if (inverted) then
            Nf2=Nf2+1
         else
            Nf1=Nf1+1
         endif
         call copy_vector(dim,normal,n1)
      enddo                     ! iface
      if (Nf1+Nf2.ne.Nf01) then
         call error(label)
         write(*,*) 'Nf01=',Nf01
         write(*,*) 'Nf1=',Nf1,' Nf2=',Nf2
         stop
      endif
c     If all faces have the same orientation: nothing to do
      if ((Nf1.eq.Nv01).or.(Nf2.eq.Nv01)) then
         goto 666
      endif
c     Debug
      if (debug) then
         write(*,*) 'Nf1=',Nf1,' Nf2=',Nf2
      endif                     ! debug
c     Debug
c     Otherwise, the main convention has to be identified
      if (Nf1.gt.Nf2) then
         face1_convention=.true.
      else
         face1_convention=.false.
      endif
c     Find first face with the right convention
      if (face1_convention) then
         first_face=1
      else
         call face_so_normal(dim,Nv01,Nf01,v01,f01,1,n1)
         do iface=1,Nf01
            call face_so_normal(dim,Nv01,Nf01,v01,f01,iface,normal)
            call scalar_product(dim,normal,n1,prod)
            if (prod.lt.0.0D+0) then
               first_face=iface
               goto 111
            endif
            call copy_vector(dim,normal,n1)
         enddo                  ! iface
 111     continue
      endif
c     Adjust face orientation
c     Debug
      if (debug) then
         write(*,*) 'faces before inversion:'
         do iface=1,Nf01
            write(*,*) iface,(f01(iface,j),j=1,3)
         enddo                  ! iface
      endif                     ! debug
c     Debug
      call face_so_normal(dim,Nv01,Nf01,v01,f01,first_face,n1)
      if (first_face.lt.Nf01) then
         do iface=first_face+1,Nf01
            call face_so_normal(dim,Nv01,Nf01,v01,f01,iface,normal)
            call scalar_product(dim,normal,n1,prod)
            if (prod.lt.0.0D+0) then
c     invert face normal
               tmp1=f01(iface,2)
               f01(iface,2)=f01(iface,3)
               f01(iface,3)=tmp1
               call scalar_vector(dim,-1.0D+0,normal,normal)
c     Debug
               if (debug) then
                  write(*,*) 'face inverted:',iface
               endif            ! debug
c     Debug
            endif
            call copy_vector(dim,normal,n1)
         enddo                  ! iface
      endif
      if (first_face.gt.1) then
         call face_so_normal(dim,Nv01,Nf01,v01,f01,first_face,n1)
         do iface=first_face-1,1,-1
            call face_so_normal(dim,Nv01,Nf01,v01,f01,iface,normal)
            call scalar_product(dim,normal,n1,prod)
            if (prod.lt.0.0D+0) then
c     invert face normal
               tmp1=f01(iface,2)
               f01(iface,2)=f01(iface,3)
               f01(iface,3)=tmp1
               call scalar_vector(dim,-1.0D+0,normal,normal)
c     Debug
               if (debug) then
                  write(*,*) 'face inverted:',iface
               endif            ! debug
c     Debug
            endif
            call copy_vector(dim,normal,n1)
         enddo                  ! iface
      endif                     ! first_face > 1
c     Debug
      if (debug) then
         write(*,*) 'faces after inversion:'
         do iface=1,Nf01
            write(*,*) iface,(f01(iface,j),j=1,3)
         enddo                  ! iface
      endif                     ! debug
c     Debug
      
 666  continue
      return
      end
