c     Copyright (C) 2023 |Meso|Star> (contact@meso-star.com)
      subroutine triangulate_contour(dim,debug,Nppc,contour,Nv,v,Nf,f)
      implicit none
      include 'max.inc'
      include 'size_params.inc'
c     
c     Purpose: to produce a trianglemesh from a given contour of points
c     in 3D space (not necessarily on the same plane, but they should also not
c     be very far from a average plane)
c     
c     Input:
c       + dim: dimension of space
c       + debug: debug mode if T
c       + Nppc: number of points in the contour
c       + contour: list of 3D positions (not particularly organised, just a list of positions)
c     
c     Output:
c       + Nv: number of vertices
c       + v: list of vertices
c       + Nf: number of triangular faces
c       + f: definition of faces
c     
c     I/O
      integer dim
      logical debug
      integer Nppc
      double precision contour(1:Nppc_mx,1:Ndim_mx)
      integer Nv
      double precision v(1:Nv_so_mx,1:Ndim_mx)
      integer Nf
      integer f(1:Nf_so_mx,1:Nvinface)
c     temp
      integer iface,iv,i,j,ip
      double precision P1(1:Ndim_mx)
      double precision P2(1:Ndim_mx)
      double precision P3(1:Ndim_mx)
      double precision P0(1:Ndim_mx)
      double precision P(1:Ndim_mx)
      double precision P1P2(1:Ndim_mx)
      double precision P1P3(1:Ndim_mx)
      double precision P1P0(1:Ndim_mx)
      double precision P1P(1:Ndim_mx)
      double precision u1(1:Ndim_mx)
      double precision u2(1:Ndim_mx)
      double precision u3(1:Ndim_mx)
      double precision n(1:Ndim_mx)
      double precision u3n
      integer Niter
      double precision u_ref(1:Ndim_mx)
      double precision v_ref(1:Ndim_mx)
      double precision w_ref(1:Ndim_mx)
c     Debug
      integer Nppc0
      double precision contour0(1:Nppc_mx,1:Ndim_mx)
c     Debug
c     label
      character*(Nchar_mx) label
      label='subroutine triangulate_contour'

      if (Nppc.lt.3) then
         call error(label)
         write(*,*) 'Nppc=',Nppc
         write(*,*) 'should be equal at least to 3'
         stop
      endif

c     Debug
      call duplicate_ctr(dim,Nppc,contour,Nppc0,contour0)
      if (debug) then
         write(*,*) 'Initial contour:'
         do i=1,Nppc
            write(*,*) i,(contour(i,j),j=1,dim)
         enddo                  ! i
      endif
c     Debug
      
      Niter=0
 111  continue
      Niter=Niter+1
      if (Niter.gt.Niter_mx) then
         call error(label)
         write(*,*) 'Number of iterations=',Niter
         write(*,*) 'this does not seem right'
         write(*,*) 'Initial contour:'
         write(*,*) 'Number of positions=',Nppc0
         do i=1,Nppc0
            write(*,*) i,(contour0(i,j),j=1,dim)
         enddo                  ! i
         stop
      endif
      do j=1,dim
         P1(j)=contour(1,j)
         P2(j)=contour(2,j)
         P3(j)=contour(3,j)
      enddo                     ! j
      if (Niter.gt.1) then
c     Project points P2 and P3 over the reference plane (only when it is known, so for Niter>1)
         call line_plane_intersection(dim,P1,u_ref,v_ref,P2,w_ref,P2)
         call line_plane_intersection(dim,P1,u_ref,v_ref,P3,w_ref,P3)
      endif
      call substract_vectors(dim,P2,P1,P1P2)
      call normalize_vector(dim,P1P2,u1)
      call substract_vectors(dim,P3,P1,P1P3)
      call vector_product_normalized(dim,P1P2,P1P3,u3)
      call vector_product_normalized(dim,u3,u1,u2)
      if (Niter.eq.1) then
         call copy_vector(dim,u1,u_ref)
         call copy_vector(dim,u2,v_ref)
         call copy_vector(dim,u3,w_ref)
      endif                     ! Niter=1
c     (P1, u1, u2) defines the local referential, with normal u3
      if (Nppc.gt.3) then
         do i=4,Nppc
c     P0 is position index i-1
c     P is position index i
            do j=1,dim
               P0(j)=contour(i-1,j)
               P(j)=contour(i,j)
            enddo               ! j
c     Project positions P0 and P over the reference plane
            call line_plane_intersection(dim,P1,u_ref,v_ref,P0,w_ref,P0)
            call line_plane_intersection(dim,P1,u_ref,v_ref,P,w_ref,P)
c     Then get P1P0.vect.P1P
            call substract_vectors(dim,P0,P1,P1P0)
            call substract_vectors(dim,P,P1,P1P)
            call vector_product_normalized(dim,P1P0,P1P,n)
            call scalar_product(dim,u3,n,u3n)
            if (u3n.lt.0.0D+0) then
c     Point P is not on a counterclockwise contour: the contour is then reorganized
c     Debug
c               write(*,*) 'Iteration',Niter,' stop for i=',i
c     Debug
               call reorganize_contour(dim,Nppc,contour,i)    
c     Debug
               if (debug) then
                  write(*,*) 'Contour after iteration',Niter
                  do ip=1,Nppc
                     write(*,*) ip,(contour(ip,j),j=1,dim)
                  enddo         ! i
                  if (Niter.ge.10) then
                     stop
                  endif
               endif
c     Debug
c     And start all over again
               goto 111
            endif
         enddo                  ! i
      endif                     ! Nppc > 3
c     Debug
c      if (Nppc.gt.3) then
c         write(*,*) 'Final contour:'
c         do i=1,Nppc
c            write(*,*) i,(contour(i,j),j=1,dim)
c         enddo                  ! i
c         stop
c      endif
c     Debug
c     Now "contour" is supposed to be anticlockwise... in its own referential, but at least it's consistent
c     Triangulate:
c     list of vertices
      Nv=0
      do iv=1,Nppc
         Nv=Nv+1
         if (Nv.gt.Nv_so_mx) then
            call error(label)
            write(*,*) 'Nv_so_mw has been reached'
            write(*,*) 'Nppc=',Nppc
            stop
         endif
         do j=1,dim
            v(Nv,j)=contour(iv,j)
         enddo                  ! j
      enddo                     ! i
c     list of faces
      Nf=Nppc-2
      if (Nf.lt.1) then
         call error(label)
         write(*,*) 'Nf=',Nf
         write(*,*) 'should be positive'
         write(*,*) 'Nppc=',Nppc
         stop
      endif
      if (Nf.gt.Nf_so_mx) then
         call error(label)
         write(*,*) 'Nf_so_mw has been exceeded'
         write(*,*) 'Nppc=',Nppc
         stop
      endif
      do iface=1,Nf
         f(iface,1)=1
         do j=2,Nvinface
            f(iface,j)=iface+j-1
         enddo                  ! j
      enddo                     ! iface
c         
      return
      end



      subroutine reorganize_contour(dim,Nppc,contour,new_position2)
      implicit none
      include 'max.inc'
c     
c     Purpose: to reorganize a contour; the new contour will be composed as such:
c     + position 1 stays at position 1
c     + position index "new_position2" becomes position 2
c     + then all other positions take successively positions 3 to Nppc
c     
c     Input:
c       + dim: dimension of space
c       + Nppc: number of points in the contour
c       + contour: list of 3D positions
c       + new_position2: index of the position that becomes the new position 2
c     
c     Output:
c       + contour (updated)
c     
c     I/O
      integer dim
      integer Nppc
      double precision contour(1:Nppc_mx,1:Ndim_mx)
      integer new_position2
c     temp
      double precision contour2(1:Nppc_mx,1:Ndim_mx)
      logical sorted(1:Nppc_mx)
      double precision P(1:Ndim_mx)
      integer i,current_position
c     label
      character*(Nchar_mx) label
      label='subroutine reorganize_contour'

      if ((new_position2.lt.1).or.(new_position2.gt.Nppc)) then
         call error(label)
         write(*,*) 'input new_position2=',new_position2
         write(*,*) 'should be in the [1, ',Nppc,'] range'
         stop
      endif
c      
      do i=1,Nppc
         sorted(i)=.false.
      enddo                     ! i
c     duplicate position 1 as position 1
      current_position=1
      call get_position_from_ctr(dim,Nppc,contour,1,P)
      call set_position_in_ctr(dim,Nppc,contour2,current_position,P)
      sorted(1)=.true.
c     position index "new_position2" takes position 2
      current_position=2
      call get_position_from_ctr(dim,Nppc,contour,new_position2,P)
      call set_position_in_ctr(dim,Nppc,contour2,current_position,P)
      sorted(new_position2)=.true.
c     then all remaining positions take positions 3 to Nppc
      do i=1,Nppc
         if (.not.sorted(i)) then
            current_position=current_position+1
            call get_position_from_ctr(dim,Nppc,contour,i,P)
            call set_position_in_ctr(dim,Nppc,contour2,current_position,P)
            sorted(i)=.true.
         endif                  ! sorted(i)=F            
      enddo                     ! i
c     checks
      if (current_position.ne.Nppc) then
         call error(label)
         write(*,*) 'After sorting, current_position=',current_position
         write(*,*) 'should be equal to Nppc=',Nppc
         stop
      endif
      do i=1,Nppc
         if (.not.sorted(i)) then
            call error(label)
            write(*,*) 'After sorting: sorted(',i,')=',sorted(i)
            write(*,*) 'should be T'
            stop
         endif
      enddo                     ! i
c     duplicate contour2 into contour
      call duplicate_ctr(dim,Nppc,contour2,Nppc,contour)
      
      return
      end
