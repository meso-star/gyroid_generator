c     Copyright (C) 2023 |Meso|Star> (contact@meso-star.com)
      subroutine obj2stl(dim,Nv,Nf,vertices,faces,invert_normal,stl_file,scaling_factor)
      implicit none
      include 'max.inc'
      include 'formats.inc'
      include 'size_params.inc'
c     
c     Purpose: to record a obj to a STL file
c     
c     Input:
c       + dim: dimension of space
c       + Nv: number of vertices
c       + Nf: number of faces
c       + vertices: vertices
c       + faces: faces
c       + invert_normal: T if normals should be inverted
c       + stl_file: STL file to record
c       + scaling_factor: global scaling factor over vertices
c     
c     Output: the required STL file
c     
c     I/O
      integer dim
      integer Nv,Nf
      double precision vertices(1:Nv_mx,1:Ndim_mx)
      integer faces(1:Nf_mx,1:Nvinface)
      logical invert_normal
      character*(Nchar_mx) stl_file
      double precision scaling_factor
c     temp
      integer iface,ivertex,i,j
      double precision p(1:Nvinface,1:Ndim_mx)
      double precision p1(1:Ndim_mx)
      double precision p2(1:Ndim_mx)
      double precision p3(1:Ndim_mx)
      double precision v1(1:Ndim_mx)
      double precision v2(1:Ndim_mx)
      double precision normal(1:Ndim_mx)
c     label
      character*(Nchar_mx) label
      label='subroutine obj2stl'

      open(11,file=trim(stl_file))
      write(11,10) 'solid vcg'
      do iface=1,Nf
         do i=1,Nvinface
            do j=1,dim
               p(i,j)=vertices(faces(iface,i),j)
            enddo               ! j
         enddo                  ! i
         do j=1,dim
            p1(j)=p(1,j)
            if (invert_normal) then
               p2(j)=p(3,j)
               p3(j)=p(2,j)
            else
               p2(j)=p(2,j)
               p3(j)=p(3,j)
            endif
         enddo                  ! j
         call substract_vectors(dim,p2,p1,v1)
         call substract_vectors(dim,p3,p1,v2)
         call vector_product_normalized(dim,v1,v2,normal)
         write(11,40) '  facet normal',(normal(j),j=1,dim)
         write(11,10) '    outer loop'
         write(11,40) '      vertex ',(p1(j)*scaling_factor,j=1,dim)
         write(11,40) '      vertex ',(p2(j)*scaling_factor,j=1,dim)
         write(11,40) '      vertex ',(p3(j)*scaling_factor,j=1,dim)
         write(11,10) '    endloop'
         write(11,10) '  endfacet'
      enddo                     ! iface
      write(11,10) 'endsolid vcg'
      close(11)
      write(*,*) 'STL file was recorded: ',trim(stl_file)
      
      return
      end


      
      subroutine sobj2stl(dim,Nv,Nf,vertices,faces,invert_normal,stl_file,scaling_factor)
      implicit none
      include 'max.inc'
      include 'formats.inc'
      include 'size_params.inc'
c     
c     Purpose: to record a single obj to a STL file
c     
c     Input:
c       + dim: dimension of space
c       + Nv: number of vertices
c       + Nf: number of faces
c       + vertices: vertices
c       + faces: faces
c       + invert_normal: T if normals should be inverted
c       + stl_file: STL file to record
c       + scaling_factor: global scaling factor over vertices
c     
c     Output: the required STL file
c     
c     I/O
      integer dim
      integer Nv,Nf
      double precision vertices(1:Nv_so_mx,1:Ndim_mx)
      integer faces(1:Nf_so_mx,1:Nvinface)
      logical invert_normal
      character*(Nchar_mx) stl_file
      double precision scaling_factor
c     temp
      integer iface,ivertex,i,j
      double precision p(1:Nvinface,1:Ndim_mx)
      double precision p1(1:Ndim_mx)
      double precision p2(1:Ndim_mx)
      double precision p3(1:Ndim_mx)
      double precision v1(1:Ndim_mx)
      double precision v2(1:Ndim_mx)
      double precision normal(1:Ndim_mx)
c     label
      character*(Nchar_mx) label
      label='subroutine sobj2stl'

      open(11,file=trim(stl_file))
      write(11,10) 'solid vcg'
      do iface=1,Nf
         do i=1,Nvinface
            do j=1,dim
               p(i,j)=vertices(faces(iface,i),j)
            enddo               ! j
         enddo                  ! i
         do j=1,dim
            p1(j)=p(1,j)
            if (invert_normal) then
               p2(j)=p(3,j)
               p3(j)=p(2,j)
            else
               p2(j)=p(2,j)
               p3(j)=p(3,j)
            endif
         enddo                  ! j
         call substract_vectors(dim,p2,p1,v1)
         call substract_vectors(dim,p3,p1,v2)
         call vector_product_normalized(dim,v1,v2,normal)
         write(11,40) '  facet normal',(normal(j),j=1,dim)
         write(11,10) '    outer loop'
         write(11,40) '      vertex ',(p1(j)*scaling_factor,j=1,dim)
         write(11,40) '      vertex ',(p2(j)*scaling_factor,j=1,dim)
         write(11,40) '      vertex ',(p3(j)*scaling_factor,j=1,dim)
         write(11,10) '    endloop'
         write(11,10) '  endfacet'
      enddo                     ! iface
      write(11,10) 'endsolid vcg'
      close(11)
      write(*,*) 'STL file was recorded: ',trim(stl_file)
      
      return
      end
