c     Copyright (C) 2023 |Meso|Star> (contact@meso-star.com)
      subroutine stardis_files(dim,hc,L,LC,Nvg,vg_dim_side,vg_bbox,draw_soles,draw_lateral,soles_thickness,lambda_soles,rho_soles,C_soles,Rcontact,Tinit_soles,
     &     Tinit_fluid,Tfluid,Trad,Trad_ref,Nsample,ddf,rho_fluid,C_fluid,d_solid,lambda_solid,rho_solid,C_solid,Tinit_solid,Nprobe,probe_position,Ntime,probe_time)
      implicit none
      include 'max.inc'
      include 'param.inc'
      include 'size_params.inc'
      include 'formats.inc'
c     
c     Purpose: produce all files required by STARDIS
c     
c     Input:
c       + dim: dimension of space
c       + hc: convective exchange coefficient [W/(m^2.K)]
c       + L: typical length over each direction [m]
c       + LC: limit conditions for each face [K]
c       + Nvg: number of vertices groups
c       + vg_dim_side: dimension and side for each vertices group
c       + vg_bbox: bounding box for each vertices group
c       + draw_soles: T if soles have to be drawn for x=+/-Lx
c       + draw_lateral: T if adiabatic lateral boundaries have to be drawn
c       + soles_thickness: soles and lateral boundaries thickness [m]
c       + lambda_soles: conductivity of soles and lateral boundaries [W.m⁻¹.K⁻¹]
c       + rho_soles: volumic mass of soles and lateral boundaries [kg.m⁻³]
c       + C_soles: specific heat of soles and lateral boundaries [J.kg⁻¹.K⁻¹]
c       + Rcontact: contact resistance between sole and gyroid solid [K.m².W⁻¹]
c       + Tinit_soles: initial temperature of soles and lateral boundaries [K]
c       + Tinit_fluid: initial temperature of the fluid [K]
c       + Tfluid: temperature of the fluid for t>0 [K]
c       + Trad: external radiative exchange temperature [K]
c       + Trad_ref: reference radiative temperature [K]
c       + Nsample: number of samples for Stardis
c       + ddf: value of delta=d_solid/ddf in Stardis
c       + rho_fluid: volumic mass [kg.m⁻³]
c       + C_fluid: massic specific heat [J.kg⁻1K⁻¹]
c       + d_solid: solid thickness [m]
c       + lambda_solid: conductivity [W.m⁻¹.K⁻¹]
c       + rho_solid: volumic mass [kg.m⁻³]
c       + C_solid: massic specific heat [J.kg⁻1K⁻¹]
c       + Tinit_solid: initial temperature [K]
c       + Nprobe: number of probe positions
c       + probe_position: probe positions [m, m, m]
c       + Ntime: number of time points
c       + probe time: values of probe times [s]
c     
c     Output:
c       + Stardis model file
c     
c     I/O
      integer dim
      double precision hc
      double precision L(1:Ndim_mx)
      double precision LC(1:Ndim_mx,1:2)
      integer Nvg
      integer vg_dim_side(1:Ngroup_mx,1:2)
      double precision vg_bbox(1:Ngroup_mx,1:Ndim_mx,1:2)
      logical draw_soles
      logical draw_lateral
      double precision soles_thickness
      double precision lambda_soles
      double precision rho_soles
      double precision C_soles
      double precision Rcontact
      double precision Tinit_soles
      double precision Tinit_fluid
      double precision Tfluid
      double precision Trad
      double precision Trad_ref
      integer Nsample
      double precision ddf
      double precision rho_fluid
      double precision C_fluid
      double precision d_solid
      double precision lambda_solid
      double precision rho_solid
      double precision C_solid
      double precision Tinit_solid
      integer Nprobe
      double precision probe_position(1:Nprobe_mx,1:Ndim_mx)
      integer Ntime
      double precision probe_time(1:Nprobe_mx)
c     temp
      integer i,j,iprobe,itime
      character*(Nchar_mx) model_file
      character*(Nchar_mx) script_file_file
      character*(Nchar_mx) interface_name
      integer ipatch,patch_index
      character*(Nchar_mx) patch_index_str,patch_file,identifier,face_file
      logical err_code
      double precision t
      character*(Nchar_mx) line,line1,xstr,time_str,command,iprobe_str,sole_str,face_name
      character*(Nchar_mx) Nsample_str,script_file
      integer err
      double precision x(1:Ndim_mx)
      logical draw_face
      integer sole_index
      double precision emissivity,h_conv
      logical cavity
      character*(Nchar_mx) path_solid_gyroid_obj
      character*(Nchar_mx) path_solid_sole_obj
      character*(Nchar_mx) path_int_gyroid_fluid_obj
      character*(Nchar_mx) path_int_sole_ext_obj
      character*(Nchar_mx) path_int_sole_universe_obj
      character*(Nchar_mx) path_int_sole_gyroid_obj
      character*(Nchar_mx) path_fluid_cavity_obj
      character*(Nchar_mx) path_int_sole_fluid_obj
      character*(Nchar_mx) path_solid_gyroid_stl
      character*(Nchar_mx) path_solid_sole_stl
      character*(Nchar_mx) path_int_gyroid_fluid_stl
      character*(Nchar_mx) path_int_sole_ext_stl
      character*(Nchar_mx) path_int_sole_universe_stl
      character*(Nchar_mx) path_int_sole_gyroid_stl
      character*(Nchar_mx) path_fluid_cavity_stl
      character*(Nchar_mx) path_int_sole_fluid_stl
      character*(Nchar_mx) solid_gyroid_stl
      character*(Nchar_mx) solid_sole_stl
      character*(Nchar_mx) int_gyroid_fluid_stl
      character*(Nchar_mx) int_sole_ext_stl
      character*(Nchar_mx) int_sole_universe_stl
      character*(Nchar_mx) int_sole_gyroid_stl
      character*(Nchar_mx) fluid_cavity_stl
      character*(Nchar_mx) int_sole_fluid_stl
c     parameters
      integer nap
      parameter(nap=6)
c     label
      character*(Nchar_mx) label
      label='stardis_files'

c     whether or not a fluid cavity needs to be created
      if ((draw_soles).and.(draw_lateral)) then
         cavity=.true.
      else
         cavity=.false.
      endif
      
c     ---------------------------------------------------------------------------------------------------------------
c     Stardis model file
c     ---------------------------------------------------------------------------------------------------------------
      model_file='./results/model.txt'
      call solid_gyroid_filenames(path_solid_gyroid_obj,path_solid_gyroid_stl)
      call extract_filename(path_solid_gyroid_stl,solid_gyroid_stl)
      open(11,file=trim(model_file))
      write(11,10) '# Ambient radiative temperature / Reference radiative temperature'
      write(11,30) 'TRAD ',Trad,Trad_ref
      write(11,*)
      write(11,10) '# media'
      identifier='gyroid_solid '
      write(11,32) 'SOLID ',trim(identifier),lambda_solid,rho_solid,C_solid,d_solid/ddf,Tinit_solid,' UNKNOWN ',0.0D+0,' FRONT ',trim(solid_gyroid_stl)
      sole_index=0
      do j=1,dim
         call drawface(j,draw_soles,draw_lateral,draw_face)
         do i=1,2
            sole_index=sole_index+1
            call fstr(sole_index,sole_str)
            if (draw_face) then
               call solid_sole_filenames(sole_index,path_solid_sole_obj,path_solid_sole_stl)
               call extract_filename(path_solid_sole_stl,solid_sole_stl)
               identifier='sole'//trim(sole_str)//'_solid '
               face_file=trim(solid_sole_stl)
               if (LC(j,i).gt.0.0D+0) then
                  write(11,31) 'SOLID ',trim(identifier),lambda_soles,rho_soles,C_soles,soles_thickness/ddf,LC(j,i),LC(j,i),0.0D+0,' FRONT ',trim(face_file)
               else
                  write(11,32) 'SOLID ',trim(identifier),1.0D-5,rho_soles,C_soles,soles_thickness/ddf,Tinit_soles,' UNKNOWN ',0.0D+0,' FRONT ',trim(face_file)
               endif
            endif               ! draw_face
         enddo                  ! i
      enddo                     ! j
      if (cavity) then
         call fluid_cavity_filenames(path_fluid_cavity_obj,path_fluid_cavity_stl)
         call extract_filename(path_fluid_cavity_stl,fluid_cavity_stl)
         identifier='fluid01 '
         write(11,37) 'FLUID ',trim(identifier),rho_fluid,C_fluid,Tinit_fluid,' UNKNOWN',' BACK ',trim(fluid_cavity_stl)
      endif                     ! draw_soles and draw_lateral
      write(11,*)
      write(11,10) '# Boundary conditions'
      if (.not.cavity) then
         call int_gyroid_fluid_filenames(path_int_gyroid_fluid_obj,path_int_gyroid_fluid_stl)
         call extract_filename(path_int_gyroid_fluid_stl,int_gyroid_fluid_stl)
         identifier='interface_fluid_gyroid '
         face_file=' '//trim(int_gyroid_fluid_stl)
         write(11,35) 'H_BOUNDARY_FOR_SOLID ',trim(identifier),Trad_ref,1.0,0.0,hc,Tfluid,trim(face_file)
      endif
      sole_index=0
      patch_index=0
      do j=1,dim
         call drawface(j,draw_soles,draw_lateral,draw_face)
         do i=1,2
            sole_index=sole_index+1
            call fstr(sole_index,sole_str)
            call int_sole_ext_filenames(sole_index,path_int_sole_ext_obj,path_int_sole_ext_stl)
            call extract_filename(path_int_sole_ext_stl,int_sole_ext_stl)
            call int_sole_universe_filenames(sole_index,path_int_sole_universe_obj,path_int_sole_universe_stl)
            call extract_filename(path_int_sole_universe_stl,int_sole_universe_stl)
            call int_sole_gyroid_filenames(sole_index,path_int_sole_gyroid_obj,path_int_sole_gyroid_stl)
            call extract_filename(path_int_sole_gyroid_stl,int_sole_gyroid_stl)
            if (cavity) then
               identifier='interface_sole'//trim(sole_str)//'_exterior '
               face_file=' '//trim(int_sole_ext_stl)
               emissivity=1.0D+0
               h_conv=hc
               write(11,35) 'H_BOUNDARY_FOR_SOLID ',trim(identifier),Trad_ref,emissivity,0.0,h_conv,Tfluid,trim(face_file)
            else
c     lateral faces
               if (draw_face) then
                  identifier='sole'//trim(sole_str)//'_boundary '
                  face_file=' '//trim(int_sole_universe_stl)
                  if (LC(j,i).gt.0.0D+0) then
                     emissivity=1.0D+0
                     h_conv=hc
                  else
                     emissivity=0.0D+0
                     h_conv=0.0D+0
                  endif
                  write(11,35) 'H_BOUNDARY_FOR_SOLID ',trim(identifier),Trad_ref,emissivity,0.0,h_conv,Tfluid,trim(face_file)
               else             ! draw_face=F
                  face_file=' '//trim(int_sole_gyroid_stl)
                  if (LC(j,i).gt.0.0D+0) then
                     identifier='Tface'//trim(sole_str)//' '
                     write(11,34) 'T_BOUNDARY_FOR_SOLID ',trim(identifier),LC(j,i),trim(face_file)
                  else          ! LC(i,j) ≤ 0
                     if (LC(j,i).eq.-1.0D+0) then
                        identifier='face'//trim(sole_str)//'_adiabatic '
                        write(11,34) 'F_BOUNDARY_FOR_SOLID ',trim(identifier),0.0D+0,trim(face_file)
                     else       ! LC(i,j)<0 but ≠-1
                        call error(label)
                        write(*,*) 'LC(',j,',',i,')=',LC(j,i)
                        write(*,*) 'is negative, but only a value of -1 is allowed'
                        stop
                     endif      ! LC(j,i) = -1
                  endif         ! LC(j,i) > 0
               endif            ! draw_face
            endif               ! cavity
         enddo                  ! i
      enddo                     ! j
      if (((draw_soles.or.draw_lateral).and.(Rcontact.gt.0.0D+0)).or.(cavity)) then
         write(11,*)
         write(11,10) '# Connections'
         if (cavity) then
            call int_gyroid_fluid_filenames(path_int_gyroid_fluid_obj,path_int_gyroid_fluid_stl)
            call extract_filename(path_int_gyroid_fluid_stl,int_gyroid_fluid_stl)
            identifier='interface_gyroid_fluid '
            face_file=' '//trim(int_gyroid_fluid_stl)
            emissivity=1.0D+0
            h_conv=hc
            write(11,36) 'SOLID_FLUID_CONNECTION ',trim(identifier),Trad_ref,emissivity,0.0,h_conv,trim(face_file)
         endif                  ! cavity
         sole_index=0
         do j=1,dim
            call drawface(j,draw_soles,draw_lateral,draw_face)
            do i=1,2
               sole_index=sole_index+1
               call fstr(sole_index,sole_str)
               call int_sole_gyroid_filenames(sole_index,path_int_sole_gyroid_obj,path_int_sole_gyroid_stl)
               call extract_filename(path_int_sole_gyroid_stl,int_sole_gyroid_stl)
               call int_sole_fluid_filenames(sole_index,path_int_sole_fluid_obj,path_int_sole_fluid_stl)
               call extract_filename(path_int_sole_fluid_stl,int_sole_fluid_stl)
               if ((draw_face).and.(Rcontact.gt.0.0D+0)) then
                  identifier='interface_sole'//trim(sole_str)//'_gyroid '
                  face_file=' '//trim(int_sole_gyroid_stl)
                  write(11,34) 'SOLID_SOLID_CONNECTION ',trim(identifier),Rcontact,trim(face_file)
               endif            ! draw_face
               if (cavity) then
                  identifier='interface_sole'//trim(sole_str)//'_fluid '
                  face_file=' '//trim(int_sole_fluid_stl)
                  if (LC(j,i).gt.0.0D+0) then
                     emissivity=1.0D+0
                     h_conv=hc
                  else
                     emissivity=0.0D+0
                     h_conv=0.0D+0
                  endif
                  write(11,36) 'SOLID_FLUID_CONNECTION ',trim(identifier),Trad_ref,emissivity,0.0,h_conv,trim(face_file)
               endif            ! cavity
            enddo               ! i
         enddo                  ! j
      endif                     ! Rcontact > 0
      close(11)
      write(*,*) 'Stardis model file was recorded: ',trim(model_file)
c     ---------------------------------------------------------------------------------------------------------------
c     Launch script
c     ---------------------------------------------------------------------------------------------------------------
      script_file='./results/run.sh'
      open(12,file=trim(script_file))
      write(12,10) '#!/bin/sh -e'
      write(12,*)
      write(12,10) '# USER PARAMETERS SECTION'
      call num2str(Nsample,Nsample_str,err_code)
      if (err_code) then
         call error(label)
         write(*,*) 'Could not convert to character: Nsample=',Nsample
         stop
      endif
      write(12,10) 'NREAL='//trim(Nsample_str)
      line='TIME="'
      do itime=1,Ntime
         t=probe_time(itime)
         call dble2str_noexp(t,nap,time_str,err)
         if (err.eq.1) then
            call error(label)
            write(*,*) 'Could not convert to character string: t=',t
            stop
         endif
         if (i.eq.1) then
            line=trim(line)//trim(time_str)
         else
            line=trim(line)//' '//trim(time_str)
         endif
      enddo                     ! i
      line=trim(line)//'"'
      write(12,10) trim(line)
      write(12,10) '# END USER PARAMETERS SECTION'
      write(12,*)
      write(12,10) '# Check stardis installation'
      write(12,10) 'if ! command -v stardis > /dev/null ; then'
      write(12,10) '  echo ">>> stardis command not found !"'
      write(12,10) '  echo ">>> To register stardis in the current shell you must type :"'
      write(12,10) '  echo ">>> source ~/Stardis-XXX-GNU-Linux64/etc/stardis.profile"'
      write(12,10) '  echo ">>> where ~/Stardis-XXX-GNU-Linux64 is the stardis directory installation"'
      write(12,10) '  exit 1'
      write(12,10) 'fi'
      write(12,*)
      write(12,10) '# Launch Stardis for each defined position and time'
      do iprobe=1,Nprobe
         call num2str3(iprobe,iprobe_str,err_code)
         if (err_code) then
            call error(label)
            write(*,*) 'Could not convert to character: iprobe=',iprobe
            stop
         endif
         do j=1,dim
            x(j)=probe_position(iprobe,j)
         enddo                  ! j
c     
         write(12,10) 'FILE="stardis_result_N${NREAL}_position'//trim(iprobe_str)//'.txt"'
         write(12,10) '# Erase FILE result if exists'
         write(12,10) 'rm -f "${FILE}"'
         write(12,10) 'echo "#time Temperature  errorbar  N_failures N_Realizations" >> "${FILE}"'
         write(12,10) 'for i in ${TIME}; do'
         write(12,10) '  printf ''%s  '' "${i}"  >> "${FILE}" '
c
         call dble2str_noexp(x(1),nap,xstr,err)
         if (err.eq.1) then
            call error(label)
            write(*,*) 'Could not convert to string: x(1)=',x(1)
            stop
         endif
         line1=' '//trim(xstr)//','
         call dble2str_noexp(x(2),nap,xstr,err)
         if (err.eq.1) then
            call error(label)
            write(*,*) 'Could not convert to string: x(2)=',x(2)
            stop
         endif
         line1=trim(line1)//trim(xstr)//','
         call dble2str_noexp(x(3),nap,xstr,err)
         if (err.eq.1) then
            call error(label)
            write(*,*) 'Could not convert to string: x(3)=',x(3)
            stop
         endif
         line1=trim(line1)//trim(xstr)
         line='  stardis -V 3 -M model.txt -p'//trim(line1)//',"${i}" -n "${NREAL}" >> "${FILE}"'
         write(12,10) trim(line)
         write(12,10) 'done'
         write(12,*)
c         
      enddo                     ! iprobe
      write(12,*)
      write(12,10) 'echo " "'
      write(12,10) 'echo ">>> Stardis simulation done"'
      close(12)
      write(*,*) 'Stardis script file was recorded: ',trim(script_file)
      command='chmod u+x '//trim(script_file)
      call exec(command)
      
      return
      end
      

      subroutine fstr(face_index,face_str)
      implicit none
      include 'max.inc'
c     
c     Purpose: produce the character string for face_index
c     
c     Input:
c       + face_index: face index [1, 99]
c     
c     Output:
c       + face_str: character string
c     
c     I/O
      integer face_index
      character*(Nchar_mx) face_str
c     temp
      logical err_code      
c     label
      character*(Nchar_mx) label
      label='subroutine fstr'
      
      if (face_index.gt.99) then
         call error(label)
         write(*,*) 'face_index=',face_index,' > 99'
         stop
      else
         call num2str2(face_index,face_str,err_code)
         if (err_code) then
            call error(label)
            write(*,*) 'Could not convert to character string: face_index=',face_index
         endif                  ! err_code
      endif                     ! face_index > 99

      return
      end
