c     Copyright (C) 2023 |Meso|Star> (contact@meso-star.com)
      subroutine add2obj(dim
     &     ,Nv,Nf,vertices,faces
     &     ,invert_normal
     &     ,Nv_so,Nf_so,v_so,f_so)
      implicit none
      include 'max.inc'
      include 'size_params.inc'
c
c     Purpose: to add a given object to the total wavefront object
c
c     Input:
c       + dim: dimension of space
c       + Nv, Nf, vertices, faces: definition of the total wavefront object
c       + Nv_so, Nf_so, v_so, f_so: definition of the object to add
c       + invert_normal: true if the direction of normals has to be inverted
c
c     Output:
c       + Nv, Nf, vertices, faces: updated
c
c     I/O
      integer dim
      integer Nv,Nf
      double precision vertices(1:Nv_mx,1:Ndim_mx)
      integer faces(1:Nf_mx,1:Nvinface)
      logical invert_normal
      integer Nv_so,Nf_so
      double precision v_so(1:Nv_so_mx,1:Ndim_mx)
      integer f_so(1:Nf_so_mx,1:Nvinface)
c     temp
      integer ivertex,iface,i
      integer Nv0
c     label
      character*(Nchar_mx) label
      label='subroutine add2obj'

c     Initial number of vertices in the global list
      Nv0=Nv
c     Invert normals if needed
      if (invert_normal) then
         call invert_normals_so(Nf_so,f_so)
      endif                     ! invert_normal
c     Add vertices to the global list
      do ivertex=1,Nv_so
         Nv=Nv+1
         if (Nv.gt.Nv_mx) then
            call error(label)
            write(*,*) 'Nv=',Nv
            write(*,*) '> Nv_mx=',Nv_mx
            stop
         endif
         do i=1,dim
            vertices(Nv,i)=v_so(ivertex,i)
         enddo                  ! i
      enddo                     ! ivertex
c     Add faces to the global list
      do iface=1,Nf_so
         Nf=Nf+1
         if (Nf.gt.Nf_mx) then
            call error(label)
            write(*,*) 'Nf=',Nf
            write(*,*) '> Nf_mx=',Nf_mx
            stop
         endif
         do i=1,Nvinface
            faces(Nf,i)=f_so(iface,i)+Nv0
         enddo                  ! i
      enddo                     ! iface

      return
      end


      
      subroutine add2sobj(dim
     &     ,Nv,Nf,vertices,faces
     &     ,invert_normal
     &     ,Nv_so,Nf_so,v_so,f_so)
      implicit none
      include 'max.inc'
      include 'size_params.inc'
c
c     Purpose: to add a given single object to a single wavefront object
c
c     Input:
c       + dim: dimension of space
c       + Nv, Nf, vertices, faces: definition of the total wavefront object
c       + Nv_so, Nf_so, v_so, f_so: definition of the object to add
c       + invert_normal: true if the direction of normals has to be inverted
c
c     Output:
c       + Nv, Nf, vertices, faces: updated
c
c     I/O
      integer dim
      integer Nv,Nf
      double precision vertices(1:Nv_so_mx,1:Ndim_mx)
      integer faces(1:Nf_so_mx,1:Nvinface)
      logical invert_normal
      integer Nv_so,Nf_so
      double precision v_so(1:Nv_so_mx,1:Ndim_mx)
      integer f_so(1:Nf_so_mx,1:Nvinface)
c     temp
      integer ivertex,iface,i
      integer Nv0
c     label
      character*(Nchar_mx) label
      label='subroutine add2sobj'

c     Initial number of vertices in the global list
      Nv0=Nv
c     Invert normals if needed
      if (invert_normal) then
         call invert_normals_so(Nf_so,f_so)
      endif                     ! invert_normal
c     Add vertices to the global list
      do ivertex=1,Nv_so
         Nv=Nv+1
         if (Nv.gt.Nv_so_mx) then
            call error(label)
            write(*,*) 'Nv=',Nv
            write(*,*) '> Nv_so_mx=',Nv_so_mx
            stop
         endif
         do i=1,dim
            vertices(Nv,i)=v_so(ivertex,i)
         enddo                  ! i
      enddo                     ! ivertex
c     Add faces to the global list
      do iface=1,Nf_so
         Nf=Nf+1
         if (Nf.gt.Nf_so_mx) then
            call error(label)
            write(*,*) 'Nf=',Nf
            write(*,*) '> Nf_so_mx=',Nf_so_mx
            stop
         endif
         do i=1,Nvinface
            faces(Nf,i)=f_so(iface,i)+Nv0
         enddo                  ! i
      enddo                     ! iface

      return
      end
