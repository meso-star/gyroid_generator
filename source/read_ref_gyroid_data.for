c     Copyright (C) 2023 |Meso|Star> (contact@meso-star.com)
      subroutine read_gyroid_data(dim,data_file,success,L,Lx,Ly,Lz,t,Ninterval)
      implicit none
      include 'max.inc'
c     
c     Purpose: to read the data used to produce the 'ref_gyroid.obj' file
c     
c     Input:
c       + dim: dimension of space
c       + data_file: file use to store the data use to produce 'ref_gyroid.obj'
c     
c     Output:
c       + success: whether or not reading the data file was succesfull
c       + L: typical length over each direction [m]
c       + Lx: value of Lx [m]
c       + Ly: value of Ly [m]
c       + Lz: value of Lz [m]
c       + t: target
c       + Ninterval: number of intervals for discretizing L
c     
c     I/O
      integer dim
      character*(Nchar_mx) data_file
      logical success
      double precision L(1:Ndim_mx)
      double precision Lx,Ly,Lz
      double precision t
      integer Ninterval
c     temp
      integer ios,j
c     label
      character*(Nchar_mx) label
      label='subroutine read_gyroid_data'

      open(11,file=trim(data_file),status='old',iostat=ios)
      if (ios.ne.0) then
         success=.false.
      else
         success=.true.
         read(11,*)
         do j=1,dim
            read(11,*)
            read(11,*) L(j)
         enddo                  ! j
         read(11,*)
         read(11,*) Lx
         read(11,*)
         read(11,*) Ly
         read(11,*)
         read(11,*) Lz
         read(11,*)
         read(11,*) t
         read(11,*)
         read(11,*) Ninterval
      endif
      close(11)

      return
      end

      

      subroutine record_gyroid_data(dim,data_file,L,Lx,Ly,Lz,t,Ninterval)
      implicit none
      include 'max.inc'
      include 'formats.inc'
c     
c     Purpose: to read the data used to produce the 'ref_gyroid.obj' file
c     
c     Input:
c       + dim: dimension of space
c       + data_file: file use to store the data use to produce 'ref_gyroid.obj'
c       + L: typical length over each direction [m]
c       + Lx: value of Lx [m]
c       + Ly: value of Ly [m]
c       + Lz: value of Lz [m]
c       + t: target
c       + Ninterval: number of intervals for discretizing L
c     
c     Output: the required file
c     
c     I/O
      integer dim
      character*(Nchar_mx) data_file
      double precision L(1:Ndim_mx)
      double precision Lx,Ly,Lz
      double precision t
      integer Ninterval
c     temp
      integer ios,j
c     label
      character*(Nchar_mx) label
      label='subroutine record_gyroid_data'

      open(11,file=trim(data_file))
      write(11,10) "# Data used for producing 'ref_gyroid.obj'"
      write(11,10) "# Dimension of domain in x-direction [m]"
      write(11,*) L(1)
      write(11,10) "# Dimension of domain in y-direction [m]"
      write(11,*) L(2)
      write(11,10) "# Dimension of domain in z-direction [m]"
      write(11,*) L(3)
      write(11,10) "# Lx [m]"
      write(11,*) Lx
      write(11,10) "# Ly [m]"
      write(11,*) Ly
      write(11,10) "# Lz [m]"
      write(11,*) Lz
      write(11,10) "# t"
      write(11,*) t
      write(11,10) "# Ninterval"
      write(11,*) Ninterval
      close(11)

      return
      end
      


      subroutine check_gyroid_data(dim,L,Lx,Ly,Lz,t,Ninterval,identical)
      implicit none
      include 'max.inc'
c     
c     Purpose: to check whether or not the 'ref_gyroid.obj' file was
c     produced for identical data as the one provided for the run
c     
c     Input:
c       + dim: dimension of space
c       + L: typical length over each direction [m]
c       + Lx: value of Lx [m]
c       + Ly: value of Ly [m]
c       + Lz: value of Lz [m]
c       + t: target
c       + Ninterval: number of intervals for discretizing L
c     
c     Output:
c       + identical: T if the data used to produce 'ref_gyroid.obj' and
c     data provided for the run is identical
c     
c     I/O
      integer dim
      double precision L(1:Ndim_mx)
      double precision Lx,Ly,Lz
      double precision t
      integer Ninterval
      logical identical
c     temp
      logical success
      integer j
      character*(Nchar_mx) data_file
      double precision L_dt(1:Ndim_mx)
      double precision Lx_dt,Ly_dt,Lz_dt
      double precision t_dt
      integer Ninterval_dt
c     label
      character*(Nchar_mx) label
      label='subroutine check_gyroid_data'
c
      data_file='./results/ref_gyroid.dat'
      call read_gyroid_data(dim,data_file,success,L_dt,Lx_dt,Ly_dt,Lz_dt,t_dt,Ninterval_dt)
c
      if (success) then
         identical=.true.
         do j=1,dim
            if (L(j).ne.L_dt(j)) then
               identical=.false.
               goto 666
            endif
         enddo                  ! j
         if (Lx.ne.Lx_dt) then
            identical=.false.
         endif
         if (Ly.ne.Ly_dt) then
            identical=.false.
         endif
         if (Lz.ne.Lz_dt) then
            identical=.false.
         endif
         if (t.ne.t_dt) then
            identical=.false.
         endif
         if (Ninterval.ne.Ninterval_dt) then
            identical=.false.
         endif
      else
         identical=.false.
      endif
c     
 666  continue
      return
      end
