      subroutine drawface(direction,draw_soles,draw_lateral,draw_face)
      implicit none
      include 'max.inc'
c     
c     Purpose: to decide whether or not the face has to be drawn
c     
c     Input:
c       + direction: main direction index
c       + draw_soles: T if soles have to be drawn
c       + draw_lateral: T if lateral adiabatic surfaces have to be drawn
c     
c     Output:
c       + draw_face: T if face has to be drawn
c     
c     I/O
      integer direction
      logical draw_soles
      logical draw_lateral
      logical draw_face
c     temp
c     label
      character*(Nchar_mx) label
      label='subroutine drawface'

      draw_face=.false.
      if ((draw_soles).and.(direction.eq.1)) then
         draw_face=.true.
         goto 666
      endif
      if ((draw_lateral).and.((direction.eq.2).or.(direction.eq.3))) then
         draw_face=.true.
         goto 666
      endif

 666  continue
      return
      end
      


      subroutine vertex_group_rotation(dim,rotation_center,rotation_axis,rotation_angle,Nv,vgroup_in,Np,ctr)
      implicit none
      include 'max.inc'
c     
c     Purpose: to rotate a group of vertices
c     
c     Input:
c       + dim: dimension of space
c       + rotation_center: position used as the rotation center
c       + rotation_axis: rotation axis
c       + rotation_angle: rotation angle [rad]
c       + Nv: number of vertices
c       + vgroup_in: input vertices (closed contour)
c     
c     Output:
c       + Np: number of points in 'ctr' (open contour)
c       + ctr: contour for the rotated group of vertices
c     
c     I/O
      integer dim
      double precision rotation_center(1:Ndim_mx)
      double precision rotation_axis(1:Ndim_mx)
      double precision rotation_angle
      integer Nv
      double precision vgroup_in(1:Nv_so_mx,1:Ndim_mx)
      integer Np
      double precision ctr(1:Nppc_mx,1:Ndim_mx)
c     temp
      integer ivertex,j
      double precision x1(1:Ndim_mx)
      double precision x2(1:Ndim_mx)
c     label
      character*(Nchar_mx) label
      label='subroutine vertex_group_rotation'

      Np=0
      do ivertex=1,Nv-1
         Np=Np+1
         do j=1,dim
            x1(j)=vgroup_in(ivertex,j)
         enddo                  ! j
         call rotation(dim,x1,rotation_angle,rotation_center,rotation_axis,x2)
         x2(3)=0.0D+0
         do j=1,dim
            ctr(ivertex,j)=x2(j)
         enddo                  ! j
      enddo                     ! ivertex

      return
      end


      
      subroutine vertex_group_translation(dim,Nv,vgroup_in,Np,ctr)
      implicit none
      include 'max.inc'
c     
c     Purpose: to rotate a group of vertices
c     
c     Input:
c       + dim: dimension of space
c       + Nv: number of vertices
c       + vgroup_in: input vertices (closed contour)
c     
c     Output:
c       + Np: number of points in 'ctr' (open contour)
c       + ctr: contour for the rotated group of vertices
c     
c     I/O
      integer dim
      integer Nv
      double precision vgroup_in(1:Nv_so_mx,1:Ndim_mx)
      integer Np
      double precision ctr(1:Nppc_mx,1:Ndim_mx)
c     temp
      integer ivertex,j
      double precision x1(1:Ndim_mx)
      double precision x2(1:Ndim_mx)
c     label
      character*(Nchar_mx) label
      label='subroutine vertex_group_translation'

      Np=0
      do ivertex=1,Nv-1
         Np=Np+1
         do j=1,dim
            x1(j)=vgroup_in(ivertex,j)
         enddo                  ! j
         call copy_vector(dim,x1,x2)
         x2(3)=0.0D+0
         do j=1,dim
            ctr(ivertex,j)=x2(j)
         enddo                  ! j
      enddo                     ! ivertex
      
      return
      end


      
      subroutine main_contour(dim,L,direction,side,rotation,translation,rotation_center,rotation_axis,rotation_angle,translation_vector,Ncontour,Npoints,points)
      implicit none
      include 'max.inc'
      include 'param.inc'
c     
c     Purpose: to set up the main contour for the interface between
c     the sole and the gyroid solid
c     
c     Input:
c       + dim: dimension of space
c       + L: dimension of the domain over each direction [m]
c       + direction: main direction index
c       + side: side index
c     
c     Output:
c       + rotation: T when a rotation must occur
c       + translation: T when a translation must occur
c       + rotation_center: position used as the rotation center
c       + rotation_axis: rotation axis
c       + rotation_angle: rotation angle [rad]
c       + translation_vector: translation vector
c       + Ncontour: number of contours (including holes)
c       + Npoints: number of points that define each contour
c       + points: list of points that define each contour
c     
c     I/O
      integer dim
      double precision L(1:Ndim_mx)
      integer direction
      integer side
      logical rotation
      logical translation
      double precision rotation_center(1:Ndim_mx)
      double precision rotation_axis(1:Ndim_mx)
      double precision rotation_angle
      double precision translation_vector(1:Ndim_mx)
      integer Ncontour
      integer Npoints(1:Ncontour_mx)
      double precision points(1:Ncontour_mx,1:Nppc_mx,1:Ndim_mx)
c     temp
c     label
      character*(Nchar_mx) label
      label='subroutine main_contour'

      Ncontour=1
      Npoints(1)=4
      if (direction.eq.1) then
         rotation=.true.
         translation=.false.
         if (side.eq.1) then
            rotation_axis(1)=0.0D+0
            rotation_axis(2)=1.0D+0
            rotation_axis(3)=0.0D+0
            rotation_center(1)=-L(1)
            rotation_center(2)=0.0D+0
            rotation_center(3)=0.0D+0
            rotation_angle=-pi/2.0D+0
         else if (side.eq.2) then
            rotation_axis(1)=0.0D+0
            rotation_axis(2)=1.0D+0
            rotation_axis(3)=0.0D+0
            rotation_center(1)=L(1)
            rotation_center(2)=0.0D+0
            rotation_center(3)=0.0D+0
            rotation_angle=pi/2.0D+0
         endif
         points(1,1,1)=rotation_center(1)-L(3)
         points(1,1,2)=-L(2)
         points(1,1,3)=0.0D+0
         points(1,2,1)=rotation_center(1)+L(3)
         points(1,2,2)=-L(2)
         points(1,2,3)=0.0D+0
         points(1,3,1)=rotation_center(1)+L(3)
         points(1,3,2)=L(2)
         points(1,3,3)=0.0D+0
         points(1,4,1)=rotation_center(1)-L(3)
         points(1,4,2)=L(2)
         points(1,4,3)=0.0D+0
      else if (direction.eq.2) then
         rotation=.true.
         translation=.false.
         if (side.eq.1) then
            rotation_axis(1)=1.0D+0
            rotation_axis(2)=0.0D+0
            rotation_axis(3)=0.0D+0
            rotation_center(1)=0.0D+0
            rotation_center(2)=-L(2)
            rotation_center(3)=0.0D+0
            rotation_angle=pi/2.0D+0
         else if (side.eq.2) then
            rotation_axis(1)=1.0D+0
            rotation_axis(2)=0.0D+0
            rotation_axis(3)=0.0D+0
            rotation_center(1)=0.0D+0
            rotation_center(2)=L(2)
            rotation_center(3)=0.0D+0
            rotation_angle=-pi/2.0D+0
         endif
         points(1,1,1)=-L(1)
         points(1,1,2)=rotation_center(2)-L(3)
         points(1,1,3)=0.0D+0
         points(1,2,1)=L(1)
         points(1,2,2)=rotation_center(2)-L(3)
         points(1,2,3)=0.0D+0
         points(1,3,1)=L(1)
         points(1,3,2)=rotation_center(2)+L(3)
         points(1,3,3)=0.0D+0
         points(1,4,1)=-L(1)
         points(1,4,2)=rotation_center(2)+L(3)
         points(1,4,3)=0.0D+0
      else if (direction.eq.3) then
         rotation=.false.
         translation=.true.
         points(1,1,1)=-L(1)
         points(1,1,2)=-L(2)
         points(1,1,3)=0.0D+0
         points(1,2,1)=L(1)
         points(1,2,2)=-L(2)
         points(1,2,3)=0.0D+0
         points(1,3,1)=L(1)
         points(1,3,2)=L(2)
         points(1,3,3)=0.0D+0
         points(1,4,1)=-L(1)
         points(1,4,2)=L(2)
         points(1,4,3)=0.0D+0
         if (side.eq.1) then
            translation_vector(1)=0.0D+0
            translation_vector(2)=0.0D+0
            translation_vector(3)=-L(3)
         else
            translation_vector(1)=0.0D+0
            translation_vector(2)=0.0D+0
            translation_vector(3)=L(3)
         endif
      endif
      
      return
      end



      subroutine complete_sole(dim,direction,side,Nv_so,Nf_so,v_so,f_so,soles_thickness,Nv_add,Nf_add,v_add,f_add)
      implicit none
      include 'max.inc'
      include 'size_params.inc'
c     
c     Purpose: to complete a sole from the mesh of the main interface
c     
c     Input:
c       + dim: dimension of space
c       + direction: main direction index
c       + side: side index
c       + Nv_so: number of vertices of the mesh
c       + Nf_so: number of faces of the mesh
c       + v_so: vertices
c       + f_so: faces
c       + soles_thickness: required thickness of the sole [m]
c     
c     Output:
c       + Nv_so, Nf_so, v_so, f_so: updated
c       + Nv_add, Nf_add, v_add, f_add: object added to the original object
c     
c     I/O
      integer dim
      integer direction,side
      integer Nv_so,Nf_so
      double precision v_so(1:Nv_so_mx,1:Ndim_mx)
      integer f_so(1:Nf_so_mx,1:Nvinface)
      double precision soles_thickness
      integer Nv_add,Nf_add
      double precision v_add(1:Nv_so_mx,1:Ndim_mx)
      integer f_add(1:Nf_so_mx,1:Nvinface)
c     temp
      integer i,j,Nv0
      double precision a(1:Ndim_mx)
      double precision b(1:Ndim_mx)
      double precision c(1:Ndim_mx)
      double precision ab(1:Ndim_mx)
      double precision ac(1:Ndim_mx)
      double precision n(1:Ndim_mx)
      double precision x1(1:Ndim_mx)
      double precision x2(1:Ndim_mx)
      double precision x3(1:Ndim_mx)
      double precision x4(1:Ndim_mx)
      double precision u12(1:Ndim_mx)
      double precision u14(1:Ndim_mx)
      double precision x5(1:Ndim_mx)
      double precision x6(1:Ndim_mx)
      double precision x7(1:Ndim_mx)
      double precision x8(1:Ndim_mx)
      double precision d(1:Ndim_mx)
      logical add,invert_normal
c     label
      character*(Nchar_mx) label
      label='subroutine complete_sole'

      do j=1,dim
         x1(j)=v_so(1,j)
         x2(j)=v_so(2,j)
         x3(j)=v_so(3,j)
         x4(j)=v_so(4,j)
         a(j)=v_so(f_so(1,1),j)
         b(j)=v_so(f_so(1,2),j)
         c(j)=v_so(f_so(1,3),j)
      enddo                     ! j
      call substract_vectors(dim,b,a,ab)
      call substract_vectors(dim,c,a,ac)
      call vector_product_normalized(dim,ab,ac,n)
c     Debug
c      write(*,*) 'direction=',direction,' side=',side,' n=',n
c     Debug
      call scalar_vector(dim,soles_thickness,n,d)
      add=.false.
      if (add) then
         call add_vectors(dim,x1,d,x5)
         call add_vectors(dim,x2,d,x6)
         call add_vectors(dim,x3,d,x7)
         call add_vectors(dim,x4,d,x8)
      else
         call substract_vectors(dim,x1,d,x5)
         call substract_vectors(dim,x2,d,x6)
         call substract_vectors(dim,x3,d,x7)
         call substract_vectors(dim,x4,d,x8)
      endif
      Nv_add=0
      do j=1,dim
         v_add(Nv_add+1,j)=x1(j)
         v_add(Nv_add+2,j)=x2(j)
         v_add(Nv_add+3,j)=x3(j)
         v_add(Nv_add+4,j)=x4(j)
      enddo                     ! j
      Nv_add=Nv_add+4
      if (Nv_add.gt.Nv_so_mx) then
         call error(label)
         write(*,*) 'Nv_add=',Nv_add
         write(*,*) '> Nv_so_mx=',Nv_so_mx
         stop
      endif
      Nv0=Nv_add
      do j=1,dim
         v_add(Nv_add+1,j)=x5(j)
         v_add(Nv_add+2,j)=x6(j)
         v_add(Nv_add+3,j)=x7(j)
         v_add(Nv_add+4,j)=x8(j)
      enddo                     ! j
      Nv_add=Nv_add+4
      if (Nv_add.gt.Nv_so_mx) then
         call error(label)
         write(*,*) 'Nv_add=',Nv_add
         write(*,*) '> Nv_so_mx=',Nv_so_mx
         stop
      endif
c     Add face 1
      Nf_add=0
      Nf_add=Nf_add+1
      if (Nf_add.gt.Nf_so_mx) then
         call error(label)
         write(*,*) 'Nf_so_mx has been reached'
         stop
      endif
      f_add(Nf_add,1)=1
      f_add(Nf_add,2)=Nv0+2
      f_add(Nf_add,3)=2
c     Add face 2
      Nf_add=Nf_add+1
      if (Nf_add.gt.Nf_so_mx) then
         call error(label)
         write(*,*) 'Nf_so_mx has been reached'
         stop
      endif
      f_add(Nf_add,1)=1
      f_add(Nf_add,2)=Nv0+1
      f_add(Nf_add,3)=Nv0+2
c     Add face 3
      Nf_add=Nf_add+1
      if (Nf_add.gt.Nf_so_mx) then
         call error(label)
         write(*,*) 'Nf_so_mx has been reached'
         stop
      endif
      f_add(Nf_add,1)=2
      f_add(Nf_add,2)=Nv0+2
      f_add(Nf_add,3)=Nv0+3
c     Add face 4
      Nf_add=Nf_add+1
      if (Nf_add.gt.Nf_so_mx) then
         call error(label)
         write(*,*) 'Nf_so_mx has been reached'
         stop
      endif
      f_add(Nf_add,1)=2
      f_add(Nf_add,2)=Nv0+3
      f_add(Nf_add,3)=3
c     Add face 5
      Nf_add=Nf_add+1
      if (Nf_add.gt.Nf_so_mx) then
         call error(label)
         write(*,*) 'Nf_so_mx has been reached'
         stop
      endif
      f_add(Nf_add,1)=3
      f_add(Nf_add,2)=Nv0+3
      f_add(Nf_add,3)=Nv0+4
c     Add face 6
      Nf_add=Nf_add+1
      if (Nf_add.gt.Nf_so_mx) then
         call error(label)
         write(*,*) 'Nf_so_mx has been reached'
         stop
      endif
      f_add(Nf_add,1)=3
      f_add(Nf_add,2)=Nv0+4
      f_add(Nf_add,3)=4
c     Add face 7
      Nf_add=Nf_add+1
      if (Nf_add.gt.Nf_so_mx) then
         call error(label)
         write(*,*) 'Nf_so_mx has been reached'
         stop
      endif
      f_add(Nf_add,1)=1
      f_add(Nf_add,2)=4
      f_add(Nf_add,3)=Nv0+4
c     Add face 8
      Nf_add=Nf_add+1
      if (Nf_add.gt.Nf_so_mx) then
         call error(label)
         write(*,*) 'Nf_so_mx has been reached'
         stop
      endif
      f_add(Nf_add,1)=1
      f_add(Nf_add,2)=Nv0+4
      f_add(Nf_add,3)=Nv0+1
c     Add face 9
      Nf_add=Nf_add+1
      if (Nf_add.gt.Nf_so_mx) then
         call error(label)
         write(*,*) 'Nf_so_mx has been reached'
         stop
      endif
      f_add(Nf_add,1)=Nv0+4
      f_add(Nf_add,2)=Nv0+3
      f_add(Nf_add,3)=Nv0+2
c     Add face 10
      Nf_add=Nf_add+1
      if (Nf_add.gt.Nf_so_mx) then
         call error(label)
         write(*,*) 'Nf_so_mx has been reached'
         stop
      endif
      f_add(Nf_add,1)=Nv0+4
      f_add(Nf_add,2)=Nv0+2
      f_add(Nf_add,3)=Nv0+1
c
      if ((direction.eq.3).and.(side.eq.2)) then
         invert_normal=.true.
      else
         invert_normal=.false.
      endif
      call add2sobj(dim,Nv_so,Nf_so,v_so,f_so,invert_normal,Nv_add,Nf_add,v_add,f_add)

      return
      end



      subroutine retrieve_original_coordinates(dim,Nv,Nf,vertices,faces,Npatch,Nv_patch,Nf_patch,v_patch,f_patch)
      implicit none
      include 'max.inc'
      include 'size_params.inc'
c     
c     Purpose: to replace coordinates of an object by original coordinates
c     The original object is composed of 4 points that define a rectangle, then a number
c     of positions that are supposed to draw "Npatch" closed loops.
c     Original coordinates of each point in each patch are in the _patch variables
c     
c     Input:
c       + dim: dimension of space
c       + Nv,Nf,vertices,faces: single object
c       + Npatch: number of patches
c       + *_patch: object that contains original coordinates
c     
c     Output:
c       + : modified
c     
c     I/O
      integer dim
      integer Nv,Nf
      double precision vertices(1:Nv_so_mx,1:Ndim_mx)
      integer faces(1:Nf_so_mx,1:Nvinface)
      integer Npatch
      integer Nv_patch(1:Npatch_mx)
      integer Nf_patch(1:Npatch_mx)
      double precision v_patch(1:Npatch_mx,1:Nv_so_mx,1:Ndim_mx)
      integer f_patch(1:Npatch_mx,1:Nf_so_mx,1:Nvinface)
c     temp
      integer i,j,ipatch,iv,ic
      integer Ncandidate
      double precision candidate(1:Nv_so_mx,1:Ndim_mx)
      double precision d
      double precision P1(1:Ndim_mx)
      double precision P2(1:Ndim_mx)
      double precision P3(1:Ndim_mx)
      logical identical_positions,identical,adjust_position
c     parameters
      double precision d_min
      parameter(d_min=1.0D-6)
c     label
      character*(Nchar_mx) label
      label='subroutine retrieve_original_coordinates'

      do i=1,Nv
         do j=1,dim
            P1(j)=vertices(i,j)
         enddo                  ! j
         Ncandidate=0
         do ipatch=1,Npatch
            do iv=1,Nv_patch(ipatch)
               do j=1,dim
                  P2(j)=v_patch(ipatch,iv,j)
               enddo            ! j               
               call distance(dim,P1,P2,d)
               if (d.lt.d_min) then
                  Ncandidate=Ncandidate+1
                  do j=1,dim
                     candidate(Ncandidate,j)=P2(j)
                  enddo         ! j
               endif            ! d < d_min
            enddo               ! iv
         enddo                  ! ipatch
c     The 4 first points should not have equivalents
         adjust_position=.false.
         if (i.gt.4) then
            if (Ncandidate.eq.1) then
               do j=1,dim
                  P2(j)=candidate(1,j)
               enddo            ! j
               adjust_position=.true.
            else if (Ncandidate.gt.1) then
c     If there are several candidates, they should all be identical positions
               identical_positions=.true.
               do j=1,dim
                  P2(j)=candidate(1,j)
               enddo            ! j
               do ic=2,Ncandidate
                  do j=1,dim
                     P3(j)=candidate(ic,j)
                  enddo         ! j
                  call identical_vectors(dim,P3,P3,identical)
                  if (.not.identical) then
                     identical_positions=.false.
                     goto 111
                  endif         ! identical=F
               enddo            ! ic
 111           continue
               if (identical_positions) then
                  adjust_position=.true.
               else
                  call error(label)
                  write(*,*) 'Position P1:',(P1(j),j=1,dim)
                  write(*,*) 'Has ',Ncandidate,' candidate replacements'
                  write(*,*) 'that are not identical positions:'
                  do ic=1,Ncandidate
                     write(*,*) ic,(candidate(ic,j),j=1,dim)
                  enddo         ! ic
                  stop
               endif
            endif               ! Ncandidate > 0
         endif                  ! i > 4
         if (adjust_position) then
            do j=1,dim
               vertices(i,j)=P2(j)
            enddo               ! j
         endif                  ! adjust_position
      enddo                     ! i

      return
      end
