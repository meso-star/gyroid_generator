c     Copyright (C) 2023 |Meso|Star> (contact@meso-star.com)
      subroutine detect_command(command,command_found)
      implicit none
      include 'max.inc'
      include 'formats.inc'
c     
c     Purpose: to find whether or not a given command is installed on the system
c     
c     Input:
c       + command: the command to find
c     
c     Output:
c       + command_found: T when 'command' is installed
c     
c     I/O
      character*(Nchar_mx) command
      logical command_found
c     temp
      character*(Nchar_mx) script_file
      character*(Nchar_mx) tfile
      character*(Nchar_mx) string
      integer ios,status
c     label
      character*(Nchar_mx) label
      label='subroutine detect_command'

      script_file='./find_command.bash'
      tfile='./temp'
c     create bash script
      open(12,file=trim(script_file))
      write(12,10) '#!/bin/bash'
      write(12,10) 'type '//trim(command)//' &>/dev/null'
      write(12,10) 'echo $? >'//trim(tfile)
      close(12)
      string='chmod u+x '//trim(script_file)
      call exec(string)
c     execute it
      string=trim(script_file)
      call exec(string)
c     read temporary result file
      open(11,file=trim(tfile),status='old',iostat=ios)
      if (ios.ne.0) then
         call error(label)
         write(*,*) 'File not found: ',trim(tfile)
         stop
      endif
      read(11,*) status
      close(11)
c     interpret result
      if (status.eq.0) then
         command_found=.true.
      else if (status.eq.1) then
         command_found=.false.
      else
         call error(label)
         write(*,*) 'status=',status
         stop
      endif
c     Debug
c      write(*,*) 'command found=',command_found
c      stop
c     Debug
     
c     remove temporary result file and script file
      string='rm -f '//trim(tfile)
      call exec(string)
      string='rm -f '//trim(script_file)
      call exec(string)

      return
      end
