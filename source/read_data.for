c     Copyright (C) 2023 |Meso|Star> (contact@meso-star.com)
      subroutine read_data(dim,datafile,force_remesh,force_duplication,L,Ninterval,sf,t,Lx,Ly,Lz,d,LC,hc,Tref,Trad)
      implicit none
      include 'max.inc'
c     
c     Purpose: to read the input data file
c     
c     Input:
c       + dim: dimension of space
c       + datafile: data file
c     
c     Output:
c       + force_remesh: T if remeshing has to be forced [T/F]
c       + force_duplication: T for forcing duplication of the main surface when filaments are detected [T/F]
c       + L: dimension of the domain over each direction [m]
c       + Ninterval: number of intervals for discretizing L
c       + sf: global scaling factor
c       + t: target
c       + Lx: value of Lx [m]
c       + Ly: value of Ly [m]
c       + Lz: value of Lz [m]
c       + d: thicness of the solid [m]
c       + LC: limit conditions for each face [K]
c       + hc: convective exchange coefficient [W.m⁻².K⁻¹]
c       + Tref: reference temperature [K]
c       + Tref: radiative temperature [K]
c     
c     I/O
      integer dim
      character*(Nchar_mx) datafile
      logical force_remesh
      logical force_duplication
      double precision L(1:Ndim_mx)
      integer Ninterval
      double precision sf
      double precision t
      double precision Lx
      double precision Ly
      double precision Lz
      double precision d
      double precision LC(1:Ndim_mx,1:2)
      double precision hc
      double precision Tref
      double precision Trad
c     temp
      integer i,j,ios
c     label
      character*(Nchar_mx) label
      label='subroutine read_data'

      open(11,file=trim(datafile),status='old',iostat=ios)
      if (ios.ne.0) then
         call error(label)
         write(*,*) 'File not found: ',trim(datafile)
         stop
      endif
      do i=1,3
         read(11,*)
      enddo                     ! i
      read(11,*) force_remesh
      read(11,*)
      read(11,*) force_duplication
      read(11,*)
      do i=1,dim
         read(11,*) L(i)
         read(11,*)
      enddo                     ! i
      read(11,*) Ninterval
      read(11,*)
      read(11,*) sf
      read(11,*)
      read(11,*) t
      read(11,*)
      read(11,*) Lx
      read(11,*)
      read(11,*) Ly
      read(11,*)
      read(11,*) Lz
      read(11,*)
      read(11,*) d
      do i=1,2
         read(11,*)
      enddo                     ! i
      do i=1,dim
         do j=1,2
            read(11,*)
            read(11,*) LC(i,j)  ! [K]
         enddo                  ! j
      enddo                     ! i
      do i=1,3
         read(11,*)
      enddo                     ! i
      read(11,*) hc             ! [W.m⁻².K⁻¹]
      read(11,*)
      read(11,*) Tref           ! [K]
      read(11,*)
      read(11,*) Trad           ! [K]
      close(11)

      do i=1,dim
         if (L(i).le.0.0D+0) then
            call error(label)
            write(*,*) 'L(',i,')=',L(i),' m'
            write(*,*) 'should be positive'
            stop
         endif
      enddo                     ! i
      if (sf.le.0.0D+0) then
         call error(label)
         write(*,*) 'Global scaling factor=',sf
         write(*,*) 'should be positive'
         stop
      endif
      if ((t.lt.-3.0D+0).or.(t.gt.3.0D+0)) then
         call error(label)
         write(*,*) 't=',t
         write(*,*) 'should be in the [-3, 3] range'
         stop
      endif
      if (Lx.le.0.0D+0) then
         call error(label)
         write(*,*) 'Lx=',Lx,' m'
         write(*,*) 'should be positive'
         stop
      endif
      if (Ly.le.0.0D+0) then
         call error(label)
         write(*,*) 'Ly=',Ly,' m'
         write(*,*) 'should be positive'
         stop
      endif
      if (Lz.le.0.0D+0) then
         call error(label)
         write(*,*) 'Lz=',Lz,' m'
         write(*,*) 'should be positive'
         stop
      endif
      if (d.le.0.0D+0) then
         call error(label)
         write(*,*) 'd=',d
         write(*,*) 'should be positive'
         stop
      endif
      do i=1,dim
         do j=1,2
            if ((LC(i,j).le.0.0D+0).and.(LC(i,j).ne.-1.0D+0)) then
               call error(label)
               write(*,*) 'LC(',i,',',j,')=',LC(i,j)
               stop
            endif
         enddo                  ! j
      enddo                     ! i
      if (hc.lt.0.0D+0) then
         call error(label)
         write(*,*) 'hc=',hc
         write(*,*) 'should be positive'
         stop
      endif
      if (Tref.le.0.0D+0) then
         call error(label)
         write(*,*) 'Tref=',Tref
         write(*,*) 'should be positive'
         stop
      endif
      if (Trad.le.0.0D+0) then
         call error(label)
         write(*,*) 'Trad=',Trad
         write(*,*) 'should be positive'
         stop
      endif

      return
      end
