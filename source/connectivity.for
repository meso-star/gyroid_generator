c     Copyright (C) 2023 |Meso|Star> (contact@meso-star.com)
      subroutine build_face_connectivity_table(dim,Nv,Nf,vertices,faces,ref_fct_file,Ncf,fct)
      implicit none
      include 'max.inc'
      include 'size_params.inc'
c     
c     Purpose: to build the connectivity table between faces
c     
c     Input:
c       + dim: dimension of space
c       + Nv, Nf, vertices, faces: definition of the object
c       + ref_fct_file: file used to record the fct
c     
c     Output:
c       + Ncf: number of connecting faces, per face
c       + fct: face connectivity table
c         fct(i,j), j=1,Ncf(i): indexes of the Ncf faces that connect to face i
c     
c     I/O
      integer dim
      integer Nv,Nf
      double precision vertices(1:Nv_mx,1:Ndim_mx)
      integer faces(1:Nf_mx,1:Nvinface)
      character*(Nchar_mx) ref_fct_file
      integer Ncf(1:Nf_mx)
      integer fct(1:Nf_mx,1:Ncf_mx)
c     temp
      integer iface,i,j
      integer Nface
      integer face_idx(1:Ncf_mx)
c     progress display
      integer len
      integer*8 ntot,ndone
      double precision fdone,fdone0
      integer ifdone,pifdone
      character*(Nchar_mx) str,source_str
      character*(Nchar_mx) fmt,fmt0
      logical err_found
c     label
      character*(Nchar_mx) label
      label='subroutine build_face_connectivity_table'

c     --- progress display
      ntot=Nf
      ndone=0
      pifdone=0
      len=6
      call num2str(len,str,err_found)
      if (err_found) then
         call error(label)
         write(*,*) 'Could not convert to str:'
         write(*,*) 'len=',len
         stop
      endif
      fmt0='(a,i'//trim(str)//',a)'
      fmt='(i'//trim(str)//',a)'
      fdone0=0.0D+0
      write(*,trim(fmt0),advance='no') 'Done:   ',floor(fdone0),' %'
c     progress display ---
      do iface=1,Nf
         call identify_adjacent_faces(dim,Nf,faces,iface,Nface,face_idx)
         Ncf(iface)=Nface
         if (Nface.gt.0) then
            do i=1,Nface
               fct(iface,i)=face_idx(i)
            enddo               ! i
         endif                  ! Nface > 0
c     --- progress display
         ndone=ndone+1
         fdone=dble(ndone)/dble(ntot)*1.0D+2
         ifdone=floor(fdone)
         if (ifdone.gt.pifdone) then
            do j=1,len+2
               write(*,"(a)",advance='no') "\b"
            enddo               ! j
            write(*,trim(fmt),advance='no') floor(fdone),' %'
            pifdone=ifdone
         endif
c     progress display ---
      enddo                     ! iface
      write(*,*)
      open(12,file=trim(ref_fct_file))
      write(12,*) Nf
      do iface=1,Nf
         write(12,*) Ncf(iface),(fct(iface,i),i=1,Ncf(iface))
      enddo                     ! iface
      close(12)
      write(*,*) 'File has been produced: ',trim(ref_fct_file)

      return
      end



      subroutine identify_adjacent_faces(dim,Nf,faces,face_index,Nface,face_idx)
      implicit none
      include 'max.inc'
      include 'size_params.inc'
c     
c     Purpose: to identify all faces that are adjacent to a given face
c     
c     Input:
c       + dim: dimension of space
c       + Nf, faces: definition of faces
c       + face_index: index of the face
c     
c     Output:
c       + Nface: number of faces that use node inde 'node_index'
c       + face_idx: indexes of these faces
c     
c     I/O
      integer dim
      integer Nf
      integer faces(1:Nf_mx,1:Nvinface)
      integer face_index
      integer Nface
      integer face_idx(1:Ncf_mx)
c     temp
      integer node_Nface
      integer node_face_idx(1:Ncf_mx)
      integer i,j,node_index,jface
      logical face_found
c     label
      character*(Nchar_mx) label
      label='subroutine identify_adjacent_faces'

      Nface=0
      do i=1,Nvinface
         node_index=faces(face_index,i)
         call identify_faces_using_node(dim,Nf,faces,node_index,node_Nface,node_face_idx)
c     each face that uses node "node_index" has to be checked whether or not is already belongs to the "face_idx" list
         do j=1,node_Nface
            face_found=.false.
            if (Nface.gt.0) then
               do jface=1,Nface
                  if (face_idx(jface).eq.node_face_idx(j)) then
                     face_found=.true.
                     goto 111
                  endif
               enddo            ! jface
            endif               ! Nface > 0
 111        continue
c     When face index "node_face_idx(j)" does not already belong to the "face_idx" list, it must be added
            if (.not.face_found) then
               Nface=Nface+1
               if (Nface.gt.Ncf_mx) then
                  call error(label)
                  write(*,*) 'Ncf_mx has been reached'
                  stop
               endif
               face_idx(Nface)=node_face_idx(j)
            endif               ! face_found=F               
         enddo                  ! j
      enddo                     ! i

      return
      end



      subroutine build_node_connectivity_table(dim,Nv,Nf,vertices,faces,ref_nct_file,Ncf,nct)
      implicit none
      include 'max.inc'
      include 'size_params.inc'
c     
c     Purpose: to build the Node Connectivity Table
c     
c     Input:
c       + dim: dimension of space
c       + Nv, Nf, vertices, faces: definition of the object
c       + ref_nct_file: file used to record the nct
c     
c     Output:
c       + Ncf: number of connecting faces, per node
c       + nct: node connectivity table
c         nct(i,j), j=1,Ncf(i): indexes of the Ncf faces that connect to node i
c     
c     I/O
      integer dim
      integer Nv,Nf
      double precision vertices(1:Nv_mx,1:Ndim_mx)
      integer faces(1:Nf_mx,1:Nvinface)
      character*(Nchar_mx) ref_nct_file
      integer Ncf(1:Nv_mx)
      integer nct(1:Nv_mx,1:Ncf_mx)
c     temp
      integer ivertex,i,j
      integer Nface
      integer face_idx(1:Ncf_mx)
c     progress display
      integer len
      integer*8 ntot,ndone
      double precision fdone,fdone0
      integer ifdone,pifdone
      character*(Nchar_mx) str,source_str
      character*(Nchar_mx) fmt,fmt0
      logical err_found
c     label
      character*(Nchar_mx) label
      label='subroutine build_node_connectivity_table'

c     --- progress display
      ntot=Nv
      ndone=0
      pifdone=0
      len=6
      call num2str(len,str,err_found)
      if (err_found) then
         call error(label)
         write(*,*) 'Could not convert to str:'
         write(*,*) 'len=',len
         stop
      endif
      fmt0='(a,i'//trim(str)//',a)'
      fmt='(i'//trim(str)//',a)'
      fdone0=0.0D+0
      write(*,trim(fmt0),advance='no') 'Done:   ',floor(fdone0),' %'
c     progress display ---
      do ivertex=1,Nv
         call identify_faces_using_node(dim,Nf,faces,ivertex,Nface,face_idx)
         Ncf(ivertex)=Nface
         if (Nface.gt.0) then
            do i=1,Nface
               nct(ivertex,i)=face_idx(i)
            enddo               ! i
         endif                  ! Nface > 0
c     --- progress display
         ndone=ndone+1
         fdone=dble(ndone)/dble(ntot)*1.0D+2
         ifdone=floor(fdone)
         if (ifdone.gt.pifdone) then
            do j=1,len+2
               write(*,"(a)",advance='no') "\b"
            enddo               ! j
            write(*,trim(fmt),advance='no') floor(fdone),' %'
            pifdone=ifdone
         endif
c     progress display ---
      enddo                     ! ivertex
      write(*,*)
      open(13,file=trim(ref_nct_file))
      write(13,*) Nv
      do ivertex=1,Nv
         write(13,*) Ncf(ivertex),(nct(ivertex,i),i=1,Ncf(ivertex))
      enddo                     ! ivertex
      close(13)
      write(*,*) 'File has been produced: ',trim(ref_nct_file)

      return
      end



      subroutine identify_faces_using_node(dim,Nf,faces,node_index,Nface,face_idx)
      implicit none
      include 'max.inc'
      include 'size_params.inc'
c     
c     Purpose: to identify all faces that use a given node
c     
c     Input:
c       + dim: dimension of space
c       + Nf, faces: definition of faces
c       + node_index: index of the node
c     
c     Output:
c       + N: number of faces that use node inde 'node_index'
c       + face_idx: indexes of these faces
c     
c     I/O
      integer dim
      integer Nf
      integer faces(1:Nf_mx,1:Nvinface)
      integer node_index
      integer Nface
      integer face_idx(1:Ncf_mx)
c     temp
      integer iface,j
      logical face_is_using_node
c     label
      character*(Nchar_mx) label
      label='subroutine identify_faces_using_node'

      Nface=0
      do iface=1,Nf
         face_is_using_node=.false.
         do j=1,Nvinface
            if (faces(iface,j).eq.node_index) then
               face_is_using_node=.true.
               goto 111
            endif               ! faces(iface,j)=node_index
         enddo                  ! j
 111     continue
         if (face_is_using_node) then
            Nface=Nface+1
            if (Nface.gt.Ncf_mx) then
               call error(label)
               write(*,*) 'Ncf_mx has been reached'
               stop
            endif
            face_idx(Nface)=iface
         endif                  ! face_is_using_node
      enddo                     ! iface

      return
      end
