      subroutine init()
      implicit none
      include 'max.inc'
c     
c     Purpose: initialization tasks
c     
c     I/O
c     temp
      character*(Nchar_mx) command,exec_file
      logical file_exists
c     label
      character*(Nchar_mx) label
      label='subroutine init'

c     remove previous obj files
      command='rm -f ./results/fluid*.obj'
      call exec(command)
      command='rm -f ./results/gyroid01.obj'
      call exec(command)
      command='rm -f ./results/interface*.obj'
      call exec(command)
      command='rm -f ./results/probe_position*.obj'
      call exec(command)
      command='rm -f ./results/solid_sole*.obj'
      call exec(command)
      
c     remove previous stl files
      command='rm -f ./results/*.stl'
      call exec(command)

c     Compilation of "triangle"
      write(*,*) 'Compilation of "triangle"...'
      exec_file='./triangle/triangle'
      command='rm -f '//trim(exec_file)
      call exec(command)
      command='cd ./triangle; ./compile_triangle.bash'
      call exec(command)
      inquire(file=trim(exec_file),exist=file_exists)
      if (.not.file_exists) then
         write(*,*) 'Compilation of TRIANGLE failed'
         stop
      else
         write(*,*) 'success'
      endif

      return
      end
