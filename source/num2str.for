c     Copyright (C) 2023 |Meso|Star> (contact@meso-star.com)
      subroutine num2str(num,str,err_code)
      implicit none
      include 'max.inc'
      include 'formats.inc'
c
c     Purpose: to convert an integer to a character string
c
c     Input:
c       + num: integer
c     
c     Output:
c       + str: character string
c       + err_code: false means conversion is OK; true means "num" was not recognized.
c
c     I/O
      integer num
      character*(Nchar_mx) str
      logical err_code
c     temp
      integer absnum,sign
      character*(Nchar_mx) f
c     label
      character*(Nchar_mx) label
      label='subroutine num2str'

      if (num.lt.0) then
         sign=-1
      else
         sign=1
      endif
      absnum=abs(num)
      
      err_code=.false.
      if ((absnum.ge.0).and.(absnum.lt.10)) then
         write(str,101) absnum
      else if ((absnum.ge.10).and.(absnum.lt.100)) then
         write(str,102) absnum
      else if ((absnum.ge.100).and.(absnum.lt.1000)) then
         write(str,103) absnum
      else if ((absnum.ge.1000).and.(absnum.lt.10000)) then
         write(str,104) absnum
      else if ((absnum.ge.10000).and.(absnum.lt.100000)) then
         write(str,105) absnum
      else if ((absnum.ge.100000).and.(absnum.lt.1000000)) then
         write(str,106) absnum
      else if ((absnum.ge.1000000).and.(absnum.lt.10000000)) then
         write(str,107) absnum
      else if ((absnum.ge.10000000).and.(absnum.lt.100000000)) then
         write(str,108) absnum
      else if ((absnum.ge.100000000).and.(absnum.lt.1000000000)) then
         write(str,109) absnum
      else
         err_code=.true.
         goto 666
      endif

      if (sign.eq.-1) then
         str="-"//trim(str)
      endif
      
 666  continue
      return
      end


      
      subroutine num2str_with_zeroes(num,str,err_code)
      implicit none
      include 'max.inc'
      include 'formats.inc'
c
c     Purpose: to convert an integer to a character string
c
c     Input:
c       + num: integer
c     
c     Output:
c       + str: character string
c       + err_code: false means conversion is OK; true means "num" was not recognized.
c
c     I/O
      integer num
      character*(Nchar_mx) str
      logical err_code
c     temp
      integer absnum,sign
      character*(Nchar_mx) f
c     label
      character*(Nchar_mx) label
      label='subroutine num2str_with_zeroes'

      if (num.lt.0) then
         sign=-1
      else
         sign=1
      endif
      absnum=abs(num)
      
      err_code=.false.
      if ((absnum.ge.0).and.(absnum.lt.10)) then
         write(str,101) absnum
         str='00000000'//trim(str)
      else if ((absnum.ge.10).and.(absnum.lt.100)) then
         write(str,102) absnum
         str='0000000'//trim(str)
      else if ((absnum.ge.100).and.(absnum.lt.1000)) then
         write(str,103) absnum
         str='000000'//trim(str)
      else if ((absnum.ge.1000).and.(absnum.lt.10000)) then
         write(str,104) absnum
         str='00000'//trim(str)
      else if ((absnum.ge.10000).and.(absnum.lt.100000)) then
         write(str,105) absnum
         str='0000'//trim(str)
      else if ((absnum.ge.100000).and.(absnum.lt.1000000)) then
         write(str,106) absnum
         str='000'//trim(str)
      else if ((absnum.ge.1000000).and.(absnum.lt.10000000)) then
         write(str,107) absnum
         str='00'//trim(str)
      else if ((absnum.ge.10000000).and.(absnum.lt.100000000)) then
         write(str,108) absnum
         str='0'//trim(str)
      else if ((absnum.ge.100000000).and.(absnum.lt.1000000000)) then
         write(str,109) absnum
      else
         err_code=.true.
         goto 666
      endif

      if (sign.eq.-1) then
         str="-"//trim(str)
      endif
      
 666  continue
      return
      end


      
      subroutine bignum2str(num,str,err_code)
      implicit none
      include 'max.inc'
      include 'formats.inc'
c
c     Purpose: to convert an integer to a character string
c
c     Input:
c       + num: integer
c     
c     Output:
c       + str: character string
c       + err_code: false means conversion is OK; true means "num" was not recognized.
c
c     I/O
      integer*8 num
      character*(Nchar_mx) str
      logical err_code
c     temp
      integer*8 absnum
      integer sign
      character*(Nchar_mx) f
c     label
      character*(Nchar_mx) label
      label='subroutine bignum2str'

      if (num.lt.0) then
         sign=-1
      else
         sign=1
      endif
      absnum=abs(num)
      
      err_code=.false.
      if ((absnum.ge.0).and.(absnum.lt.10)) then
         write(str,101) absnum
      else if ((absnum.ge.10).and.(absnum.lt.100)) then
         write(str,102) absnum
      else if ((absnum.ge.100).and.(absnum.lt.1000)) then
         write(str,103) absnum
      else if ((absnum.ge.1000).and.(absnum.lt.10000)) then
         write(str,104) absnum
      else if ((absnum.ge.10000).and.(absnum.lt.100000)) then
         write(str,105) absnum
      else if ((absnum.ge.100000).and.(absnum.lt.1000000)) then
         write(str,106) absnum
      else if ((absnum.ge.1000000).and.(absnum.lt.10000000)) then
         write(str,107) absnum
      else if ((absnum.ge.10000000).and.(absnum.lt.100000000)) then
         write(str,108) absnum
      else if ((absnum.ge.100000000).and.(absnum.lt.1000000000)) then
         write(str,109) absnum
      else
         err_code=.true.
         goto 666
      endif

      if (sign.eq.-1) then
         str="-"//trim(str)
      endif
      
 666  continue
      return
      end



      subroutine num2str1(num,str1,err_code)
      implicit none
      include 'max.inc'
      include 'formats.inc'
c
c     Purpose: to convert an integer to a character string of size 1
c
c     Input:
c       + num: integer
c     
c     Output:
c       + str2: character string
c       + err_code: false means conversion is OK; true means "num" was not recognized.
c
c     I/O
      integer num
      character*(Nchar_mx) str1
      logical err_code
c     temp
      character*1 kch1
c     label
      character*(Nchar_mx) label
      label='subroutine num2str1'

      if ((num.ge.0).and.(num.lt.10)) then
         write(kch1,11) num
         str1=trim(kch1)
         err_code=.false.
      else
         err_code=.true.
      endif

      return
      end



      subroutine num2str2(num,str2,err_code)
      implicit none
      include 'max.inc'
      include 'formats.inc'
c
c     Purpose: to convert an integer to a character string of size 2
c
c     Input:
c       + num: integer
c     
c     Output:
c       + str2: character string
c       + err_code: false means conversion is OK; true means "num" was not recognized.
c
c     I/O
      integer num
      character*(Nchar_mx) str2
      logical err_code
c     temp
      character*1 zeroch,kch1
      character*2 kch2
c     label
      character*(Nchar_mx) label
      label='subroutine num2str2'

      err_code=.false.
      write(zeroch,11) 0
      if ((num.ge.0).and.(num.lt.10)) then
         write(kch1,11) num
         str2=trim(zeroch)//trim(kch1)
      else if ((num.ge.10).and.(num.lt.100)) then
         write(kch2,12) num
         str2=trim(kch2)
      else
         err_code=.true.
      endif

      return
      end



      subroutine num2str3(num,str3,err_code)
      implicit none
      include 'max.inc'
c
c     Purpose: to convert an integer to a character string of size 3
c
c     Input:
c       + num: integer
c     
c     Output:
c       + str3: character string
c       + err_code: false means conversion is OK; true means "num" was not recognized.
c
c     I/O
      integer num
      character*(Nchar_mx) str3
      logical err_code
c     temp
      character*(Nchar_mx) str_tmp
c     label
      character*(Nchar_mx) label
      label='subroutine num2str3'

      call num2str(num,str_tmp,err_code)
      if (.not.err_code) then
         if (num.lt.10) then
            str3='00'//trim(str_tmp)
         else if (num.lt.100) then
            str3='0'//trim(str_tmp)
         else if (num.lt.1000) then
            str3=trim(str_tmp)
         else
            call error(label)
            write(*,*) 'Bad input argument:'
            write(*,*) 'num=',num
            write(*,*) 'should be < 1000'
            stop
         endif
      endif

      return
      end



      subroutine num2str4(num,str4,err_code)
      implicit none
      include 'max.inc'
c
c     Purpose: to convert an integer to a character string of size 4
c
c     Input:
c       + num: integer
c     
c     Output:
c       + str4: character string
c       + err_code: false means conversion is OK; true means "num" was not recognized.
c

c     I/O
      integer num
      character*(Nchar_mx) str4
      logical err_code
c     temp
      character*(Nchar_mx) str_tmp
c     label
      character*(Nchar_mx) label
      label='subroutine num2str4'
c
      call num2str(num,str_tmp,err_code)
c      
      if (.not.err_code) then
         if (num.lt.10) then
            str4='000'//trim(str_tmp)
         else if (num.lt.100) then
            str4='00'//trim(str_tmp)
         else if (num.lt.1000) then
            str4='0'//trim(str_tmp)
         else if (num.lt.10000) then
            str4=trim(str_tmp)
         else
            call error(label)
            write(*,*) 'Bad input argument:'
            write(*,*) 'num=',num
            write(*,*) 'should be < 10000'
            stop
         endif
      endif

      return
      end


      
      subroutine dble2str(num,nap,str,err)
      implicit none
      include 'max.inc'
      include 'formats.inc'
c
c     Purpose: to convert a double precision to a character string;
c     the output character string uses a "+/-0.XXXXE+/-N" convention
c
c     Input:
c       + num: double precision
c       + nap: number of digits after the "." that are converted to character
c     
c     Output:
c       + str: character string
c       + err: a value of zero means conversion is OK; a value of 1 means "num"
c                was not recognized.
c

c     I/O
      double precision num
      integer nap
      character*(Nchar_mx) str
      logical err
c     temp
      integer sign
      integer i,j,idx,t
      character*1 str1
      character*(Nchar_mx) str3
      integer n
      double precision absnum,tmp,a
      character*(Nchar_mx) nstr
      logical err_code
c     label
      character*(Nchar_mx) label
      label='subroutine dble2str'

      if (num.eq.0.0D+0) then
         str='0.00'
         err=.false.
         goto 666
      endif

      if (num.lt.0.0D+0) then
         sign=-1
      else
         sign=1
      endif
      absnum=dabs(num)
      
      err=.false.
      n=0
      tmp=absnum
      if (absnum.gt.1.0D+0) then
         do while (tmp.gt.1.0D+0)
            n=n+1
            tmp=tmp/1.0D+1
c     Debug
c            write(*,*) tmp,n
c     Debug
         enddo
      else if (absnum.lt.1.0D+0) then
         do while (tmp.lt.1.0D+0)
            n=n-1
            tmp=tmp*1.0D+1
         enddo
      else if (absnum.eq.1.0D+0) then
      else
         call error(label)
         write(*,*) 'absnum=',absnum
         stop
      endif
      a=tmp
c     at this point, absnum=a*10^n
c     Debug
c      write(*,*) 'a=',a
c      write(*,*) 'n=',n
c     Debug

      str=''
      idx=int(a)
      write(str1,11) idx
      t=idx*10
      str=trim(str)//trim(str1)
      str=trim(str)//'.'
      do i=1,nap
         idx=int(a*10**i-t)
c     Debug
c         write(*,*) 'i=',i,' idx=',idx
c     Debug
         t=10*(t+idx)
         write(str1,11) idx
         str=trim(str)
     &        //trim(str1)
      enddo ! i
c     Debug
c      write(*,*) 'str=',trim(str)
c     Debug

      if (n.ne.0) then
         call num2str3(abs(n),str3,err_code)
         if (err_code) then
            call error(label)
            write(*,*) 'Could not convert to character: n=',abs(n)
            stop
         endif
         if (n.lt.0) then
            nstr='-'//trim(str3)
         else
            nstr=trim(str3)
         endif
         str=trim(str)
     &        //'E'
     &        //trim(nstr)
c     &        //'}'
      endif

      if (sign.eq.-1) then
         str="-"//trim(str)
      endif
      
 666  continue
      return
      end



      subroutine dble2str_noexp(num,nap,str,err)
      implicit none
      include 'max.inc'
      include 'formats.inc'
c
c     Purpose: to convert a double precision to a character string
c     using a natural "+/-XXXX.XXXXX" convention
c
c     Input:
c       + num: double precision
c       + nap: number of digits after the "." that are converted to character
c     
c     Output:
c       + str: character string
c       + err: a value of zero means conversion is OK; a value of 1 means "num"
c                was not recognized.
c

c     I/O
      double precision num
      integer nap
      character*(Nchar_mx) str
      logical err
c     temp
      integer sign
      integer i,j,idx,t
      character*1 str1,zero_ch
      character*3 str3
      integer n
      double precision absnum,tmp,a
      character*(Nchar_mx) nstr
c     label
      character*(Nchar_mx) label
      label='subroutine dble2str_noexp'

      if (num.eq.0.0D+0) then
         str='0.00'
         err=.false.
         goto 666
      endif

      if (num.lt.0.0D+0) then
         sign=-1
      else
         sign=1
      endif
      absnum=dabs(num)
      
      err=.false.
      n=0
      tmp=absnum
      write(zero_ch,11) 0
      if (absnum.gt.1.0D+0) then
         do while (tmp.ge.1.0D+0)
            n=n+1
            tmp=tmp/1.0D+1
c     Debug
c            write(*,*) tmp,n
c     Debug
         enddo
         n=n-1
      else if (absnum.lt.1.0D+0) then
         do while (tmp.lt.1.0D+0)
            n=n-1
            tmp=tmp*1.0D+1
         enddo
      else if (absnum.eq.1.0D+0) then
      else
         call error(label)
         write(*,*) 'absnum=',absnum
         stop
      endif
      a=tmp
c     at this point, absnum=a*10^n
c     Debug
c      write(*,*) 'a=',a
c      write(*,*) 'n=',n
c     Debug

      str=''
      if (n.ge.0) then
         tmp=absnum
         do i=n,0,-1
            idx=tmp/(10**i)
            tmp=tmp-idx*10**i
            write(str1,11) idx
            str=trim(str)
     &           //trim(str1)
         enddo                  ! i
         str=trim(str)
     &        //'.'
         do i=1,nap
            tmp=tmp*10
            idx=int(tmp)
            tmp=tmp-dble(idx)
            write(str1,11) idx
            str=trim(str)//trim(str1)
         enddo ! i
      else
         str=trim(zero_ch)//'.'
         do i=1,abs(n+1)
            str=trim(str)
     &           //trim(zero_ch)
         enddo ! i
         tmp=absnum*10**abs(n+1)
         do i=1,nap
            tmp=tmp*10
            idx=int(tmp)
            tmp=tmp-dble(idx)
            write(str1,11) idx
            str=trim(str)//trim(str1)
         enddo ! i
      endif

      if (sign.eq.-1) then
         str="-"//trim(str)
      endif
      
 666  continue
      return
      end
