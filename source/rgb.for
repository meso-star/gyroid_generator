c     Copyright (C) 2023 |Meso|Star> (contact@meso-star.com)
      subroutine read_rgb_codes(datafile,Ncode,mat_label,rgb_code)
      implicit none
      include 'max.inc'
c     
c     Purpose: to read RGB codes associated to various materials / media
c     
c     Input:
c       + datafile: file to read
c     
c     Output:
c       + Ncode: number of codes
c       + mat_label: name of each material
c       + rgb_code: RGB code for each material
c     
c     I/O
      character*(Nchar_mx) datafile
      integer Ncode
      character*(Nchar_mx) mat_label(1:Ncode_mx)
      integer rgb_code(1:Ncode_mx,1:3)
c     temp
      integer i,j,ios,nl
c     label
      character*(Nchar_mx) label
      label='subroutine read_rgb_codes'

      open(12,file=trim(datafile),status='old',iostat=ios)
      if (ios.ne.0) then
         call error(label)
         write(*,*) 'File not found:'
         write(*,*) trim(datafile)
         stop
      else
         call get_nlines(datafile,nl)
         Ncode=nl-1
         if (Ncode.lt.1) then
            call error(label)
            write(*,*) 'Ncode=',Ncode
            write(*,*) 'should be > 0'
            stop
         else if (Ncode.gt.Ncode_mx) then
            call error(label)
            write(*,*) 'Ncode=',Ncode
            write(*,*) 'is > Ncode_mx=',Ncode_mx
            stop
         else
            read(12,*)
            do i=1,Ncode
               read(12,*) mat_label(i),(rgb_code(i,j),j=1,3)
            enddo               ! i
         endif                  ! incorrect value for Ncode
      endif                     ! ios
      close(12)

      return
      end



      subroutine retrieve_code(Ncode,mat_label,rgb_code,name,color_code)
      implicit none
      include 'max.inc'
c     
c     Purpose: to identify the RGB color code for a given material/medium
c     
c     Input:
c       + Ncode: number of codes
c       + mat_label: name of each material
c       + rgb_code: RGB code for each material
c       + name: identifier of the material/medium
c     
c     Output:
c       + color_code: RGB color code
c     
c     I/O
      integer Ncode
      character*(Nchar_mx) mat_label(1:Ncode_mx)
      integer rgb_code(1:Ncode_mx,1:3)
      character*(Nchar_mx) name
      double precision color_code(1:3)
c     temp
      logical found
      integer index,i
c     label
      character*(Nchar_mx) label
      label='subroutine retrieve_code'

      call code_index(Ncode,mat_label,name,found,index)
      if (found) then
         do i=1,3
            color_code(i)=dble(rgb_code(index,i))/255.0D+0
         enddo                  ! i
      else
         call error(label)
         write(*,*) 'material: ',trim(name)
         write(*,*) 'was not identified in the "mat_label" list'
         stop
      endif                     ! found

      return
      end
      


      subroutine code_index(Ncode,mat_label,name,found,index)
      implicit none
      include 'max.inc'
c     
c     Purpose: to identify a material/medium in the list of RGB color codes
c     
c     Input:
c       + Ncode: number of codes
c       + mat_label: name of each material
c       + name: name of the material/medium
c     
c     Output:
c       + found: true if name was found in the "mat_label" list
c       + index: index in the list for found=T
c
c     I/O
      integer Ncode
      character*(Nchar_mx) mat_label(1:Ncode_mx)
      character*(Nchar_mx) name
      logical found
      integer index
c     temp
      integer i
c     label
      character*(Nchar_mx) label
      label='subroutine code_index'

      found=.false.
      do i=1,Ncode
         if (trim(name).eq.trim(mat_label(i))) then
            found=.true.
            index=i
            goto 111
         endif
      enddo                     ! i
 111  continue

      return
      end
