c     Copyright (C) 2023 |Meso|Star> (contact@meso-star.com)
      subroutine read_fluid_properties(datafile,rho,C,Tinit,Tfluid)
      implicit none
      include 'max.inc'
c     
c     Purpose: to read solid properties
c     
c     Input:
c       + datafile: file to read
c     
c     Output:
c       + rho: volumic mass [kg.m⁻³]
c       + C: specific heat [J.kg⁻¹.K⁻¹]
c       + Tinit: initial temperature [K]
c       + Tfluid: temperature of fluid for t>0 [K]
c     
c     I/O
      character*(Nchar_mx) datafile
      double precision rho
      double precision C
      double precision Tinit
      double precision Tfluid
c     temp
      integer i,ios
c     label
      character*(Nchar_mx) label
      label='subroutine read_fluid_properties'

      open(12,file=trim(datafile),status='old',iostat=ios)
      if (ios.ne.0) then
         call error(label)
         write(*,*) 'File not found: ',trim(datafile)
         stop
      else
         write(*,*) 'Reading file: ',trim(datafile)
      endif
      do i=1,2
         read(12,*)
      enddo                     ! i
      read(12,*) rho
      read(12,*) 
      read(12,*) C
      read(12,*) 
      read(12,*) Tinit
      read(12,*) 
      read(12,*) Tfluid
      close(12)

      if (rho.le.0.0D+0) then
         call error(label)
         write(*,*) 'rho=',rho
         write(*,*) 'should be positive'
         stop
      endif
      if (C.le.0.0D+0) then
         call error(label)
         write(*,*) 'C=',C
         write(*,*) 'should be positive'
         stop
      endif
      if (Tinit.le.0.0D+0) then
         call error(label)
         write(*,*) 'Tinit=',Tinit
         write(*,*) 'should be positive'
         stop
      endif
      if (Tfluid.le.0.0D+0) then
         call error(label)
         write(*,*) 'Tfluid=',Tfluid
         write(*,*) 'should be positive'
         stop
      endif

      return
      end
