      subroutine find_character(str,char,Nchar,idx)
      implicit none
      include 'max.inc'
c     
c     Purpose: to identify every occurence of a given character in a character string
c     
c     Input:
c       + str: character string
c       + char: character to identify
c     
c     Output:
c       + Nchar: number of occurences found
c       + idx: index of every occurence
c     
c     I/O
      character*(Nchar_mx) str
      character*1 char
      integer Nchar
      integer idx(1:Nchar_mx)
c     temp
      integer i
c     label
      character*(Nchar_mx) label
      label='subroutine find_character'

      Nchar=0
      do i=1,len_trim(str)
         if (str(i:i).eq.char(1:1)) then
            Nchar=Nchar+1
            if (Nchar.gt.Nchar_mx) then
               call error(label)
               write(*,*) 'Nchar_mx has been reached'
               stop
            endif
            idx(Nchar)=i
         endif
      enddo                     ! i
      
      return
      end
