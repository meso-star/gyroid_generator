c     Copyright (C) 2023 |Meso|Star> (contact@meso-star.com)
      subroutine read_probe_times(datafile,dim,Ntime,probe_time)
      implicit none
      include 'max.inc'
c     
c     Purpose: read time positions from file
c     
c     Input:
c       + datafile: input data file
c       + dim: dimension of space
c     
c     Output:
c       + Ntime: number of time positions
c       + probe_time: list of time positions
c     
c     I/O
      character*(Nchar_mx) datafile
      integer dim
      integer Ntime
      double precision probe_time(1:Ntime_mx)
c     temp
      integer i,j,ios,read_ios
      logical keep_reading
      double precision tmp
c     label
      character*(Nchar_mx) label
      label='subroutine read_probe_time'

      open(11,file=trim(datafile),status='old',iostat=ios)
      if (ios.ne.0) then
         call error(label)
         write(*,*) 'File not found: ',trim(datafile)
         stop
      else
         write(*,*) 'Reading file: ',trim(datafile)
         do i=1,3
            read(11,*)
         enddo                  ! i
         Ntime=0
         keep_reading=.true.
         do while (keep_reading)
            read(11,*,iostat=read_ios) tmp
            if (read_ios.eq.0) then
               Ntime=Ntime+1
               if (Ntime.gt.Ntime_mx) then
                  call error(label)
                  write(*,*) 'Ntime_mx has been reached'
                  stop
               endif
               probe_time(Ntime)=tmp
            else
               keep_reading=.false.
            endif
         enddo                  ! while (keep_reading)
      endif
      close(11)

c     consistency
      do i=1,Ntime
         if (probe_time(i).le.0.0D+0) then
            call error(label)
            write(*,*) 'probe_time(',i,')=',probe_time(i)
            write(*,*) 'should be > 0'
            stop
         endif
      enddo                     ! i

      return
      end
