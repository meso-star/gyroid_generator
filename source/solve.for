c     Copyright (C) 2023 |Meso|Star> (contact@meso-star.com)
      subroutine cube_gyroid_intersection(dim,Lx,Ly,Lz,range,t,exit_status,Nintersection,intersection)
      implicit none
      include 'max.inc'
c     
c     Purpose: to solve the intersections between the gyroid and a cube
c     
c     Input:
c       + dim: dimension of space
c       + Lx: value of Lx [m]
c       + Ly: value of Ly [m]
c       + Lz: value of Lz [m]
c       + range: range over each direction [m]
c       + t: target
c     
c     Output:
c       + exit_status: status
c         - exit_status=0: at most 1 intersection per edge
c         - exit_status=1: error: more than 1 intersection per edge
c       + Nintersection: number of intersections over each direction
c       + intersection: coordinates of the intersection, for each direction
c     
c     I/O
      integer dim
      double precision Lx,Ly,Lz
      double precision t
      double precision range(1:Ndim_mx,1:Ndim_mx-1)
      integer exit_status
      integer Nintersection(1:4*Ndim_mx)
      double precision intersection(1:4*Ndim_mx,1:Ndim_mx)
c     temp
      integer i,j
      integer solve_for
      double precision sigma1
      double precision sigma2
      double precision sigma3_range(1:Ndim_mx-1)
      double precision errf
      integer Nsol
      double precision sigma3(1:Nsol_mx)
c     label
      character*(Nchar_mx) label
      label='subroutine cube_gyroid_intersection'

      errf=1.0D-8
      
      do i=1,4*dim
         Nintersection(i)=0
      enddo                     ! i
      exit_status=0
c     ---------------------------------------------------------------
c     Edges 1-4: solving for x
      solve_for=1
      sigma3_range(1)=range(1,1)
      sigma3_range(2)=range(1,2)
c     solving for edge 1: x in [x_min, x_max], y=y_min, z=z_min
      sigma1=range(2,1)
      sigma2=range(3,1)
      call solve_gyroid(dim,Lx,Ly,Lz,solve_for,sigma1,sigma2,sigma3_range,t,errf,Nsol,sigma3)
      if (Nsol.eq.1) then
         Nintersection(1)=1
         intersection(1,1)=sigma3(1)
         intersection(1,2)=sigma1
         intersection(1,3)=sigma2
      else if (Nsol.gt.1) then
         exit_status=1
         goto 666
      endif
c     solving for edge 2: x in [x_min, x_max], y=y_max, z=z_min
      sigma1=range(2,2)
      sigma2=range(3,1)
      call solve_gyroid(dim,Lx,Ly,Lz,solve_for,sigma1,sigma2,sigma3_range,t,errf,Nsol,sigma3)
      if (Nsol.eq.1) then
         Nintersection(2)=1
         intersection(2,1)=sigma3(1)
         intersection(2,2)=sigma1
         intersection(2,3)=sigma2
      else if (Nsol.gt.1) then
         exit_status=1
         goto 666
      endif
c     solving for edge 3: x in [x_min, x_max], y=y_min, z=z_max
      sigma1=range(2,1)
      sigma2=range(3,2)
      call solve_gyroid(dim,Lx,Ly,Lz,solve_for,sigma1,sigma2,sigma3_range,t,errf,Nsol,sigma3)
      if (Nsol.eq.1) then
         Nintersection(3)=1
         intersection(3,1)=sigma3(1)
         intersection(3,2)=sigma1
         intersection(3,3)=sigma2
      else if (Nsol.gt.1) then
         exit_status=1
         goto 666
      endif
c     solving for edge 4: x in [x_min, x_max], y=y_max, z=z_max
      sigma1=range(2,2)
      sigma2=range(3,2)
      call solve_gyroid(dim,Lx,Ly,Lz,solve_for,sigma1,sigma2,sigma3_range,t,errf,Nsol,sigma3)
      if (Nsol.eq.1) then
         Nintersection(4)=1
         intersection(4,1)=sigma3(1)
         intersection(4,2)=sigma1
         intersection(4,3)=sigma2
      else if (Nsol.gt.1) then
         exit_status=1
         goto 666
      endif
c     ---------------------------------------------------------------
c     Edges 5-8: solving for y
      solve_for=2
      sigma3_range(1)=range(2,1)
      sigma3_range(2)=range(2,2)
c     solving for edge 5: x=x_min, y in [y_min, y_max], z=z_min
      sigma1=range(1,1)
      sigma2=range(3,1)
      call solve_gyroid(dim,Lx,Ly,Lz,solve_for,sigma1,sigma2,sigma3_range,t,errf,Nsol,sigma3)
      if (Nsol.eq.1) then
         Nintersection(5)=1
         intersection(5,1)=sigma1
         intersection(5,2)=sigma3(1)
         intersection(5,3)=sigma2
      else if (Nsol.gt.1) then
         exit_status=1
         goto 666
      endif
c     solving for edge 6: x=x_max, y in [y_min, y_max], z=z_min
      sigma1=range(1,2)
      sigma2=range(3,1)
      call solve_gyroid(dim,Lx,Ly,Lz,solve_for,sigma1,sigma2,sigma3_range,t,errf,Nsol,sigma3)
      if (Nsol.eq.1) then
         Nintersection(6)=1
         intersection(6,1)=sigma1
         intersection(6,2)=sigma3(1)
         intersection(6,3)=sigma2
      else if (Nsol.gt.1) then
         exit_status=1
         goto 666
      endif
c     solving for edge 7: x=x_min, y in [y_min, y_max], z=z_max
      sigma1=range(1,1)
      sigma2=range(3,2)
      call solve_gyroid(dim,Lx,Ly,Lz,solve_for,sigma1,sigma2,sigma3_range,t,errf,Nsol,sigma3)
      if (Nsol.eq.1) then
         Nintersection(7)=1
         intersection(7,1)=sigma1
         intersection(7,2)=sigma3(1)
         intersection(7,3)=sigma2
      else if (Nsol.gt.1) then
         exit_status=1
         goto 666
      endif
c     solving for edge 8: x=x_max, y in [y_min, y_max], z=z_max
      sigma1=range(1,2)
      sigma2=range(3,2)
      call solve_gyroid(dim,Lx,Ly,Lz,solve_for,sigma1,sigma2,sigma3_range,t,errf,Nsol,sigma3)
      if (Nsol.eq.1) then
         Nintersection(8)=1
         intersection(8,1)=sigma1
         intersection(8,2)=sigma3(1)
         intersection(8,3)=sigma2
      else if (Nsol.gt.1) then
         exit_status=1
         goto 666
      endif
c     ---------------------------------------------------------------
c     Edges 9-12: solving for z
      solve_for=3
      sigma3_range(1)=range(3,1)
      sigma3_range(2)=range(3,2)
c     solving for edge 9: x=x_min, y=y_min, z in [z_min, z_max]
      sigma1=range(1,1)
      sigma2=range(2,1)
      call solve_gyroid(dim,Lx,Ly,Lz,solve_for,sigma1,sigma2,sigma3_range,t,errf,Nsol,sigma3)
      if (Nsol.eq.1) then
         Nintersection(9)=1
         intersection(9,1)=sigma1
         intersection(9,2)=sigma2
         intersection(9,3)=sigma3(1)
      else if (Nsol.gt.1) then
         exit_status=1
         goto 666
      endif
c     solving for edge 10: x=x_max, y=y_min, z in [z_min, z_max]
      sigma1=range(1,2)
      sigma2=range(2,1)
      call solve_gyroid(dim,Lx,Ly,Lz,solve_for,sigma1,sigma2,sigma3_range,t,errf,Nsol,sigma3)
      if (Nsol.eq.1) then
         Nintersection(10)=1
         intersection(10,1)=sigma1
         intersection(10,2)=sigma2
         intersection(10,3)=sigma3(1)
      else if (Nsol.gt.1) then
         exit_status=1
         goto 666
      endif
c     solving for edge 11: x=x_min, y=y_max, z in [z_min, z_max]
      sigma1=range(1,1)
      sigma2=range(2,2)
      call solve_gyroid(dim,Lx,Ly,Lz,solve_for,sigma1,sigma2,sigma3_range,t,errf,Nsol,sigma3)
      if (Nsol.eq.1) then
         Nintersection(11)=1
         intersection(11,1)=sigma1
         intersection(11,2)=sigma2
         intersection(11,3)=sigma3(1)
      else if (Nsol.gt.1) then
         exit_status=1
         goto 666
      endif
c     solving for edge 12: x=x_max, y=y_max, z in [z_min, z_max]
      sigma1=range(1,2)
      sigma2=range(2,2)
      call solve_gyroid(dim,Lx,Ly,Lz,solve_for,sigma1,sigma2,sigma3_range,t,errf,Nsol,sigma3)
      if (Nsol.eq.1) then
         Nintersection(12)=1
         intersection(12,1)=sigma1
         intersection(12,2)=sigma2
         intersection(12,3)=sigma3(1)
      else if (Nsol.gt.1) then
         exit_status=1
         goto 666
      endif
c     ---------------------------------------------------------------
      
 666  continue
      
      return
      end



      subroutine solve_gyroid(dim,Lx,Ly,Lz,solve_for,sigma1,sigma2,sigma3_range,t,errf,Nsol,sigma3)
      implicit none
      include 'max.inc'
c     
c     Purpose: to solve the implicit gyroid relation
c     
c     Input:
c       + dim: dimension of space
c       + Lx: value of Lx [m]
c       + Ly: value of Ly [m]
c       + Lz: value of Lz [m]
c       + solve_for: integer that specifies which coordinate the implicit equation is solved for:
c         - solve_for=1: (sigma1, sigma2)=(y, z) and the equation is solved for sigma3=x
c         - solve_for=2: (sigma1, sigma2)=(x, z) and the equation is solved for sigma3=y
c         - solve_for=3: (sigma1, sigma2)=(x, y) and the equation is solved for sigma3=z
c       + sigma1: value of the first coordinate [m]
c       + sigma2: value of the second coordiante [m]
c       + sigma3_range: range of third coordinate [m]
c       + t: target
c       + errf: required numerical accuracy over f [m]
c     
c     Output:
c       + Nsol: number of identified solutions
c       + sigma3: corresponding values of the third coordinate
c     
c     I/O
      integer dim
      double precision Lx,Ly,Lz
      integer solve_for
      double precision sigma1
      double precision sigma2
      double precision sigma3_range(1:Ndim_mx-1)
      double precision t
      double precision errf
      integer Nsol
      double precision sigma3(1:Nsol_mx)
c     temp
      double precision x(1:Ndim_mx)
      logical solution_found
      logical keep_running,keep_searching
      integer Niter
      double precision sigma3_min,sigma3_max,sigma3_mid
      double precision g_min,g_max,g
      logical range_found
      double precision dsigma3
      integer Ninterval,interval
c     functions
      double precision gyroid
c     label
      character*(Nchar_mx) label
      label='subroutine solve_gyroid'
c
      if ((solve_for.lt.1).or.(solve_for.gt.dim)) then
         call error(label)
         write(*,*) 'solve_for=',solve_for
         write(*,*) 'should be in the [1, ',dim,'] range'
         stop
      endif
      if (sigma3_range(2).le.sigma3_range(1)) then
         call error(label)
         write(*,*) 'sigma3_range(2)=',sigma3_range(2)
         write(*,*) 'should be > sigma3_range(1)=',sigma3_range(1)
         stop
      endif
      Ninterval=100
      dsigma3=(sigma3_range(2)-sigma3_range(1))/dble(Ninterval)
      Nsol=0
c
      sigma3_min=sigma3_range(1)
      interval=0
      keep_searching=.true.
      do while (keep_searching)
         interval=interval+1
         sigma3_min=sigma3_range(1)+dsigma3*(interval-1)
         sigma3_max=sigma3_range(1)+dsigma3*interval
c     evaluation of g(sigma3_min)
         if (solve_for.eq.1) then
            x(1)=sigma3_min
            x(2)=sigma1
            x(3)=sigma2
         else if (solve_for.eq.2) then
            x(1)=sigma1
            x(2)=sigma3_min
            x(3)=sigma2
         else if (solve_for.eq.3) then
            x(1)=sigma1
            x(2)=sigma2
            x(3)=sigma3_min
         endif
         g_min=gyroid(dim,Lx,Ly,Lz,x,t)
c     evaluation of g(sigma3_max)
         if (solve_for.eq.1) then
            x(1)=sigma3_max
            x(2)=sigma1
            x(3)=sigma2
         else if (solve_for.eq.2) then
            x(1)=sigma1
            x(2)=sigma3_max
            x(3)=sigma2
         else if (solve_for.eq.3) then
            x(1)=sigma1
            x(2)=sigma2
            x(3)=sigma3_max
         endif
         g_max=gyroid(dim,Lx,Ly,Lz,x,t)
c     
         if (g_min*g_max.le.0.0D+0) then
            Niter=0
            keep_running=.true.
            solution_found=.false.
            do while (keep_running)
               sigma3_mid=(sigma3_min+sigma3_max)/2.0D+0
               if (solve_for.eq.1) then
                  x(1)=sigma3_mid
                  x(2)=sigma1
                  x(3)=sigma2
               else if (solve_for.eq.2) then
                  x(1)=sigma1
                  x(2)=sigma3_mid
                  x(3)=sigma2
               else if (solve_for.eq.3) then
                  x(1)=sigma1
                  x(2)=sigma2
                  x(3)=sigma3_mid
               endif
               g=gyroid(dim,Lx,Ly,Lz,x,t)
               if (g_min*g.le.0.0D+0) then
                  sigma3_max=sigma3_mid
                  g_max=g
               else
                  sigma3_min=sigma3_mid
                  g_min=g
               endif
               if (dabs(g).le.errf) then
                  keep_running=.false.
                  solution_found=.true.
               endif
               if (Niter.gt.Niter_mx) then
                  keep_running=.false.
               endif
            enddo               ! while (keep_running)
            if (solution_found) then
               Nsol=Nsol+1
               if (Nsol.gt.Nsol_mx) then
                  call error(label)
                  write(*,*) 'Nsol_mx has been reached'
                  stop
               endif
               sigma3(Nsol)=sigma3_mid
c     Debug
               if ((sigma3(Nsol).lt.sigma3_range(1)).or.(sigma3(Nsol).gt.sigma3_range(2))) then
                  call error(label)
                  write(*,*) 'sigma3(',Nsol,')=',sigma3(Nsol)
                  write(*,*) 'should be in [',sigma3_range(1),', ',sigma3_range(2),'] range'
                  write(*,*) 'sigma3_min=',sigma3_min
                  write(*,*) 'sigma3_max=',sigma3_max
                  write(*,*) 'dsigma3=',dsigma3
                  write(*,*) 'interval=',interval
               endif
c     Debug
            endif               ! solution_found
         endif                  ! g_zmin*g_zmax<0
         if (interval.ge.Ninterval) then
            keep_searching=.false.
         endif
      enddo                     ! keep_searching
      
      return
      end

      

      double precision function gyroid(dim,Lx,Ly,Lz,x,t)
      implicit none
      include 'max.inc'
      include 'param.inc'
c     
c     Purpose: to obtain the value of the implicit gyroid equation
c     
c     Input:
c       + dim: dimension of space
c       + Lx: value of Lx [m]
c       + Ly: value of Ly [m]
c       + Lz: value of Lz [m]
c       + x: value of the position [m]
c       + t: target
c     
c     Output:
c       + g: gyroid function
c     
c     I/O
      integer dim
      double precision Lx,Ly,Lz
      double precision x(1:Ndim_mx)
      double precision t
c     temp
c     label
      character*(Nchar_mx) label
      label='function gyroid'

      gyroid=dsin(2.0D+0*pi*x(1)/Lx)*dcos(2.0D+0*pi*x(2)/Ly)+dsin(2.0D+0*pi*x(2)/Ly)*dcos(2.0D+0*pi*x(3)/Lz)+dsin(2.0D+0*pi*x(3)/Lz)*dcos(2.0D+0*pi*x(1)/Lx)-t

      return
      end
