c     Copyright (C) 2023 |Meso|Star> (contact@meso-star.com)
      subroutine record_obj(dim,obj_file,display,invert_normal,Nv,Nf,vertices,faces,scaling_factor,mtllib_file,Nmat,materials_list,material_index)
      implicit none
      include 'max.inc'
      include 'formats.inc'
      include 'param.inc'
      include 'size_params.inc'
c
c     Purpose: to record a trianglemesh to a wavefront file
c
c     Input:
c       + dim: dimension of the physical space
c       + obj_file: name of the output wavefront file
c       + display: T if a info line should be written to display
c       + invert_normal: true if the direction of normals has to be inverted
c       + Nv: number of vertices
c       + Nf: number of faces
c       + vertices: array of verices definition (3D position of each vertex)
c       + faces: array of faces (index of vertices that belong to each face)
c       + scaling_factor: global scaling factor over vertices
c       + mtllib_file: name of the materials library file
c       + Nmat: number of materials in the list of materials
c       + materials_list: list of materials
c       + material_index: index of the material in the list, for each face
c
c     I/O
      integer dim
      character*(Nchar_mx) obj_file
      logical display
      logical invert_normal
      integer Nv,Nf
      double precision vertices(1:Nv_mx,1:Ndim_mx)
      integer faces(1:Nf_mx,1:Nvinface)
      double precision scaling_factor
      character*(Nchar_mx) mtllib_file
      integer Nmat
      character*(Nchar_mx) materials_list(1:Nmaterial_mx)
      integer material_index(1:Nf_mx)
c     temp
      integer iv,iface,idim,imat
      integer Ncode
      character*(Nchar_mx) mat_label(1:Ncode_mx)
      integer rgb_code(1:Ncode_mx,1:3)
      character*(Nchar_mx) material,face_material
      character*(Nchar_mx) mtllib_file_path,datafile,command
c     label
      character*(Nchar_mx) label
      label='subroutine record_obj'
c
      if (invert_normal) then
         call invert_normals(Nf,faces)
      endif                     ! invert_normal
c
      datafile='./data/rgb_color_codes.in'
      call read_rgb_codes(datafile,Ncode,mat_label,rgb_code)
      mtllib_file_path='./results/'//trim(mtllib_file)
      command=' rm -f '//trim(mtllib_file_path)
      call exec(command)
      do imat=1,Nmat
         material=trim(materials_list(imat))
         call append_mtllib_file(mtllib_file_path,Ncode,mat_label,rgb_code,material)
      enddo                     ! imat
c
c     Record OBJ
      open(11,file=trim(obj_file))
      write(11,10) 'mtllib '//trim(mtllib_file)
      do iv=1,Nv
         write(11,21) 'v',(vertices(iv,idim)*scaling_factor,idim=1,dim)
      enddo                     ! iv
      do imat=1,Nmat
         material=trim(materials_list(imat))
         write(11,10) 'usemtl '//trim(material)
         do iface=1,Nf
            face_material=trim(materials_list(material_index(iface)))
            if (trim(face_material).eq.trim(material)) then
               write(11,22) 'f',(faces(iface,iv),iv=1,3)
            endif
         enddo                  ! iface
      enddo                     ! imat
      close(11)
      if (display) then
         write(*,*) 'File was recorded: ',trim(obj_file)
      endif
      
      return
      end

      

      subroutine record_single_obj(dim,obj_file,display,invert_normal,Nv,Nf,vertices,faces,scaling_factor,mtllib_file,Nmat,materials_list,material_index)
      implicit none
      include 'max.inc'
      include 'formats.inc'
      include 'param.inc'
      include 'size_params.inc'
c
c     Purpose: to record a trianglemesh to a wavefront file
c
c     Input:
c       + dim: dimension of the physical space
c       + obj_file: name of the output wavefront file
c       + display: T if a info line should be written to display
c       + invert_normal: true if the direction of normals has to be inverted
c       + Nv: number of vertices
c       + Nf: number of faces
c       + vertices: array of verices definition (3D position of each vertex)
c       + faces: array of faces (index of vertices that belong to each face)
c       + scaling_factor: global scaling factor over vertices
c       + mtllib_file: name of the materials library file
c       + Nmat: number of materials in the list of materials
c       + materials_list: list of materials
c       + material_index: index of the material in the list, for each face
c
c     I/O
      integer dim
      character*(Nchar_mx) obj_file
      logical display
      logical invert_normal
      integer Nv,Nf
      double precision vertices(1:Nv_so_mx,1:Ndim_mx)
      integer faces(1:Nf_so_mx,1:Nvinface)
      double precision scaling_factor
      character*(Nchar_mx) mtllib_file
      integer Nmat
      character*(Nchar_mx) materials_list(1:Nmaterial_mx)
      integer material_index(1:Nf_so_mx)
c     temp
      integer iv,iface,idim,imat
      integer Ncode
      character*(Nchar_mx) mat_label(1:Ncode_mx)
      integer rgb_code(1:Ncode_mx,1:3)
      character*(Nchar_mx) material,face_material
      character*(Nchar_mx) mtllib_file_path,datafile,command
c     label
      character*(Nchar_mx) label
      label='subroutine record_single_obj'
c
      if (invert_normal) then
         call invert_normals_so(Nf,faces)
      endif                     ! invert_normal
c
      datafile='./data/rgb_color_codes.in'
      call read_rgb_codes(datafile,Ncode,mat_label,rgb_code)
      mtllib_file_path='./results/'//trim(mtllib_file)
      command=' rm -f '//trim(mtllib_file_path)
      call exec(command)
      do imat=1,Nmat
         material=trim(materials_list(imat))
         call append_mtllib_file(mtllib_file_path,Ncode,mat_label,rgb_code,material)
      enddo                     ! imat
c
c     Record OBJ
      open(11,file=trim(obj_file))
      write(11,10) 'mtllib '//trim(mtllib_file)
      do iv=1,Nv
         write(11,21) 'v',(vertices(iv,idim)*scaling_factor,idim=1,dim)
      enddo                     ! iv
      do imat=1,Nmat
         material=trim(materials_list(imat))
         write(11,10) 'usemtl '//trim(material)
         do iface=1,Nf
            face_material=trim(materials_list(material_index(iface)))
            if (trim(face_material).eq.trim(material)) then
               write(11,22) 'f',(faces(iface,iv),iv=1,3)
            endif
         enddo                  ! iface
      enddo                     ! imat
      close(11)
      if (display) then
         write(*,*) 'File was recorded: ',trim(obj_file)
      endif
      
      return
      end
