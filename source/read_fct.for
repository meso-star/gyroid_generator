c     Copyright (C) 2023 |Meso|Star> (contact@meso-star.com)
      subroutine read_fct(dim,data_file,Nf,Ncf,fct)
      implicit none
      include 'max.inc'
      include 'size_params.inc'
c     
c     Purpose: to read the Face Connectivity Table
c     
c     Input:
c       + dim: dimension of space
c       + data_file: file to read
c     
c     Output:
c       + Nf: number of faces
c       + Ncf: number of connecting faces, per face
c       + fct: face connectivity table
c         fct(i,j), j=1,Ncf(i): indexes of the Ncf faces that connect to face i
c     
c     I/O
      integer dim
      character*(Nchar_mx) data_file
      integer Nf
      integer Ncf(1:Nf_mx)
      integer fct(1:Nf_mx,1:Ncf_mx)
c     temp
      integer ios,iface,j
c     label
      character*(Nchar_mx) label
      label='subroutine read_fct'

      open(12,file=trim(data_file),status='old',iostat=ios)
      if (ios.ne.0) then
         call error(label)
         write(*,*) 'File not found: ',trim(data_file)
         stop
      else
         write(*,*) 'Reading file: ',trim(data_file)
      endif
      read(12,*) Nf
      do iface=1,Nf
         read(12,*) Ncf(iface),(fct(iface,j),j=1,Ncf(iface))
      enddo                     ! iface
      close(12)

      return
      end
