c     Copyright (C) 2023 |Meso|Star> (contact@meso-star.com)
      program gyroid_generator
      implicit none
      include 'max.inc'
      include 'size_params.inc'
      include 'param.inc'
c
c     Purpose: to produce a 'gyroid-shaped' solid that can be duplicated in every direction of space
c
c     The input data files are:
c       + data.in for common data and options
c       + fluid.in for data specific to the fluid
c       + solid.in for data specific to the solid
c       + probe_positions.in for probe positions
c       + probe_times.in for probe times
c       + mc.in for MC computations related data
c
c     IMPORTANT: the 'sed' command must be installed on the system
c
c     Since some steps take a lot of time, a system of intermediate production files is used
c     by the program: when the length of the domain (in each direction) and value of the target
c     have not changed since the last shape has been produced, most of the time-consuming steps
c     can be skipped by reading previously recorded data.
c
c     The thickness of the solid can still be modified: reference data does not make use of it;
c     although, this thickness parameter should be used with precaution: too high a value will
c     cause a mess with triangular faces: since the solid is thickened from the reference surface
c     using the normals @ nodes, this will produce overlaping faces when the thickness of the
c     solid is too high; a value of 2e-2 has been tested to provide correct results for Lx=Ly=Lz=1m
c     
c     Also, Limit Conditions can be modified without losing reference data.
c
c     This is version 02: an attempt to automatically identify boundary patches, that would make
c     possible to change the value of the "t" parameter.
c
c     Variables
      integer dim
      character*(Nchar_mx) seed_file
      integer seed
      character*(Nchar_mx) datafile
      logical force_remesh
      logical force_duplication
      double precision L(1:Ndim_mx)
      double precision sf
      double precision t
      double precision Lx,Ly,Lz
      double precision d
      double precision LC(1:Ndim_mx,1:2)
      logical draw_soles
      logical draw_lateral
      double precision soles_thickness
      double precision lambda_soles
      double precision rho_soles
      double precision C_soles
      double precision Rcontact
      double precision Tinit_soles
      double precision T_low,T_high
      double precision hc
      double precision Tref
      double precision Trad
      double precision lambda_solid,rho_solid,C_solid,Tinit_solid
      double precision rho_fluid,C_fluid,Tinit_fluid,Tfluid
      double precision range(1:Ndim_mx,1:Ndim_mx-1)
      integer exit_status
      integer Nintersection(1:4*Ndim_mx)
      double precision intersection(1:4*Ndim_mx,1:Ndim_mx)
      double precision dx(1:Ndim_mx)
      integer Ninterval
      integer idim,i,j,ix,iy,iz,iv,ivertex,iface,icontour
      integer cube_Nintersection
      integer Nppc
      double precision contour(1:Nppc_mx,1:Ndim_mx)
      logical debug
      character*(Nchar_mx) obj_file,intermediate_obj_file
      logical display
      logical invert_normal
      character*(Nchar_mx) mtllib_file
      integer Nmat
      character*(Nchar_mx) materials_list(1:Nmaterial_mx)
      integer material_index(1:Nf_mx)
      integer fNcf(1:Nf_mx)
      integer fct(1:Nf_mx,1:Ncf_mx)
      integer nNcf(1:Nv_mx)
      integer nct(1:Nv_mx,1:Ncf_mx)
      double precision node_normal(1:Nv_mx,1:Ndim_mx)
      logical node_is_on_edge(1:Nv_mx)
      integer Nedge(1:Nv_mx)
      integer edge_idx(1:Nv_mx,1:Ndim_mx,1:2)
      integer edge_index(1:2)
      double precision bounding_box(1:Ndim_mx,1:2)
      integer Ngroup1
      double precision vgroup1(1:Nv_so_mx,1:Ndim_mx)
      integer Ngroup2
      double precision vgroup2(1:Nv_so_mx,1:Ndim_mx)
      integer sorting_dir
      logical identical
      character*(Nchar_mx) ref_obj_file,ref_obj_dt_file
      character*(Nchar_mx) ref_fct_file,ref_nct_file
      character*(Nchar_mx) command
c     Note: main gyroid external surface
      integer Nv,Nf
      double precision vertices(1:Nv_mx,1:Ndim_mx)
      integer faces(1:Nf_mx,1:Nvinface)
c     Note: main gyroid external surface
      integer Nv2,Nf2
      double precision vertices2(1:Nv_mx,1:Ndim_mx)
      integer faces2(1:Nf_mx,1:Nvinface)
c     Note: 01 is used for lateral patches
      integer Nv01,Nf01
      double precision v01(1:Nv_so_mx,1:Ndim_mx)
      integer f01(1:Nf_so_mx,1:Nvinface)
c     Note: 02 is for the lateral face of a sole, pierced by patch holes, and then for the whole sole pierced by patch holes
      integer Nv02,Nf02
      double precision v02(1:Nv_so_mx,1:Ndim_mx)
      integer f02(1:Nf_so_mx,1:Nvinface)
c     Note: 03 is for the group of patches (only) for any given lateral face, and then for the whole sole (including patches)
      integer Nv03,Nf03
      double precision v03(1:Nv_so_mx,1:Ndim_mx)
      integer f03(1:Nf_so_mx,1:Nvinface)
c     Note: 04 is a duplicate for the groupe of patches for a lateral face
      integer Nv04,Nf04
      double precision v04(1:Nv_so_mx,1:Ndim_mx)
      integer f04(1:Nf_so_mx,1:Nvinface)
c     Note: _patch is the list of lateral patches
      integer Nv_patch(1:Npatch_mx)
      integer Nf_patch(1:Npatch_mx)
      double precision v_patch(1:Npatch_mx,1:Nv_so_mx,1:Ndim_mx)
      integer f_patch(1:Npatch_mx,1:Nf_so_mx,1:Nvinface)
c     Note: _add is for the object that has been added to the main lateral face in order to complete the sole
      integer Nv_add,Nf_add
      double precision v_add(1:Nv_so_mx,1:Ndim_mx)
      integer f_add(1:Nf_so_mx,1:Nvinface)
c     
      integer direction,side,ipatch,patch_index,mat_index
      integer patch_index0
      logical err_code
      integer Nvg
      integer vg_dim_side(1:Ngroup_mx,1:2)
      double precision vg_bbox(1:Ngroup_mx,1:Ndim_mx,1:2)
      logical loops_only,use_group2
      integer Npatch,Npatch_in_face
      integer vg_idx(1:Npatch_mx)
      logical Tfound,directory_exists
      logical sed_command_found
      logical err_found
      integer Nsample
      double precision ddf
      integer Nprobe
      double precision probe_position(1:Nprobe_mx,1:Ndim_mx)
      integer Ntime
      double precision probe_time(1:Ntime_mx)
      logical rotation
      logical translation
      double precision rotation_center(1:Ndim_mx)
      double precision rotation_axis(1:Ndim_mx)
      double precision rotation_angle
      double precision translation_vector(1:Ndim_mx)
      integer Ncontour2
      integer Nppc2(1:Ncontour_mx)
      double precision contour2(1:Ncontour_mx,1:Nppc_mx,1:Ndim_mx)
      integer Np
      double precision ctr(1:Nppc_mx,1:Ndim_mx)
      integer sole_index
      character*(Nchar_mx) int_gyroid_fluid_obj
      character*(Nchar_mx) int_gyroid_fluid_stl
      character*(Nchar_mx) str2,ipatch_str2
      character*(Nchar_mx) int_patch_obj
      character*(Nchar_mx) int_patch_stl
      character*(Nchar_mx) solid_gyroid_obj
      character*(Nchar_mx) solid_gyroid_stl
      character*(Nchar_mx) solid_sole_obj
      character*(Nchar_mx) solid_sole_stl
      character*(Nchar_mx) int_sole_fluid_obj
      character*(Nchar_mx) int_sole_fluid_stl
      character*(Nchar_mx) int_sole_ext_obj
      character*(Nchar_mx) int_sole_ext_stl
      character*(Nchar_mx) int_sole_universe_obj
      character*(Nchar_mx) int_sole_universe_stl
      character*(Nchar_mx) int_sole_gyroid_obj
      character*(Nchar_mx) int_sole_gyroid_stl
      character*(Nchar_mx) fluid_cavity_obj
      character*(Nchar_mx) fluid_cavity_stl
      double precision interface_area
c     progress display
      integer len
      integer*8 ntot,ndone
      double precision fdone,fdone0
      integer ifdone,pifdone
      character*(Nchar_mx) str,source_str
      character*(Nchar_mx) fmt,fmt0
c     label
      character*(Nchar_mx) label
      label='program gyroid_generator'
c
      dim=3                     ! dimension of space
c     detecting required commands
      command='sed'
      call detect_command(command,sed_command_found)
      if (.not.sed_command_found) then
         call warning(label)
         write(*,*) 'Command "'//trim(command)//'" was not found on this system'
         write(*,*) 'Some limitations may incurr'
      endif
c     create the "results" directory if missing
      inquire(file='./results',exist=directory_exists)
      if (.not.directory_exists) then
         command='mkdir ./results'
         call exec(command)
      endif
c     initialization
      call init()
      
c     data about materials. Will need it later
      mtllib_file='materials.mtl'
      Nmat=5
      if (Nmat.gt.Nmaterial_mx) then
         call error(label)
         write(*,*) 'Nmat=',Nmat
         write(*,*) '> Nmaterial_mx=',Nmaterial_mx
         stop
      endif
      materials_list(1)='slate'
      materials_list(2)='blue'
      materials_list(3)='red'
      materials_list(4)='white'
      materials_list(5)='brown_wood'
c     Initialize the random number generator (required for working with contours)
      seed_file='./data/seed'
      ref_obj_file='./results/ref_gyroid.obj'
      ref_obj_dt_file='./results/ref_gyroid.dat'
      ref_fct_file='./results/ref_fct.txt'
      ref_nct_file='./results/ref_nct.txt'
      call read_seed(seed_file,seed)
      call initialize_random_generator(seed)
c     Read input data
      datafile='./data.in'
      call read_data(dim,datafile,force_remesh,force_duplication,L,Ninterval,sf,t,Lx,Ly,Lz,d,LC,hc,Tref,Trad)
c     Identification of low and high temperatures
      Tfound=.false.
      do idim=1,dim
         do j=1,2
            if ((LC(idim,j).gt.0.0D+0).and.(.not.Tfound)) then
               T_low=LC(idim,j)
               T_high=LC(idim,j)
               Tfound=.true.
            endif
            if ((LC(idim,j).gt.0.0D+0).and.(Tfound)) then
               if (LC(idim,j).lt.T_low) then
                  T_low=LC(idim,j)
               endif
               if (LC(idim,j).gt.T_high) then
                  T_high=LC(idim,j)
               endif
            endif
         enddo                  ! j
      enddo                     ! idim
      write(*,*) 'Limit conditions:'
      write(*,*) 'Lower temperature:',T_low,' K'
      write(*,*) 'Higher temperature:',T_high,' K'
c     Reading soles properties
      datafile='./soles.in'
      call read_soles_properties(datafile,draw_soles,draw_lateral,soles_thickness,lambda_soles,rho_soles,C_soles,Rcontact,Tinit_soles)
c     Reading physical properties
      datafile='./solid.in'
      call read_solid_properties(datafile,lambda_solid,rho_solid,C_solid,Tinit_solid)
      datafile='./fluid.in'
      call read_fluid_properties(datafile,rho_fluid,C_fluid,Tinit_fluid,Tfluid)
c     Reading probe positions
      datafile='./probe_positions.in'
      call read_probe_positions(datafile,dim,L,sf,mtllib_file,Nmat,materials_list,Nprobe,probe_position)
c     Reading probe times
      datafile='./probe_times.in'
      call read_probe_times(datafile,dim,Ntime,probe_time)
c     Reading MC data
      datafile='./mc.in'
      call read_mc_data(datafile,Nsample,ddf)
      
c     Initialization
      do i=1,dim
         dx(i)=2.0D+0*L(i)/dble(Ninterval)
      enddo                     ! i
      icontour=0

c     check whether or not the data provided matches the data used to produce the 'ref_gyroid.obj' file
      if (force_remesh) then
         identical=.false.
         write(*,*) 'Remesh is forced'
      else
         call check_gyroid_data(dim,L,Lx,Ly,Lz,t,Ninterval,identical)
      endif                     ! force_remesh
c     
      if (identical) then
c     no need to produce the reference gyroid: just read it from file
         write(*,*) 'Reference gyroid, fct and nct are read from file'
         call read_obj(ref_obj_file,dim,Nv,Nf,vertices,faces)
         call read_fct(dim,ref_fct_file,Nf,fNcf,fct)
         call read_nct(dim,ref_nct_file,Nv,nNcf,nct)
      else
c     no luck; you have to start all over again
         write(*,*) 'Discretization of the main shape...'
         Nv=0
         Nf=0
c     --- progress display
         ntot=Ninterval**3
         ndone=0
         pifdone=0
         len=6
         call num2str(len,str,err_found)
         if (err_found) then
            call error(label)
            write(*,*) 'Could not convert to str:'
            write(*,*) 'len=',len
            stop
         endif
         fmt0='(a,i'//trim(str)//',a)'
         fmt='(i'//trim(str)//',a)'
         fdone0=0.0D+0
         write(*,trim(fmt0),advance='no') 'Done:   ',floor(fdone0),' %'
c     progress display ---
         do ix=1,Ninterval
            range(1,1)=-L(1)+(ix-1)*dx(1)
            range(1,2)=-L(1)+ix*dx(1)
            do iy=1,Ninterval
               range(2,1)=-L(2)+(iy-1)*dx(2)
               range(2,2)=-L(2)+iy*dx(2)
               do iz=1,Ninterval
                  range(3,1)=-L(3)+(iz-1)*dx(3)
                  range(3,2)=-L(3)+iz*dx(3)
c     
                  call cube_gyroid_intersection(dim,Lx,Ly,Lz,range,t,exit_status,Nintersection,intersection)
                  if (exit_status.ne.0) then
                     call error(label)
                     write(*,*) 'ix=',ix,' iy=',iy,' iz=',iz
                     write(*,*) 'exit_status=',exit_status
                     stop
                  endif
                  cube_Nintersection=0
                  do i=1,4*dim
                     cube_Nintersection=cube_Nintersection+Nintersection(i)
                  enddo         ! i
c     Number of intersections per cube should be 0, 3-6
                  if (((cube_Nintersection.ge.1).and.(cube_Nintersection.le.2)).or.(cube_Nintersection.ge.7)) then
                     write(*,*) 'Number of intersection for cube:',cube_Nintersection
                     write(*,*) 'Linits in x:',range(1,1),range(1,2)
                     write(*,*) 'Linits in y:',range(2,1),range(2,2)
                     write(*,*) 'Linits in z:',range(3,1),range(3,2)
                     do i=1,4*dim
                        if (Nintersection(i).eq.1) then
                           write(*,*) 'edge ',i,':',(intersection(i,j),j=1,dim)
                        endif
                     enddo      ! i
                     stop
                  endif
c     Get the raw contour from intersection positions
                  Nppc=0
                  do i=1,4*dim
                     if (Nintersection(i).eq.1) then
                        Nppc=Nppc+1
                        if (Nppc.gt.Nppc_mx) then
                           call error(label)
                           write(*,*) 'Nppc=',Nppc
                           write(*,*) '> Nppc_mx=',Nppc_mx
                           stop
                        endif
                        do j=1,dim
                           contour(Nppc,j)=intersection(i,j)
                        enddo   ! j
                     endif      ! Nintersection(i)=1
                  enddo         ! i
c     Produce triangles from this contour
                  if (Nppc.gt.0) then
                     icontour=icontour+1
                     debug=.false.
                     call triangulate_contour(dim,debug,Nppc,contour,Nv01,v01,Nf01,f01)
                     call add2obj(dim,Nv,Nf,vertices,faces,.false.,Nv01,Nf01,v01,f01)
                  endif
c     --- progress display
                  ndone=ndone+1
                  fdone=dble(ndone)/dble(ntot)*1.0D+2
                  ifdone=floor(fdone)
                  if (ifdone.gt.pifdone) then
                     do j=1,len+2
                        write(*,"(a)",advance='no') "\b"
                     enddo      ! j
                     write(*,trim(fmt),advance='no') floor(fdone),' %'
                     pifdone=ifdone
                  endif
c     progress display ---
               enddo            ! iz
            enddo               ! iy
         enddo                  ! ix
         write(*,*)
c     Remove duplicate vertices
         write(*,*) 'Removing duplicate vertices; initial number of vertices:',Nv
         call remove_duplicate_vertices(dim,Nv,Nf,vertices,faces)
         write(*,*)
         write(*,*) 'Final number of vertices:',Nv
c     record 'ref_gyroid.obj'
         display=.true.
         invert_normal=.false.
         do iface=1,Nf
            material_index(iface)=1
         enddo                  ! iface
         call record_obj(dim,ref_obj_file,display,invert_normal,Nv,Nf,vertices,faces,1.0D+0,mtllib_file,Nmat,materials_list,material_index)
c     record 'ref_gyroid.dat'
         call record_gyroid_data(dim,ref_obj_dt_file,L,Lx,Ly,Lz,t,Ninterval)
c     Build the Node Connectivity Table
         write(*,*) 'Building the Node Connectivity Table...'
         call build_node_connectivity_table(dim,Nv,Nf,vertices,faces,ref_nct_file,nNcf,nct)
c     Build Face Connectivity Table
         write(*,*) 'Building the Face Connectivity Table...'
         call build_face_connectivity_table(dim,Nv,Nf,vertices,faces,ref_fct_file,fNcf,fct)
      endif                     ! identical
c     Adjust face normals
      write(*,*) 'Adjusting normals...'
      call adjust_normals(dim,Nv,Nf,vertices,faces,fNcf,fct)
c     Recording intermediate OBJ
      write(*,*) 'Recording OBJ file...'
      intermediate_obj_file='./results/gyroid01.obj'
      display=.true.
      invert_normal=.false.
      do iface=1,Nf
         material_index(iface)=1
      enddo                     ! iface
      call record_obj(dim,intermediate_obj_file,display,invert_normal,Nv,Nf,vertices,faces,sf,mtllib_file,Nmat,materials_list,material_index)
c     Compute normal at nodes
      write(*,*) 'Normal @ nodes...'
      call node_normals(dim,L,Lx,Ly,Lz,t,Nv,Nf,vertices,faces,nNcf,nct,.true.,node_normal,node_is_on_edge,Nedge,edge_idx)
c     Identify group of points on the boundary
      write(*,*) 'Identification of boundary node groups...'
      call identify_boundary_vertex_groups(dim,L,Nv,Nf,vertices,faces,node_is_on_edge,Nedge,edge_idx,dx,Nvg,vg_dim_side,vg_bbox,loops_only)
c     
c     "use_group2" variable: has to be set to .true. when the main surface must be duplicated
c     in order to generate a volume
      if ((loops_only).and.(.not.force_duplication)) then
c     "loops_only" refers to the fact that the reference surface forms closed loops at the edges of the spatial domain (such as with t=1.2)
c     In that case, the main surface will not be duplicated, except if the user forces the duplication
         use_group2=.false.
         call invert_normals(Nf,faces)
      else
c     In all other cases, the main surface must be duplicated
         use_group2=.true.
      endif

c     Debug
c      write(*,*) 'loops_only=',loops_only
c      write(*,*) 'draw_soles=',draw_soles
c      write(*,*) 'draw_lateral=',draw_lateral
c      write(*,*) 'use_group2=',use_group2
cc      stop
c     Debug
c     
c     options compatibility
c      if (((draw_soles).or.(draw_lateral)).and.(loops_only).and.(force_duplication)) then
      if (((draw_soles).or.(draw_lateral)).and.(use_group2)) then
         call error(label)
         write(*,*) 'Options incompatibility:'
         if (draw_soles) then
            write(*,*) 'Drawing soles is required'
         else if (draw_lateral) then
            write(*,*) 'Drawing lateral surfaces is required'
         endif
         write(*,*) 'but'
         write(*,*) 'duplication of main surface is forced'
         write(*,*) 'and normal at edges of the domain may not be normal to domain boundaries'
         stop
      endif                     ! draw_soles and use_group2
c     Displace original surface at distance d/2 and duplicate original surface at distance -d/2
      if (use_group2) then
         call thicken_gyroid_surface2(dim,L,Nv,Nf,vertices,faces,node_normal,d,node_is_on_edge,Nedge,edge_idx)
      endif
      call copy_obj(dim,Nv,Nf,vertices,faces,Nv2,Nf2,vertices2,faces2)
c     ---------------------------------------------------------------------------------------------------------------------------------------------------------
c     Patches
c     ---------------------------------------------------------------------------------------------------------------------------------------------------------
      patch_index=0
      sole_index=0
      Npatch=9999
      do direction=1,dim
         edge_index(1)=direction
         do side=1,2
c     Debug
c            write(*,*) 'side=',side
c     Debug
            edge_index(2)=side
c     Identify all patches for current direction / side
            call identify_patches(dim,L,Nvg,vg_dim_side,vg_bbox,direction,side,Npatch_in_face,vg_idx,sorting_dir)
c     Set material index
            if (LC(direction,side).eq.T_low) then
               mat_index=2
            else if (LC(direction,side).eq.T_high) then
               mat_index=3
            else if (LC(direction,side).eq.-1.0D+0) then
               mat_index=4
            else
               call error(label)
               write(*,*) 'LC(',direction,',',side,')=',LC(direction,side)
               write(*,*) 'is different from T_low=',T_low
               write(*,*) 'is different from T_high=',T_high
               write(*,*) 'and is not equal to -1'
               stop
            endif
c     SOLES
c     Interface between sole and solid
            sole_index=sole_index+1
            Ncontour2=0
            call main_contour(dim,L,direction,side,rotation,translation,rotation_center,rotation_axis,rotation_angle,translation_vector,Ncontour2,Nppc2,contour2)
c     
            patch_index0=patch_index ! index of the last patch before current face
            do ipatch=1,Npatch_in_face
               patch_index=patch_index+1
c     Debug
c               write(*,*) 'patch_index=',patch_index,' Npatch_in_face=',Npatch_in_face
c               if (patch_index.eq.1) then
c                  debug=.true.
c     else
               debug=.false.
c               endif
c     Debug
               call get_bounding_box(dim,Npatch_in_face,vg_idx,ipatch,vg_bbox,bounding_box)
               call identify_edge_nodes(debug,dim,L,Nv,vertices,node_is_on_edge,Nedge,edge_idx,edge_index,bounding_box,use_group2,Ngroup1,vgroup1,Ngroup2,vgroup2)
               if (rotation) then
                  call vertex_group_rotation(dim,rotation_center,rotation_axis,rotation_angle,Ngroup1,vgroup1,Np,ctr)
               endif            ! rotation
               if (translation) then
                  call vertex_group_translation(dim,Ngroup1,vgroup1,Np,ctr)
               endif            ! translation
               call add_ctr_to_contour(dim,Ncontour2,Nppc2,contour2,Np,ctr)
               call make_patch(.false.,dim,Ngroup1,vgroup1,Ngroup2,vgroup2,use_group2,Nv01,Nf01,v01,f01)
c     Add object to Nv_patch, Nf_patch, v_patch, f_patch
               call add_patch(dim,Nv01,Nf01,v01,f01,patch_index,Nv_patch,Nf_patch,v_patch,f_patch)
c     record obj file
c     Debug
               if (patch_index.gt.Npatch) then
                  call error(label)
                  write(*,*) 'patch_index=',patch_index
                  write(*,*) '> Npatch=',Npatch
                  stop
               endif
c     Debug
               call int_patch_filenames(Npatch,patch_index,int_patch_obj,int_patch_stl)
               display=.true.
               invert_normal=.false.
               do iface=1,Nf01
                  material_index(iface)=mat_index
               enddo            ! iface
               call record_single_obj(dim,int_patch_obj,display,invert_normal,Nv01,Nf01,v01,f01,sf,mtllib_file,Nmat,materials_list,material_index)
c     record stl file
c     Debug
c               write(*,*) 'ok6'
c     Debug
               call sobj2stl(dim,Nv01,Nf01,v01,f01,invert_normal,int_patch_stl,sf)
c     Debug
c               write(*,*) 'ok7'
c     Debug
            enddo               ! ipatch
c     Debug
c            if (((draw_soles).or.(draw_lateral)).and.(use_group2)) then
            if ((draw_soles).or.(draw_lateral)) then
c     Debug
c     SOLES
c     Debug
c               write(*,*) 'ok8'
c     Debug
               Nv02=0
               Nf02=0
               Nv03=0
               Nf03=0
c     Debug
c               write(*,*) 'Ncontour2=',Ncontour2
c               write(*,*) 'Nppc2=',Nppc2
c     Debug
               call delaunay_triangulation(dim,Ncontour2,Nppc2,contour2,Nv02,Nf02,v02,f02)
c     Debug
c               write(*,*) 'ok9'
c     Debug
               if (rotation) then
                  call rotate_sobj_complete(dim,Nv02,v02,rotation_center,rotation_axis,-rotation_angle)
               endif            ! rotation
               if (translation) then
                  call translate_sobj(dim,Nv02,v02,translation_vector)
               endif            ! translation
               if ((direction.eq.3).and.(side.eq.2)) then
                  call invert_normals_so(Nf02,f02)
               endif
c     
c     02 is the object for the sole's main face (pierced by patches), unscaled
c     _patch is the same, but with original data (without consecutive rotations / translations)
               write(*,*) 'Analysis face index:',sole_index
               call retrieve_original_coordinates(dim,Nv02,Nf02,v02,f02,Npatch_in_face,Nv_patch,Nf_patch,v_patch,f_patch)
c     Complete object "Nv2,Nf2,vertices2,faces2" for the fluid enclosure
               invert_normal=.false.
               call add2obj(dim,Nv2,Nf2,vertices2,faces2,invert_normal,Nv02,Nf02,v02,f02)
c     record sole-fluid interface
               call int_sole_fluid_filenames(sole_index,int_sole_fluid_obj,int_sole_fluid_stl)
               do iface=1,Nf02
                  material_index(iface)=mat_index
               enddo            ! iface
               call record_single_obj(dim,int_sole_fluid_obj,.true.,invert_normal,Nv02,Nf02,v02,f02,sf,mtllib_file,Nmat,materials_list,material_index)
               call sobj2stl(dim,Nv02,Nf02,v02,f02,invert_normal,int_sole_fluid_stl,sf)
               call trianglemesh_area(dim,Nv02,Nf02,v02,f02,interface_area)
               write(*,*) 'Interface between sole ',sole_index,' and fluid: area=',interface_area,' [m²]'
c     
               call complete_sole(dim,direction,side,Nv02,Nf02,v02,f02,soles_thickness,Nv_add,Nf_add,v_add,f_add)
c     add individual patches to the obj of the sole
               do ipatch=1,Npatch_in_face
                  patch_index=patch_index0+ipatch
c     get patch definition from Nv_patch, Nf_patch, v_patch, f_patch
                  call get_patch(dim,Nv_patch,Nf_patch,v_patch,f_patch,patch_index,Nv01,Nf01,v01,f01)
                  call add2sobj(dim,Nv03,Nf03,v03,f03,.true.,Nv01,Nf01,v01,f01)
               enddo            ! ipatch
               call copy_sobj(dim,Nv03,Nf03,v03,f03,Nv04,Nf04,v04,f04) ! 04 holds the mesh for a group of patches (for a lateral face)
c     record objs for the sole
               call solid_sole_filenames(sole_index,solid_sole_obj,solid_sole_stl)
               call int_sole_ext_filenames(sole_index,int_sole_ext_obj,int_sole_ext_stl)
               call int_sole_universe_filenames(sole_index,int_sole_universe_obj,int_sole_universe_stl)
               call int_sole_gyroid_filenames(sole_index,int_sole_gyroid_obj,int_sole_gyroid_stl)
               do iface=1,Nf_add
                  material_index(iface)=mat_index
               enddo            ! iface
               call record_single_obj(dim,int_sole_ext_obj,.true.,.false.,Nv_add,Nf_add,v_add,f_add,sf,mtllib_file,Nmat,materials_list,material_index)
               do iface=1,Nf02
                  material_index(iface)=mat_index
               enddo            ! iface
               call record_single_obj(dim,int_sole_universe_obj,.true.,.false.,Nv02,Nf02,v02,f02,sf,mtllib_file,Nmat,materials_list,material_index)
               call add2sobj(dim,Nv03,Nf03,v03,f03,.false.,Nv02,Nf02,v02,f02) ! Now _03 is the obj for the whole sole
               do iface=1,Nf03
                  material_index(iface)=mat_index
               enddo            ! iface
               call record_single_obj(dim,solid_sole_obj,.true.,.false.,Nv03,Nf03,v03,f03,sf,mtllib_file,Nmat,materials_list,material_index)
               do iface=1,Nf04
                  material_index(iface)=mat_index
               enddo            ! iface
               call record_single_obj(dim,int_sole_gyroid_obj,.true.,.false.,Nv04,Nf04,v04,f04,sf,mtllib_file,Nmat,materials_list,material_index)
               call trianglemesh_area(dim,Nv04,Nf04,v04,f04,interface_area)
               write(*,*) 'Interface between sole ',sole_index,' and gyroid: area=',interface_area,' [m²]'
c     record stl files
               call sobj2stl(dim,Nv_add,Nf_add,v_add,f_add,.false.,int_sole_ext_stl,sf)
               call sobj2stl(dim,Nv02,Nf02,v02,f02,.false.,int_sole_universe_stl,sf)
               call sobj2stl(dim,Nv03,Nf03,v03,f03,.false.,solid_sole_stl,sf)
               call sobj2stl(dim,Nv04,Nf04,v04,f04,.false.,int_sole_gyroid_stl,sf)
c     
            endif               ! (((draw_soles).or.(draw_lateral)).and.(use_group2))
c     
         enddo                  ! side
      enddo                     ! direction
      Npatch=patch_index
c      
c     Record the final OBJ
      write(*,*) 'Recording OBJ files...'
      display=.true.
      invert_normal=.false.
c     object "Nv,Nf,vertices,faces"
      do iface=1,Nf
         material_index(iface)=1
      enddo                     ! iface
      call int_gyroid_fluid_filenames(int_gyroid_fluid_obj,int_gyroid_fluid_stl)
      call record_obj(dim,int_gyroid_fluid_obj,display,invert_normal,Nv,Nf,vertices,faces,sf,mtllib_file,Nmat,materials_list,material_index)
c     Record stl file
c     Debug
c      write(*,*) 'ok0'
c     Debug
      call obj2stl(dim,Nv,Nf,vertices,faces,invert_normal,int_gyroid_fluid_stl,sf)
c     Debug
c      write(*,*) 'ok1'
c     Debug
c     object "Nv2,Nf2,vertices2,faces2"
      call fluid_cavity_filenames(fluid_cavity_obj,fluid_cavity_stl)
      do iface=1,Nf2
         material_index(iface)=1
      enddo                     ! iface
      call record_obj(dim,fluid_cavity_obj,display,invert_normal,Nv2,Nf2,vertices2,faces2,sf,mtllib_file,Nmat,materials_list,material_index)
c     Record stl file
c     Debug
c      write(*,*) 'ok2'
c     Debug
      call obj2stl(dim,Nv2,Nf2,vertices2,faces2,invert_normal,fluid_cavity_stl,sf)
c     Debug
c      write(*,*) 'ok3'
c     Debug
c     Production of final STL file
      do ipatch=1,Npatch
c     get object from Nv_patch, Nf_patch, v_patch, f_patch
         Nv01=Nv_patch(ipatch)
         do ivertex=1,Nv01
            do j=1,dim
               v01(ivertex,j)=v_patch(ipatch,ivertex,j)
            enddo               ! j
         enddo                  ! ivertex
         Nf01=Nf_patch(ipatch)
         do iface=1,Nf01
            do j=1,Nvinface
               f01(iface,j)=f_patch(ipatch,iface,j)
            enddo               ! j
         enddo                  ! iface
         call add2obj(dim,Nv,Nf,vertices,faces,invert_normal,Nv01,Nf01,v01,f01)
      enddo                     ! ipatch
      call solid_gyroid_filenames(solid_gyroid_obj,solid_gyroid_stl)
      do iface=1,Nf
         material_index(iface)=1
      enddo                     ! iface
      call record_obj(dim,solid_gyroid_obj,display,invert_normal,Nv,Nf,vertices,faces,sf,mtllib_file,Nmat,materials_list,material_index)
c     Debug
c      write(*,*) 'ok4'
c     Debug
      call obj2stl(dim,Nv,Nf,vertices,faces,invert_normal,solid_gyroid_stl,sf)
c     Debug
c      write(*,*) 'ok5'
c     Debug
c     produce STARDIS files
      call stardis_files(dim,hc,L,LC,Nvg,vg_dim_side,vg_bbox,draw_soles,draw_lateral,soles_thickness,lambda_soles,rho_soles,C_soles,Rcontact,Tinit_soles,
     &     Tinit_fluid,Tfluid,Trad,Tref,Nsample,ddf,rho_fluid,C_fluid,d,lambda_solid,rho_solid,C_solid,Tinit_solid,Nprobe,probe_position,Ntime,probe_time)
c     replace 'E' by 'e' in STL files -> command 'sed' must be installed !
      if (sed_command_found) then
         command="sed -i 's/E/e/g' results/*.stl"
         call exec(command)
      endif
c     Exit properly
      command='rm -rf nlines_*'
      call exec(command)
c      
      end
