c     Copyright (C) 2023 |Meso|Star> (contact@meso-star.com)
      subroutine get_nlines(infile,nl)
      implicit none
      include 'max.inc'
c     
c     Purpose: to get the number of lines in the provided file
c
c     Inputs:
c       + infile: file whose number of lines has to be retrieved
c
c     Outputs:
c       + nl: number of lines in the "infile" file; a value of zero indicates
c             the file does not exist
c
c     I/O
      character*(Nchar_mx) infile
      integer nl
c     temp
      integer ios,pindex
      character*(Nchar_mx) command,lfile
      character*(Nchar_mx) ich
      logical err_code
c     label
      character*(Nchar_mx) label
      label='subroutine get_nlines'

      pindex=0
c     Number of lines in the specified data file
      call num2str3(pindex,ich,err_code)
      if (err_code) then
         call error(label)
         write(*,*) 'Could not convert to character string:'
         write(*,*) 'pindex=',pindex
         stop
      endif
      lfile='./nlines_'//trim(ich)
      command="cat "//trim(infile)//" | wc -l > "//trim(lfile)
      call exec(command)

      open(10,file=trim(lfile),status='old',iostat=ios)
      if (ios.ne.0) then        ! file not found
         call error(label)
         write(*,*) 'File not found:'
         write(*,*) trim(lfile)
         stop
      endif
      read(10,*) nl
      close(10)

      return
      end
