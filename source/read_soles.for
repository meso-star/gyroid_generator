c     Copyright (C) 2023 |Meso|Star> (contact@meso-star.com)
      subroutine read_soles_properties(datafile,draw_soles,draw_lateral,thickness,lambda,rho,C,Rcontact,Tinit)
      implicit none
      include 'max.inc'
c     
c     Purpose: to read soles properties
c     
c     Input:
c       + datafile: file to read
c     
c     Output:
c       + draw_soles: T if soles have to be drawn for x=+/-Lx
c       + draw_lateral: T if adiabatic lateral surfaces have to be drawn
c       + thickness: soles thickness [m]
c       + lambda: conductivity [W.m⁻¹.K⁻¹]
c       + rho: volumic mass [kg.m⁻³]
c       + C: specific heat [J.kg⁻¹.K⁻¹]
c       + Rcontact: contact resistance between sole and gyroid solid [K.m².W⁻¹]
c       + Tinit: initial temperature [K]
c     
c     I/O
      character*(Nchar_mx) datafile
      logical draw_soles
      logical draw_lateral
      double precision thickness
      double precision lambda
      double precision rho
      double precision C
      double precision Rcontact
      double precision Tinit
c     temp
      integer i,ios
c     label
      character*(Nchar_mx) label
      label='subroutine read_soles_properties'

      open(12,file=trim(datafile),status='old',iostat=ios)
      if (ios.ne.0) then
         call error(label)
         write(*,*) 'File not found: ',trim(datafile)
         stop
      else
         write(*,*) 'Reading file: ',trim(datafile)
      endif
      do i=1,2
         read(12,*)
      enddo                     ! i
      read(12,*) draw_soles
      read(12,*)
      read(12,*) draw_lateral
      read(12,*)
      read(12,*) thickness
      read(12,*)
      read(12,*) lambda
      read(12,*) 
      read(12,*) rho
      read(12,*) 
      read(12,*) C
      read(12,*) 
      read(12,*) Rcontact
      read(12,*) 
      read(12,*) Tinit
      close(12)

      if ((draw_soles).or.(draw_lateral)) then
         if (thickness.le.0.0D+0) then
            call error(label)
            write(*,*) 'thickness=',thickness
            write(*,*) 'should be positive'
            stop
         endif
      endif
      if (lambda.le.0.0D+0) then
         call error(label)
         write(*,*) 'lambda=',lambda
         write(*,*) 'should be positive'
         stop
      endif
      if (rho.le.0.0D+0) then
         call error(label)
         write(*,*) 'rho=',rho
         write(*,*) 'should be positive'
         stop
      endif
      if (C.le.0.0D+0) then
         call error(label)
         write(*,*) 'C=',C
         write(*,*) 'should be positive'
         stop
      endif
      if (Rcontact.lt.0.0D+0) then
         call error(label)
         write(*,*) 'Rcontact=',Rcontact
         write(*,*) 'should be ≥ 0'
         stop
      endif
      if (Tinit.le.0.0D+0) then
         call error(label)
         write(*,*) 'Tinit=',Tinit
         write(*,*) 'should be positive'
         stop
      endif

      return
      end
