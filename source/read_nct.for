c     Copyright (C) 2023 |Meso|Star> (contact@meso-star.com)
      subroutine read_nct(dim,data_file,Nv,Ncf,nct)
      implicit none
      include 'max.inc'
      include 'size_params.inc'
c     
c     Purpose: to read the Node Connectivity Table
c     
c     Input:
c       + dim: dimension of space
c       + data_file: file to read
c     
c     Output:
c       + Nv: number of vertices
c       + Ncf: number of connecting faces, per node
c       + nct: node connectivity table
c         nct(i,j), j=1,Ncf(i): indexes of the Ncf faces that connect to node i
c     
c     I/O
      integer dim
      character*(Nchar_mx) data_file
      integer Nv
      integer Ncf(1:Nv_mx)
      integer nct(1:Nv_mx,1:Ncf_mx)
c     temp
      integer ios,ivertex,j
c     label
      character*(Nchar_mx) label
      label='subroutine read_nct'

      open(13,file=trim(data_file),status='old',iostat=ios)
      if (ios.ne.0) then
         call error(label)
         write(*,*) 'File not found: ',trim(data_file)
         stop
      else
         write(*,*) 'Reading file: ',trim(data_file)
      endif
      read(13,*) Nv
      do ivertex=1,Nv
         read(13,*) Ncf(ivertex),(nct(ivertex,j),j=1,Ncf(ivertex))
      enddo                     ! ivertex
      close(13)

      return
      end
