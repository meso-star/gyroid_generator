      subroutine extract_filename(path,filename)
      implicit none
      include 'max.inc'
c     
c     Purpose: extract a file name from a full path
c     
c     Input:
c       + path: full path
c     
c     Output:
c       + filename: file name
c     
c     I/O
      character*(Nchar_mx) path
      character*(Nchar_mx) filename
c     temp
      character*1 char
      integer Nchar
      integer idx(1:Nchar_mx)
c     label
      character*(Nchar_mx) label
      label='subroutine extract_filename'

      char='/'
      call find_character(path,char,Nchar,idx)
      if (Nchar.ne.2) then
         call error(label)
         write(*,*) 'path=',trim(path)
         write(*,*) 'should include two "',trim(char),'" characters'
         stop
      endif
      filename=path(idx(2)+1:len_trim(path))

      return
      end



      subroutine solid_gyroid_filenames(solid_gyroid_obj,solid_gyroid_stl)
      implicit none
      include 'max.inc'
c     
c     Purpose: to provide the name of the files used to record the
c     solid for the gyroid shape
c     
c     Input:
c     
c     Output:
c       + solid_gyroid_obj: obj file
c       + solid_gyroid_stl: stl file
c     
c     I/O
      character*(Nchar_mx) solid_gyroid_obj
      character*(Nchar_mx) solid_gyroid_stl
c     temp
      character*(Nchar_mx) solid_gyroid
c     label
      character*(Nchar_mx) label
      label='subroutine solid_gyroid_filenames'

      solid_gyroid='./results/solid_gyroid'
      solid_gyroid_obj=trim(solid_gyroid)//'.obj'
      solid_gyroid_stl=trim(solid_gyroid)//'.stl'

      return
      end

      

      subroutine solid_sole_filenames(sole_index,solid_sole_obj,solid_sole_stl)
      implicit none
      include 'max.inc'
c     
c     Purpose: to provide the name of the files used to record the
c     solid for the sole shape
c     
c     Input:
c       + sole_index: sole index
c     
c     Output:
c       + solid_sole_obj: obj file
c       + solid_sole_stl: stl file
c     
c     I/O
      integer sole_index
      character*(Nchar_mx) solid_sole_obj
      character*(Nchar_mx) solid_sole_stl
c     temp
      character*(Nchar_mx) solid_sole
      character*(Nchar_mx) index_str
c     label
      character*(Nchar_mx) label
      label='subroutine solid_sole_filenames'

      call fstr(sole_index,index_str)
      solid_sole='./results/solid_sole'//trim(index_str)
      solid_sole_obj=trim(solid_sole)//'.obj'
      solid_sole_stl=trim(solid_sole)//'.stl'

      return
      end



      subroutine fluid_cavity_filenames(fluid_cavity_obj,fluid_cavity_stl)
      implicit none
      include 'max.inc'
c     
c     Purpose: to provide the name of the files used to record the
c     fluid cavity
c     
c     Input:
c     
c     Output:
c     + fluid_cavity_obj: obj file
c     + fluid_cavity_stl: stl file
c     
c     I/O
      character*(Nchar_mx) fluid_cavity_obj
      character*(Nchar_mx) fluid_cavity_stl
c     temp
      character*(Nchar_mx) fluid_cavity
c     label
      character*(Nchar_mx) label
      label='subroutine fluid_cavity_filenames'

      fluid_cavity='./results/fluid_cavity'
      fluid_cavity_obj=trim(fluid_cavity)//'.obj'
      fluid_cavity_stl=trim(fluid_cavity)//'.stl'

      return
      end



      subroutine int_gyroid_fluid_filenames(int_gyroid_fluid_obj,int_gyroid_fluid_stl)
      implicit none
      include 'max.inc'
c     
c     Purpose: to provide the name of the files used to record the
c     int between the gyroid and the fluid
c     
c     Input:
c     
c     Output:
c     + int_gyroid_fluid_obj: obj file
c     + int_gyroid_fluid_stl: stl file
c     
c     I/O
      character*(Nchar_mx) int_gyroid_fluid_obj
      character*(Nchar_mx) int_gyroid_fluid_stl
c     temp
      character*(Nchar_mx) int_gyroid_fluid
c     label
      character*(Nchar_mx) label
      label='subroutine int_gyroid_fluid_filenames'

      int_gyroid_fluid='./results/interface_gyroid_fluid'
      int_gyroid_fluid_obj=trim(int_gyroid_fluid)//'.obj'
      int_gyroid_fluid_stl=trim(int_gyroid_fluid)//'.stl'

      return
      end



      subroutine int_patch_filenames(Npatch,patch_index,int_patch_obj,int_patch_stl)
      implicit none
      include 'max.inc'
c     
c     Purpose: to provide the name of the files used to record the
c     interface for a matter filament patch
c     
c     Input:
c       + Npatch: number of lateral patches
c       + patch_index: index of the patch
c     
c     Output:
c       + int_patch_obj: obj file
c       + int_patch_stl: stl file
c     
c     I/O
      integer Npatch
      integer patch_index
      character*(Nchar_mx) int_patch_obj
      character*(Nchar_mx) int_patch_stl
c     temp
      character*(Nchar_mx) int_patch
      character*(Nchar_mx) patch_index_str
c     label
      character*(Nchar_mx) label
      label='subroutine int_patch_filenames'

      int_patch='./results/interface_patch'
c     Debug
      if (patch_index.gt.Npatch) then
         call error(label)
         write(*,*) 'patch_index=',patch_index
         write(*,*) '> Npatch=',Npatch
         stop
      endif
c     Debug
      call patch_name(Npatch,patch_index,patch_index_str)
      int_patch=trim(int_patch)//trim(patch_index_str)
      
      int_patch_obj=trim(int_patch)//'.obj'
      int_patch_stl=trim(int_patch)//'.stl'
      
      return
      end


      
      subroutine int_sole_fluid_filenames(sole_index,int_sole_fluid_obj,int_sole_fluid_stl)
      implicit none
      include 'max.inc'
c     
c     Purpose: to provide the name of the files used to record the
c     interface between a sole and the fluid
c     
c     Input:
c       + sole_index: sole index
c     
c     Output:
c       + int_sole_fluid_obj: obj file
c       + int_sole_fluid_stl: stl file
c     
c     I/O
      integer sole_index
      character*(Nchar_mx) int_sole_fluid_obj
      character*(Nchar_mx) int_sole_fluid_stl
c     temp
      character*(Nchar_mx) int_sole_fluid
      character*(Nchar_mx) index_str
c     label
      character*(Nchar_mx) label
      label='subroutine int_sole_fluid_filenames'

      call fstr(sole_index,index_str)
      int_sole_fluid='./results/interface_sole'//trim(index_str)//'_fluid'
      int_sole_fluid_obj=trim(int_sole_fluid)//'.obj'
      int_sole_fluid_stl=trim(int_sole_fluid)//'.stl'

      return
      end


      
      subroutine int_sole_ext_filenames(sole_index,int_sole_ext_obj,int_sole_ext_stl)
      implicit none
      include 'max.inc'
c     
c     Purpose: to provide the name of the files used to record the
c     interface between a sole and the exterior
c     
c     Input:
c       + sole_index: sole index
c     
c     Output:
c       + int_sole_ext_obj: obj file
c       + int_sole_ext_stl: stl file
c     
c     I/O
      integer sole_index
      character*(Nchar_mx) int_sole_ext_obj
      character*(Nchar_mx) int_sole_ext_stl
c     temp
      character*(Nchar_mx) int_sole_ext
      character*(Nchar_mx) index_str
c     label
      character*(Nchar_mx) label
      label='subroutine int_sole_ext_filenames'

      call fstr(sole_index,index_str)
      int_sole_ext='./results/interface_sole'//trim(index_str)//'_extern'
      int_sole_ext_obj=trim(int_sole_ext)//'.obj'
      int_sole_ext_stl=trim(int_sole_ext)//'.stl'

      return
      end
      

      
      subroutine int_sole_universe_filenames(sole_index,int_sole_universe_obj,int_sole_universe_stl)
      implicit none
      include 'max.inc'
c     
c     Purpose: to provide the name of the files used to record the
c     interface between a sole and the rest of the universe
c     
c     Input:
c       + sole_index: sole index
c     
c     Output:
c       + int_sole_universe_obj: obj file
c       + int_sole_universe_stl: stl file
c     
c     I/O
      integer sole_index
      character*(Nchar_mx) int_sole_universe_obj
      character*(Nchar_mx) int_sole_universe_stl
c     temp
      character*(Nchar_mx) int_sole_universe
      character*(Nchar_mx) index_str
c     label
      character*(Nchar_mx) label
      label='subroutine int_sole_universe_filenames'

      call fstr(sole_index,index_str)
      int_sole_universe='./results/interface_sole'//trim(index_str)//'_universe'
      int_sole_universe_obj=trim(int_sole_universe)//'.obj'
      int_sole_universe_stl=trim(int_sole_universe)//'.stl'

      return
      end
      

      
      subroutine int_sole_gyroid_filenames(sole_index,int_sole_gyroid_obj,int_sole_gyroid_stl)
      implicit none
      include 'max.inc'
c     
c     Purpose: to provide the name of the files used to record the
c     interface between a sole and the gyroid
c     
c     Input:
c       + sole_index: sole index
c     
c     Output:
c       + int_sole_gyroid_obj: obj file
c       + int_sole_gyroid_stl: stl file
c     
c     I/O
      integer sole_index
      character*(Nchar_mx) int_sole_gyroid_obj
      character*(Nchar_mx) int_sole_gyroid_stl
c     temp
      character*(Nchar_mx) int_sole_gyroid
      character*(Nchar_mx) index_str
c     label
      character*(Nchar_mx) label
      label='subroutine int_sole_gyroid_filenames'

      call fstr(sole_index,index_str)
      int_sole_gyroid='./results/interface_sole'//trim(index_str)//'_gyroid'
      int_sole_gyroid_obj=trim(int_sole_gyroid)//'.obj'
      int_sole_gyroid_stl=trim(int_sole_gyroid)//'.stl'

      return
      end
      
      
      
      subroutine patch_name(Npatch,patch_index,patch_index_str)
      implicit none
      include 'max.inc'
c     
c     Purpose: to produce the patch index character string
c     
c     Input:
c       + Npatch: number of lateral patches
c       + patch_index: index of the patch
c     
c     Output:
c       + patch_index_str: character string for the patch index
c     
c     I/O
      integer Npatch
      integer patch_index
      character*(Nchar_mx) patch_index_str
c     temp
      character*(Nchar_mx) str
      logical err_code
c     label
      character*(Nchar_mx) label
      label='subroutine patch_name'

      call num2str(patch_index,str,err_code)
      if (err_code) then
         call error(label)
         write(*,*) 'Could not convert to character string: patch_index=',patch_index
         stop
      endif
      if (Npatch.lt.100) then
         if (patch_index.lt.10) then
            patch_index_str='0'//trim(str)
         else if (patch_index.lt.100) then
            patch_index_str=trim(str)
         else
            write(*,*) 'patch_index=',patch_index,'> Npatch=',Npatch
            write(*,*) 'how is that even possible'
            stop
         endif
      else if (Npatch.lt.1000) then
         if (patch_index.lt.10) then
            patch_index_str='00'//trim(str)
         else if (patch_index.lt.100) then
            patch_index_str='0'//trim(str)
         else if (patch_index.lt.1000) then
            patch_index_str=trim(str)
         else
            write(*,*) 'patch_index=',patch_index,'> Npatch=',Npatch
            write(*,*) 'how is that even possible'
            stop
         endif
      else if (Npatch.lt.10000) then
         if (patch_index.lt.10) then
            patch_index_str='000'//trim(str)
         else if (patch_index.lt.100) then
            patch_index_str='00'//trim(str)
         else if (patch_index.lt.1000) then
            patch_index_str='0'//trim(str)
         else if (patch_index.lt.10000) then
            patch_index_str=trim(str)
         else
            write(*,*) 'patch_index=',patch_index,'> Npatch=',Npatch
            write(*,*) 'how is that even possible'
            stop
         endif
      else
         call error(label)
         write(*,*) 'Npatch=',Npatch,' > 10000'
         write(*,*) 'that is a lot'
         stop
      endif

      return
      end
