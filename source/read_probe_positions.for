c     Copyright (C) 2023 |Meso|Star> (contact@meso-star.com)
      subroutine read_probe_positions(datafile,dim,L,sf,mtllib_file,Nmat,materials_list,Nprobe,probe_position)
      implicit none
      include 'max.inc'
      include 'size_params.inc'
c     
c     Purpose: read probe positions from file
c     
c     Input:
c       + datafile: input data file
c       + dim: dimension of space
c       + L: typical length over each direction [m]
c       + sf: global scaling factor
c       + mtllib_file: file used to store materials colors
c       + Nmat: number of materials
c       + materials_list: list of materials
c     
c     Output:
c       + Nprobe: number of probe positions
c       + probe_position: list of probe positions
c     
c     I/O
      character*(Nchar_mx) datafile
      integer dim
      double precision L(1:Ndim_mx)
      double precision sf
      character*(Nchar_mx) mtllib_file
      integer Nmat
      character*(Nchar_mx) materials_list(1:Nmaterial_mx)
      integer Nprobe
      double precision probe_position(1:Nprobe_mx,1:Ndim_mx)
c     temp
      integer i,j,ios,read_ios,iface
      logical keep_reading
      double precision tmp(1:Ndim_mx)
      character*(Nchar_mx) istr2
      logical err_code
      integer Nv01,Nf01
      double precision v01(1:Nv_so_mx,1:Ndim_mx)
      integer f01(1:Nf_so_mx,1:Nvinface)
      integer Nv02,Nf02
      double precision v02(1:Nv_so_mx,1:Ndim_mx)
      integer f02(1:Nf_so_mx,1:Nvinface)
      double precision center(1:Ndim_mx)
      character*(Nchar_mx) obj_file
      logical display
      logical invert_normal
      integer material_index(1:Nf_so_mx)
c     parameters
      double precision radius
      parameter(radius=5.0D-3)
      integer Ntheta
      parameter(Ntheta=8)
      integer Nphi
      parameter(Nphi=16)
c     label
      character*(Nchar_mx) label
      label='subroutine read_probe_position'

      open(11,file=trim(datafile),status='old',iostat=ios)
      if (ios.ne.0) then
         call error(label)
         write(*,*) 'File not found: ',trim(datafile)
         stop
      else
         write(*,*) 'Reading file: ',trim(datafile)
         do i=1,3
            read(11,*)
         enddo                  ! i
         Nprobe=0
         keep_reading=.true.
         do while (keep_reading)
            read(11,*,iostat=read_ios) (tmp(j),j=1,dim)
            if (read_ios.eq.0) then
               Nprobe=Nprobe+1
               if (Nprobe.gt.Nprobe_mx) then
                  call error(label)
                  write(*,*) 'Nprobe_mx has been reached'
                  stop
               endif
               do j=1,dim
                  probe_position(Nprobe,j)=tmp(j)
                  if ((probe_position(Nprobe,j).lt.-L(j)).or.(probe_position(Nprobe,j).gt.L(j))) then
                     write(*,*) 'Probe position index:',Nprobe
                     write(*,*) 'Dimension ',j,':',probe_position(Nprobe,j)
                     write(*,*) 'is out of domain bounds: [',-L(j),', ',L(j),']'
                     stop
                  else
                     probe_position(Nprobe,j)=probe_position(Nprobe,j)*sf
                  endif
               enddo            ! j
            else
               keep_reading=.false.
            endif
         enddo                  ! while (keep_reading)
      endif
      close(11)

      call sphere_obj(dim,radius,Ntheta,Nphi,Nv01,Nf01,v01,f01)
      display=.false.
      invert_normal=.false.
      do i=1,Nprobe
         do j=1,dim
            center(j)=probe_position(i,j)
         enddo                  ! j
         call copy_sobj(dim,Nv01,Nf01,v01,f01,Nv02,Nf02,v02,f02)
         call move_obj(dim,Nv02,v02,center)
         call num2str2(i,istr2,err_code)
         if (err_code) then
            call error(label)
            write(*,*) 'Could not convert to character string: i=',i
            stop
         else
            obj_file='./results/probe_position_'//trim(istr2)//'.obj'
            do iface=1,Nf02
               material_index(iface)=3
            enddo               ! iface
            call record_single_obj(dim,obj_file,display,invert_normal,Nv02,Nf02,v02,f02,sf,mtllib_file,Nmat,materials_list,material_index)
         endif
      enddo                     ! i

      return
      end
