c     Copyright (C) 2023 |Meso|Star> (contact@meso-star.com)
      subroutine trianglemesh_area(dim,Nv,Nf,v,f,area)
      implicit none
      include 'max.inc'
c
c     Purpose: to compute the area of a trianglemesh
c     
c     Input:
c       + dim: dimension of space 
c       + Nv, v, Nf, f: definition of the trianglmesh
c     
c     Output:
c       + area: area of the trianglmesh
c     
c     I/O
      integer dim
      integer Nv
      integer Nf
      double precision v(1:Nv_so_mx,1:Ndim_mx)
      integer f(1:Nf_so_mx,1:3)
      double precision area
c     temp
      integer iface
      integer j
      double precision p1(1:Ndim_mx)
      double precision p2(1:Ndim_mx)
      double precision p3(1:Ndim_mx)
      double precision dA
c     label
      character*(Nchar_mx) label
      label='subroutine trianglemesh_area'

      area=0.0D+0
      do iface=1,Nf
         do j=1,dim
            p1(j)=v(f(iface,1),j)
            p2(j)=v(f(iface,2),j)
            p3(j)=v(f(iface,3),j)
         enddo                  ! j
         call triangle_area(dim,p1,p2,p3,dA)
         area=area+dA
c     Debug
c         if (dA.lt.0.0D+0) then
c            write(*,*) 'iface=',iface
c            write(*,*) 'dA=',dA
c            write(*,*) 'p1=',p1
c            write(*,*) 'p2=',p2
c            write(*,*) 'p3=',p3
c            stop
c         endif
c     Debug
      enddo ! iface
      
      return
      end



      subroutine triangle_area(dim,p1,p2,p3,area)
      implicit none
      include 'max.inc'
c
c     Purpose: to compute the area of a triangle
c     
c     Input:
c       + dim: dimension of space 
c       + p1: coordinates of the 1st point
c       + p2: coordinates of the 2nd point
c       + p3: coordinates of the 3rd point
c     
c     Output:
c       + area: area of the triangle
c     
c     I/O
      integer dim
      double precision p1(1:Ndim_mx)
      double precision p2(1:Ndim_mx)
      double precision p3(1:Ndim_mx)
      double precision area
c     temp
      double precision v12(1:Ndim_mx)
      double precision v13(1:Ndim_mx)
      double precision w(1:Ndim_mx)
      double precision length
c     label
      character*(Nchar_mx) label
      label='subroutine triangle_area'

      call substract_vectors(dim,p2,p1,v12)
      call substract_vectors(dim,p3,p1,v13)
      call vector_product(dim,v12,v13,w)
      call vector_length(dim,w,length)
      area=length/2.0D+0

      return
      end



      subroutine tetrahedron_volume(dim,p1,p2,p3,p4,volume)
      implicit none
      include 'max.inc'
c
c     Purpose: to compute the volume of a tetrahedron from the
c     coordinates of its 4 nodes.
c     Note: the volume will be positive when (p1, p2, p3) define
c     a triangular base of the tetrahedron, provided a order such
c     that vector n=(p1p2)x(p1p3) is directed in the same direction
c     as vector (p1p4).
c     
c     Input:
c       + dim: dimension of space 
c       + p1: coordinates of the 1st point
c       + p2: coordinates of the 2nd point
c       + p3: coordinates of the 3rd point
c       + p4: coordinates of the 4th point
c     
c     Output:
c       + area: area of the triangle
c     
c     I/O
      integer dim
      double precision p1(1:Ndim_mx)
      double precision p2(1:Ndim_mx)
      double precision p3(1:Ndim_mx)
      double precision p4(1:Ndim_mx)
      double precision volume
c     temp
      double precision u12(1:Ndim_mx)
      double precision u13(1:Ndim_mx)
      double precision u14(1:Ndim_mx)
      double precision n(1:Ndim_mx)
      double precision product
c     label
      character*(Nchar_mx) label
      label='subroutine tetrahedron_volume'

      call substract_vectors(dim,p2,p1,u12)
      call substract_vectors(dim,p3,p1,u13)
      call substract_vectors(dim,p4,p1,u14)
      call vector_product(dim,u12,u13,n)
      call vector_vector(dim,n,u14,product)
      volume=product/6.0D+0
      
      return
      end



      subroutine enclosure_volume(dim,Nv,Nf,v,f,volume)
      implicit none
      include 'max.inc'
      include 'size_params.inc'
c     
c     Purpose: to evaluate the volume of a enclosure
c     
c     Input:
c       + Nv,Nf,v,f: defintion of the enclosure as a trianglemesh
c     
c     Output:
c       + volume: enclosed volume
c     
c     I/O
      integer dim
      integer Nv
      integer Nf
      double precision v(1:Nv_mx,1:Ndim_mx)
      integer f(1:Nf_mx,1:Nvinface)
      double precision volume
c     temp
      integer vidx,fidx,j,i
      double precision minmax(1:Ndim_mx,1:2)
      double precision p0(1:Ndim_mx)
      double precision p1(1:Ndim_mx)
      double precision p2(1:Ndim_mx)
      double precision p3(1:Ndim_mx)
      double precision dV
c     label
      character*(Nchar_mx) label
      label='subroutine enclosure_volume'

c     get the min and max of the enclose along each direction
      do j=1,dim
         minmax(j,2)=v(1,j)
      enddo                     ! j
      do vidx=1,Nv
         do j=1,dim
            if (v(vidx,j).gt.minmax(j,2)) then
               minmax(j,2)=v(vidx,j)
            endif
         enddo                  ! j
      enddo                     ! vidx
      do j=1,dim
         minmax(j,1)=minmax(j,2)
      enddo                     ! j
      do vidx=1,Nv
         do j=1,dim
            if (v(vidx,j).lt.minmax(j,1)) then
               minmax(j,1)=v(vidx,j)
            endif
         enddo                  ! j
      enddo                     ! vidx
      
c     guess a central position
      do j=1,dim
         p0(j)=(minmax(j,1)+minmax(j,2))/2.0D+0
      enddo                     ! j

c     compute the volume
      volume=0.0D+0
      do fidx=1,Nf
         do j=1,dim
            p1(j)=v(f(fidx,1),j)
            p2(j)=v(f(fidx,2),j)
            p3(j)=v(f(fidx,3),j)
         enddo                  ! j
         call tetrahedron_volume(dim,p1,p2,p3,p0,dV)
         volume=volume+dV
      enddo                     ! fidx
            
      return
      end


      
      subroutine distance_to_vector(dim,x1,x2,P,dmin,A,
     &     A_is_at_end,end_side)
      implicit none
      include 'max.inc'
c     
c     Purpose: to compute the distance between a given vector and a position
c     
c     Input:
c       + dim: dimension of space
c       + x1: first position that defines the vector
c       + x2: second position that defines the vector
c       + P: position
c     
c     Output:
c       + dmin: smallest distance between P and vector
c       + A: closest position to P that belongs to the vector
c       + A_is_at_end: true if A is at one end of the [x1,x2] segment
c       + end_side: index (1 or 2) or the end side if A_is_at_end=T
c     
c     I/O
      integer dim
      double precision x1(1:Ndim_mx)
      double precision x2(1:Ndim_mx)
      double precision P(1:Ndim_mx)
      double precision dmin
      double precision A(1:Ndim_mx)
      logical A_is_at_end
      integer end_side
c     temp
      integer j
      double precision u(1:Ndim_mx)
      double precision v(1:Ndim_mx)
      double precision nu(1:Ndim_mx)
      double precision tmp(1:Ndim_mx)
      double precision AP(1:Ndim_mx)
      double precision norm_u,norm_v,L1,d,sp
c     label
      character*(Nchar_mx) label
      label='subroutine distance_to_vector'

      call substract_vectors(dim,x2,x1,u)
      call normalize_vector(dim,u,nu)
      call substract_vectors(dim,P,x1,v)
      call vector_length(dim,u,norm_u)
      call vector_length(dim,v,norm_v)
      call vector_vector(dim,u,v,sp)
      if (norm_u.le.0.0D+0) then
         call error(label)
         write(*,*) 'norm_u=',norm_u
         write(*,*) 'u=',u
         write(*,*) 'x1=',x1
         write(*,*) 'x2=',x2
         stop
      else
         L1=sp/norm_u
      endif
      if (L1.le.0.0D+0) then
         call copy_vector(dim,x1,A)
         do j=1,dim
            tmp(j)=0.0D+0
         enddo                  ! j
         A_is_at_end=.true.
         end_side=1
      else if (L1.ge.norm_u) then
         call copy_vector(dim,x2,A)
         call copy_vector(dim,u,tmp)
         A_is_at_end=.true.
         end_side=2
      else
         call scalar_vector(dim,L1,nu,tmp)
         call add_vectors(dim,x1,tmp,A)
         A_is_at_end=.false.
      endif
      call substract_vectors(dim,v,tmp,AP)
      call vector_length(dim,AP,dmin)

      return
      end



      subroutine face_normal(dim,Nv,Nf,vertices,faces,face_idx,normal)
      implicit none
      include 'max.inc'
      include 'size_params.inc'
c     
c     Purpose: to compute a given face' normal
c     
c     Input:
c       + dim: dimension of space
c       + Nv, Nf, vertices, faces: definition of the object
c       + face_idx: face index
c     
c     Output:
c       + normal: normal to face index "face_idx"
c     
c     I/O
      integer dim
      integer Nv,Nf
      double precision vertices(1:Nv_mx,1:Ndim_mx)
      integer faces(1:Nf_mx,1:Nvinface)
      integer face_idx
      double precision normal(1:Ndim_mx)
c     temp
      double precision P1(1:Ndim_mx)
      double precision P2(1:Ndim_mx)
      double precision P3(1:Ndim_mx)
      double precision u12(1:Ndim_mx)
      double precision u13(1:Ndim_mx)
      integer j
c     label
      character*(Nchar_mx) label
      label='subroutine face_normal'

      do j=1,dim
         P1(j)=vertices(faces(face_idx,1),j)
         P2(j)=vertices(faces(face_idx,2),j)
         P3(j)=vertices(faces(face_idx,3),j)
      enddo                     ! j
      call substract_vectors(dim,P2,P1,u12)
      call substract_vectors(dim,P3,P1,u13)
      call vector_product_normalized(dim,u12,u13,normal)

      return
      end




      subroutine face_so_normal(dim,Nv,Nf,vertices,faces,face_idx,normal)
      implicit none
      include 'max.inc'
      include 'size_params.inc'
c     
c     Purpose: to compute a given face' normal
c     
c     Input:
c       + dim: dimension of space
c       + Nv, Nf, vertices, faces: definition of the object
c       + face_idx: face index
c     
c     Output:
c       + normal: normal to face index "face_idx"
c     
c     I/O
      integer dim
      integer Nv,Nf
      double precision vertices(1:Nv_so_mx,1:Ndim_mx)
      integer faces(1:Nf_so_mx,1:Nvinface)
      integer face_idx
      double precision normal(1:Ndim_mx)
c     temp
      double precision P1(1:Ndim_mx)
      double precision P2(1:Ndim_mx)
      double precision P3(1:Ndim_mx)
      double precision u12(1:Ndim_mx)
      double precision u13(1:Ndim_mx)
      integer j
c     label
      character*(Nchar_mx) label
      label='subroutine face_so_normal'

      do j=1,dim
         P1(j)=vertices(faces(face_idx,1),j)
         P2(j)=vertices(faces(face_idx,2),j)
         P3(j)=vertices(faces(face_idx,3),j)
      enddo                     ! j
      call substract_vectors(dim,P2,P1,u12)
      call substract_vectors(dim,P3,P1,u13)
      call vector_product_normalized(dim,u12,u13,normal)

      return
      end
