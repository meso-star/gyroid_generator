c     Copyright (C) 2023 |Meso|Star> (contact@meso-star.com)
      subroutine get_bounding_box(dim,Npatch,vg_idx,ipatch,vg_bbox,bounding_box)
      implicit none
      include 'max.inc'
c
c     Purpose: to get the bounding box for a given patch
c
c     Input:
c       + dim: dimension of space
c       + Npatch: number of patches that match the direction/side
c       + vg_idx: indexes of the corresponding vertices groups
c       + ipatch: index of the required patch
c       + vg_bbox: bounding box for each vertices group
c
c     Output:
c       + bounding_box: bounding box for the required patch
c
c     I/O
      integer dim
      integer Npatch
      integer vg_idx(1:Npatch_mx)
      integer ipatch
      double precision vg_bbox(1:Ngroup_mx,1:Ndim_mx,1:2)
      double precision bounding_box(1:Ndim_mx,1:2)
c     temp
      integer vg_index,idim,j
c     label
      character*(Nchar_mx) label
      label='subroutine get_bounding_box'
      
      if ((ipatch.lt.1).or.(ipatch.gt.Npatch)) then
         call error(label)
         write(*,*) 'ipatch=',ipatch
         write(*,*) 'should be in the [1,',Npatch,'] range'
         stop
      else
         vg_index=vg_idx(ipatch)
      endif
      do idim=1,dim
         do j=1,2
            bounding_box(idim,j)=vg_bbox(vg_index,idim,j)
         enddo                  ! j
      enddo                     ! idim      

      return
      end



      subroutine identify_patches(dim,L,Nvg,vg_dim_side,vg_bbox,direction,side,Npatch,vg_idx,sorting_dir)
      implicit none
      include 'max.inc'
c     
c     Purpose: to identify patches for a given direction / side
c     
c     Input:
c       + dim: dimension of space
c       + L: typical length over each direction [m]
c       + Nvg: number of vertices groups
c       + vg_dim_side: dimension and side for each vertices group
c       + vg_bbox: bounding box for each vertices group
c       + direction: required direction
c       + side: required side
c     
c     Output:
c       + Npatch: number of patches that match the direction/side
c       + vg_idx: indexes of the corresponding vertices groups
c       + sorting_dir: sorting direction for the required direction/side
c     
c     I/O
      integer dim
      double precision L(1:Ndim_mx)
      integer Nvg
      integer vg_dim_side(1:Ngroup_mx,1:2)
      double precision vg_bbox(1:Ngroup_mx,1:Ndim_mx,1:2)
      integer direction
      integer side
      integer Npatch
      integer vg_idx(1:Npatch_mx)
      integer sorting_dir
c     temp
      integer i,idim,j
      double precision bounding_box(1:Ndim_mx,1:2)
      logical sorting_dir_set
c     label
      character*(Nchar_mx) label
      label='subroutine identify_patches'

c     Debug
c$$$      write(*,*) 'Nvg=',Nvg
c$$$      do i=1,Nvg
c$$$         write(*,*) 'vg index:',i
c$$$         write(*,*) 'vg_dim_side=',(vg_dim_side(i,j),j=1,2)
c$$$         write(*,*) 'bounding box:'
c$$$         do idim=1,dim
c$$$            write(*,*) (vg_bbox(i,idim,j),j=1,2)
c$$$         enddo                  ! idim
c$$$      enddo                     ! i
c     Debug
      
      Npatch=0
      sorting_dir_set=.false.
      do i=1,Nvg
         if ((vg_dim_side(i,1).eq.direction).and.(vg_dim_side(i,2).eq.side)) then
            Npatch=Npatch+1
            if (Npatch.gt.Npatch_mx) then
               call error(label)
               write(*,*) 'Npatch_mx has been reached'
               stop
            endif
            vg_idx(Npatch)=i
            if (.not.sorting_dir_set) then
               do idim=1,dim
                  do j=1,2
                     bounding_box(idim,j)=vg_bbox(i,idim,j)
                  enddo         ! j
               enddo            ! idim
               do idim=1,dim
                  if ((bounding_box(idim,1).le.-L(idim)).and.(bounding_box(idim,2).ge.L(idim))) then
c     Debug
c$$$                     write(*,*) 'direction=',direction,' side=',side
c$$$                     write(*,*) 'sorting_dir=',idim
c$$$                     write(*,*) 'bounding_box(',idim,')=',(bounding_box(idim,j),j=1,2)
c$$$                     stop
c     Debug
                     sorting_dir=idim
                     sorting_dir_set=.true.
                     goto 111
                  endif
               enddo            ! idim
            endif               ! sorting_dir_set=F
 111        continue
         endif
      enddo                     ! i

      return
      end
      


      subroutine set_bounding_box(dim,L,edge_index,ipatch,bounding_box,sorting_dir,mat_index)
      implicit none
      include 'max.inc'
c     
c     Purpose: to set bounding box for given face / direction
c     
c     Input:
c       + dim: dimension of space
c       + L: typical length over each direction [m]
c       + edge_index:
c         - edge_indexx(1)=1-3 (direction)
c         - edge_index(2)=1 or 2 (negative / positive side)
c       + ipatch: patch index
c     
c     Output:
c       + bounding_box: bounding box to look vertices into
c       + sorting_dir: direction vertices must be sorted to
c       + mat_index: material index
c     
c     I/O
      integer dim
      double precision L(1:Ndim_mx)
      integer edge_index(1:2)
      integer ipatch
      double precision bounding_box(1:Ndim_mx,1:2)
      integer sorting_dir
      integer mat_index
c     temp
c     label
      character*(Nchar_mx) label
      label='subroutine set_bounding_box'

      if (edge_index(1).eq.1) then
         if (edge_index(2).eq.1) then
            sorting_dir=2
            mat_index=2
            bounding_box(1,1)=-L(1)*1.1D+0
            bounding_box(1,2)=-L(1)*0.75D+0
            if (ipatch.eq.1) then
               bounding_box(2,1)=-L(2)
               bounding_box(2,2)=0.0D+0
               bounding_box(3,1)=-L(3)*1.1D+0
               bounding_box(3,2)=-L(3)*0.75
            else if (ipatch.eq.2) then
               bounding_box(2,1)=0.0D+0
               bounding_box(2,2)=L(2)
               bounding_box(3,1)=-L(3)*1.1D+0
               bounding_box(3,2)=-L(3)*0.75
            else if (ipatch.eq.3) then
               bounding_box(2,1)=-L(2)*1.10D+0
               bounding_box(2,2)=L(2)*1.10D+0
               bounding_box(3,1)=-L(3)*0.75D+0
               bounding_box(3,2)=-L(3)*0.25
            else if (ipatch.eq.4) then
               bounding_box(2,1)=-L(2)*1.10D+0
               bounding_box(2,2)=L(2)*1.10D+0
               bounding_box(3,1)=-L(3)*0.25D+0
               bounding_box(3,2)=L(3)*0.25
            else if (ipatch.eq.5) then
               bounding_box(2,1)=-L(2)*1.10D+0
               bounding_box(2,2)=L(2)*1.10D+0
               bounding_box(3,1)=L(3)*0.25D+0
               bounding_box(3,2)=L(3)*0.75D+0
            else if (ipatch.eq.6) then
               bounding_box(2,1)=-L(2)*1.10D+0
               bounding_box(2,2)=-L(2)*0.25D+0
               bounding_box(3,1)=L(3)*0.75D+0
               bounding_box(3,2)=L(3)*1.1D+0
            else if (ipatch.eq.7) then
               bounding_box(2,1)=-L(2)*0.25D+0
               bounding_box(2,2)=L(2)*0.75D+0
               bounding_box(3,1)=L(3)*0.75D+0
               bounding_box(3,2)=L(3)*1.1D+0
            else
               call error(label)
               write(*,*) 'ipatch=',ipatch
               write(*,*) 'should be in the [1, 7] range'
               stop
            endif               ! ipatch
         else if (edge_index(2).eq.2) then
            sorting_dir=2
            mat_index=3
            bounding_box(1,1)=L(1)*0.75D+0
            bounding_box(1,2)=L(1)*1.1D+0
            if (ipatch.eq.1) then
               bounding_box(2,1)=-L(2)*0.5D+0
               bounding_box(2,2)=L(2)*0.5D+0
               bounding_box(3,1)=-L(3)*1.1D+0
               bounding_box(3,2)=-L(3)*0.75
            else if (ipatch.eq.2) then
               bounding_box(2,1)=L(2)*0.50D+0
               bounding_box(2,2)=L(2)*1.1D+0
               bounding_box(3,1)=-L(3)*1.1D+0
               bounding_box(3,2)=-L(3)*0.75
            else if (ipatch.eq.3) then
               bounding_box(2,1)=-L(2)*1.10D+0
               bounding_box(2,2)=L(2)*1.10D+0
               bounding_box(3,1)=-L(3)*0.75D+0
               bounding_box(3,2)=-L(3)*0.25
            else if (ipatch.eq.4) then
               bounding_box(2,1)=-L(2)*1.10D+0
               bounding_box(2,2)=L(2)*1.10D+0
               bounding_box(3,1)=-L(3)*0.25D+0
               bounding_box(3,2)=L(3)*0.25
            else if (ipatch.eq.5) then
               bounding_box(2,1)=-L(2)*1.10D+0
               bounding_box(2,2)=L(2)*1.10D+0
               bounding_box(3,1)=L(3)*0.25D+0
               bounding_box(3,2)=L(3)*0.75D+0
            else if (ipatch.eq.6) then
               bounding_box(2,1)=-L(2)*1.10D+0
               bounding_box(2,2)=-L(2)*0.25D+0
               bounding_box(3,1)=L(3)*0.75D+0
               bounding_box(3,2)=L(3)*1.1D+0
            else if (ipatch.eq.7) then
               bounding_box(2,1)=-L(2)*0.25D+0
               bounding_box(2,2)=L(2)*0.75D+0
               bounding_box(3,1)=L(3)*0.75D+0
               bounding_box(3,2)=L(3)*1.1D+0
            else
               call error(label)
               write(*,*) 'ipatch=',ipatch
               write(*,*) 'should be in the [1, 7] range'
               stop
            endif               ! ipatch
         else
            call error(label)
            write(*,*) 'edge_index(2)=',edge_index(2)
            write(*,*) 'should be in [1, 2] range'
            stop
         endif                  ! edge_index(2)
      else if (edge_index(1).eq.2) then
         if (edge_index(2).eq.1) then
            sorting_dir=3
            mat_index=4
            bounding_box(2,1)=-L(2)*1.1D+0
            bounding_box(2,2)=-L(2)*0.75D+0
            if (ipatch.eq.1) then
               bounding_box(1,1)=-L(1)*1.1D+0
               bounding_box(1,2)=-L(1)*0.75D+0
               bounding_box(3,1)=-L(3)*0.75D+0
               bounding_box(3,2)=L(3)*0.25D+0
            else if (ipatch.eq.2) then
               bounding_box(1,1)=-L(1)*1.1D+0
               bounding_box(1,2)=-L(1)*0.75D+0
               bounding_box(3,1)=L(3)*0.25D+0
               bounding_box(3,2)=L(3)*1.1D+0
            else if (ipatch.eq.3) then
               bounding_box(1,1)=-L(1)*0.75D+0
               bounding_box(1,2)=-L(1)*0.25D+0
               bounding_box(3,1)=-L(3)*1.1D+0
               bounding_box(3,2)=L(3)*1.1D+0
            else if (ipatch.eq.4) then
               bounding_box(1,1)=-L(1)*0.25D+0
               bounding_box(1,2)=L(1)*0.25D+0
               bounding_box(3,1)=-L(3)*1.1D+0
               bounding_box(3,2)=L(3)*1.1D+0
            else if (ipatch.eq.5) then
               bounding_box(1,1)=L(1)*0.25D+0
               bounding_box(1,2)=L(1)*0.75D+0
               bounding_box(3,1)=-L(3)*1.1D+0
               bounding_box(3,2)=L(3)*1.1D+0
            else if (ipatch.eq.6) then
               bounding_box(1,1)=L(1)*0.75D+0
               bounding_box(1,2)=L(1)*1.1D+0
               bounding_box(3,1)=-L(3)*1.1D+0
               bounding_box(3,2)=-L(3)*0.25D+0
            else if (ipatch.eq.7) then
               bounding_box(1,1)=L(1)*0.75D+0
               bounding_box(1,2)=L(1)*1.1D+0
               bounding_box(3,1)=-L(3)*0.25D+0
               bounding_box(3,2)=L(3)*0.75D+0
            else
               call error(label)
               write(*,*) 'ipatch=',ipatch
               write(*,*) 'should be in the [1, 7] range'
               stop
            endif               ! ipatch
         else if (edge_index(2).eq.2) then
            sorting_dir=3
            mat_index=4
            bounding_box(2,1)=L(2)*0.75D+0
            bounding_box(2,2)=L(2)*1.1D+0
            if (ipatch.eq.1) then
               bounding_box(1,1)=-L(1)*1.1D+0
               bounding_box(1,2)=-L(1)*0.75D+0
               bounding_box(3,1)=-L(3)*0.75D+0
               bounding_box(3,2)=L(3)*0.25D+0
            else if (ipatch.eq.2) then
               bounding_box(1,1)=-L(1)*1.1D+0
               bounding_box(1,2)=-L(1)*0.75D+0
               bounding_box(3,1)=L(3)*0.25D+0
               bounding_box(3,2)=L(3)*1.1D+0
            else if (ipatch.eq.3) then
               bounding_box(1,1)=-L(1)*0.75D+0
               bounding_box(1,2)=-L(1)*0.25D+0
               bounding_box(3,1)=-L(3)*1.1D+0
               bounding_box(3,2)=L(3)*1.1D+0
            else if (ipatch.eq.4) then
               bounding_box(1,1)=-L(1)*0.25D+0
               bounding_box(1,2)=L(1)*0.25D+0
               bounding_box(3,1)=-L(3)*1.1D+0
               bounding_box(3,2)=L(3)*1.1D+0
            else if (ipatch.eq.5) then
               bounding_box(1,1)=L(1)*0.25D+0
               bounding_box(1,2)=L(1)*0.75D+0
               bounding_box(3,1)=-L(3)*1.1D+0
               bounding_box(3,2)=L(3)*1.1D+0
            else if (ipatch.eq.6) then
               bounding_box(1,1)=L(1)*0.75D+0
               bounding_box(1,2)=L(1)*1.1D+0
               bounding_box(3,1)=-L(3)*1.1D+0
               bounding_box(3,2)=-L(3)*0.25D+0
            else if (ipatch.eq.7) then
               bounding_box(1,1)=L(1)*0.75D+0
               bounding_box(1,2)=L(1)*1.1D+0
               bounding_box(3,1)=-L(3)*0.25D+0
               bounding_box(3,2)=L(3)*0.75D+0
            else
               call error(label)
               write(*,*) 'ipatch=',ipatch
               write(*,*) 'should be in the [1, 7] range'
               stop
            endif               ! ipatch
         else
            call error(label)
            write(*,*) 'edge_index(2)=',edge_index(2)
            write(*,*) 'should be in [1, 2] range'
            stop
         endif                  ! edge_index(2)
      else if (edge_index(1).eq.3) then
         if (edge_index(2).eq.1) then
            sorting_dir=1
            mat_index=4
            bounding_box(3,1)=-L(3)*1.1D+0
            bounding_box(3,2)=-L(3)*0.75D+0
            if (ipatch.eq.1) then
               bounding_box(1,1)=-L(1)*0.75D+0
               bounding_box(1,2)=L(1)*0.25D+0
               bounding_box(2,1)=-L(2)*1.1D+0
               bounding_box(2,2)=-L(2)*0.75D+0
            else if (ipatch.eq.2) then
               bounding_box(1,1)=L(1)*0.25D+0
               bounding_box(1,2)=L(1)*1.1D+0
               bounding_box(2,1)=-L(2)*1.1D+0
               bounding_box(2,2)=-L(2)*0.75D+0
            else if (ipatch.eq.3) then
               bounding_box(1,1)=-L(1)*1.1D+0
               bounding_box(1,2)=L(1)*1.1D+0
               bounding_box(2,1)=-L(2)*0.75D+0
               bounding_box(2,2)=-L(2)*0.25D+0
            else if (ipatch.eq.4) then
               bounding_box(1,1)=-L(1)*1.1D+0
               bounding_box(1,2)=L(1)*1.1D+0
               bounding_box(2,1)=-L(2)*0.25D+0
               bounding_box(2,2)=L(2)*0.25D+0
            else if (ipatch.eq.5) then
               bounding_box(1,1)=-L(1)*1.1D+0
               bounding_box(1,2)=L(1)*1.1D+0
               bounding_box(2,1)=L(2)*0.25D+0
               bounding_box(2,2)=L(2)*0.75D+0
            else if (ipatch.eq.6) then
               bounding_box(1,1)=-L(1)*1.1D+0
               bounding_box(1,2)=-L(1)*0.25D+0
               bounding_box(2,1)=L(2)*0.75D+0
               bounding_box(2,2)=L(2)*1.1D+0
            else if (ipatch.eq.7) then
               bounding_box(1,1)=-L(1)*0.25D+0
               bounding_box(1,2)=L(1)*0.75D+0
               bounding_box(2,1)=L(2)*0.75D+0
               bounding_box(2,2)=L(2)*1.1D+0
            else
               call error(label)
               write(*,*) 'ipatch=',ipatch
               write(*,*) 'should be in the [1, 7] range'
               stop
            endif               ! ipatch
         else if (edge_index(2).eq.2) then
            sorting_dir=1
            mat_index=4
            bounding_box(3,1)=L(3)*0.75D+0
            bounding_box(3,2)=L(3)*1.1D+0
            if (ipatch.eq.1) then
               bounding_box(1,1)=-L(1)*0.75D+0
               bounding_box(1,2)=L(1)*0.25D+0
               bounding_box(2,1)=-L(2)*1.1D+0
               bounding_box(2,2)=-L(2)*0.75D+0
            else if (ipatch.eq.2) then
               bounding_box(1,1)=L(1)*0.25D+0
               bounding_box(1,2)=L(1)*1.1D+0
               bounding_box(2,1)=-L(2)*1.1D+0
               bounding_box(2,2)=-L(2)*0.75D+0
            else if (ipatch.eq.3) then
               bounding_box(1,1)=-L(1)*1.1D+0
               bounding_box(1,2)=L(1)*1.1D+0
               bounding_box(2,1)=-L(2)*0.75D+0
               bounding_box(2,2)=-L(2)*0.25D+0
            else if (ipatch.eq.4) then
               bounding_box(1,1)=-L(1)*1.1D+0
               bounding_box(1,2)=L(1)*1.1D+0
               bounding_box(2,1)=-L(2)*0.25D+0
               bounding_box(2,2)=L(2)*0.25D+0
            else if (ipatch.eq.5) then
               bounding_box(1,1)=-L(1)*1.1D+0
               bounding_box(1,2)=L(1)*1.1D+0
               bounding_box(2,1)=L(2)*0.25D+0
               bounding_box(2,2)=L(2)*0.75D+0
            else if (ipatch.eq.6) then
               bounding_box(1,1)=-L(1)*1.1D+0
               bounding_box(1,2)=-L(1)*0.25D+0
               bounding_box(2,1)=L(2)*0.75D+0
               bounding_box(2,2)=L(2)*1.1D+0
            else if (ipatch.eq.7) then
               bounding_box(1,1)=-L(1)*0.25D+0
               bounding_box(1,2)=L(1)*0.75D+0
               bounding_box(2,1)=L(2)*0.75D+0
               bounding_box(2,2)=L(2)*1.1D+0
            else
               call error(label)
               write(*,*) 'ipatch=',ipatch
               write(*,*) 'should be in the [1, 7] range'
               stop
            endif               ! ipatch
         else
            call error(label)
            write(*,*) 'edge_index(2)=',edge_index(2)
            write(*,*) 'should be in [1, 2] range'
            stop
         endif                  ! edge_index(2)
      else
         call error(label)
         write(*,*) 'edge_index(1)=',edge_index(1)
         write(*,*) 'should be in [1,',dim,'] range'
         stop
      endif                     ! edge_index(1)

      return
      end
