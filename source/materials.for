c     Copyright (C) 2023 |Meso|Star> (contact@meso-star.com)
      subroutine add_material(Nmat,materials_list,material,mat_index)
      implicit none
      include 'max.inc'
c     
c     Purpose: to add a material to the list of materials (if not already present)
c     
c     Input:
c       + Nmat: number of materials in the list of materials
c       + materials_list: list of materials
c       + material: material to add to the list
c     
c     Output:
c       + Nmat, materials_list: modified
c       + mat_index: index of the material in the list
c     
c     I/O
      integer Nmat
      character*(Nchar_mx) materials_list(1:Nmaterial_mx)
      character*(Nchar_mx) material
      integer mat_index
c     temp
      logical mat_found
c     label
      character*(Nchar_mx) label
      label='subroutine add_material'

      call find_material(Nmat,materials_list,material,mat_found,mat_index)
      if (.not.mat_found) then
         Nmat=Nmat+1
         if (Nmat.gt.Nmaterial_mx) then
            call error(label)
            write(*,*) 'Nmaterial_mx has been reached'
            stop
         endif
         materials_list(Nmat)=trim(material)
         mat_index=Nmat
      endif

      return
      end



      subroutine find_material(Nmat,materials_list,material,mat_found,mat_index)
      implicit none
      include 'max.inc'
c     
c     Purpose: to find a material to the list of materials
c     
c     Input:
c       + Nmat: number of materials in the list of materials
c       + materials_list: list of materials
c       + material: material to add to the list
c     
c     Output:
c       + mat_found: T if material has been identified
c       + mat_index: index of the material in the list
c     
c     I/O
      integer Nmat
      character*(Nchar_mx) materials_list(1:Nmaterial_mx)
      character*(Nchar_mx) material
      logical mat_found
      integer mat_index
c     temp
      integer imat
c     label
      character*(Nchar_mx) label
      label='subroutine find_material'

      mat_found=.false.
      do imat=1,Nmat
         if (trim(material).eq.trim(materials_list(imat))) then
            mat_found=.true.
            mat_index=imat
            goto 111
         endif
      enddo                     ! imat
 111  continue

      return
      end


      
      subroutine append_mtllib_file(mtllib_file,Ncode,mat_label,rgb_code,mat_name)
      implicit none
      include 'max.inc'
c     
c     Purpose: to append the mtllib file with the provided material
c     
c     Input:
c       + mtllib_file: mtllib file
c       + Ncode: number of codes
c       + mat_label: name of each material
c       + rgb_code: RGB code for each material
c       + mat_name: name of the material
c     
c     Output:
c       + updated "mtllib_file" file
c     
c     I/O
      character*(Nchar_mx) mtllib_file
      integer Ncode
      character*(Nchar_mx) mat_label(1:Ncode_mx)
      integer rgb_code(1:Ncode_mx,1:3)
      character*(Nchar_mx) mat_name
c     temp
      integer i
      double precision color_code(1:3)
      character*(Nchar_mx) filename
c     label
      character*(Nchar_mx) label
      label='subroutine append_mtllib_file'
c
      if ((trim(mat_name).ne.'air').and.(trim(mat_name).ne.'internal_air')) then
         call retrieve_code(Ncode,mat_label,rgb_code,mat_name,color_code)
      endif
      open(43,file=trim(mtllib_file),access='append')
      write(43,*) 'newmtl '//trim(mat_name)
      if ((trim(mat_name).eq.'air').or.
     &     (trim(mat_name).eq.'internal_air')) then
         write(43,*) 'Tr ',1.0D+0
      else if (trim(mat_name).eq.'glass') then
         write(43,*) 'Ka ',(color_code(i),i=1,3)
         write(43,*) 'Kd ',(color_code(i),i=1,3)
         write(43,*) 'Ks ',(color_code(i),i=1,3)
         write(43,*) 'Ns ',10.0D+0
      else
         write(43,*) 'Ka ',(color_code(i),i=1,3)
         write(43,*) 'Kd ',(color_code(i),i=1,3)
      endif
      write(43,*)
      close(43)
      
      return
      end
