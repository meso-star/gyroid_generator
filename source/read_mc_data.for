c     Copyright (C) 2023 |Meso|Star> (contact@meso-star.com)
      subroutine read_mc_data(datafile,Nsample,ddf)
      implicit none
      include 'max.inc'
c     
c     Purpose: to read MC data from file
c     
c     Input:
c       + datafile: file to read
c     
c     Output:
c       + Nsample: number of MC samples
c       + ddf: value by which the thickness of the solid is divided in order to get delta_solid
c     
c     I/O
      character*(Nchar_mx) datafile
      integer Nsample
      double precision ddf
c     temp
      integer i,ios
c     label
      character*(Nchar_mx) label
      label='subroutine read_mc_data'

      open(11,file=trim(datafile),status='old',iostat=ios)
      if (ios.ne.0) then
         call error(label)
         write(*,*) 'File not found: ',trim(datafile)
         stop
      else
         write(*,*) 'Reading file: ',trim(datafile)
         do i=1,3
            read(11,*)
         enddo                  ! i
         read(11,*) Nsample
         read(11,*)
         read(11,*) ddf
      endif
      close(11)

c     consistency
      if (Nsample.lt.1) then
         call error(label)
         write(*,*) 'Nsample=',Nsample
         write(*,*) 'should be > 0'
         stop
      endif
      if (ddf.lt.0.0D+0) then
         call error(label)
         write(*,*) 'ddf=',ddf
         write(*,*) 'should be positive'
         stop
      endif

      return
      end
      
