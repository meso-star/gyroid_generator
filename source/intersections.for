c     Copyright (C) 2023 |Meso|Star> (contact@meso-star.com)
      subroutine contour_self_intersects(dim,
     &     mapXsize,mapYsize,
     &     Ncontour,Nppc,contour,icontour,
     &     intersection_found)
      implicit none
      include 'max.inc'
c     
c     Purpose: detect whether or not a given contour intersects itself
c     
c     Input:
c       + dim: dimension of space
c       + mapXsize: size of the map along the X-coordinate [m]
c       + mapYsize: size of the map along the Y-coordinate [m]
c       + Ncontour: number of contours
c       + Nppc: number of points that define each contour
c       + contour: list of coordinates for each contour
c       + icontour: index of the examined contour
c     
c     Output:
c       + intersection_found: true if contour index "icontour" intersects another contour
c
c     I/O
      integer dim
      double precision mapXsize
      double precision mapYsize
      integer Ncontour
      integer Nppc(1:Ncontour_mx)
      double precision contour(1:Ncontour_mx,1:Nppc_mx,1:Ndim_mx-1)
      integer icontour
      logical intersection_found
c     temp
      integer Nsegi,iseg,jseg,i1,i2,j1,j2,j
      double precision P11(1:Ndim_mx)
      double precision P12(1:Ndim_mx)
      double precision P21(1:Ndim_mx)
      double precision P22(1:Ndim_mx)
      double precision Pint(1:Ndim_mx)
      logical identical,intersection
      double precision alpha,beta,gamma
      double precision u1(1:Ndim_mx)
      double precision u2(1:Ndim_mx)
      double precision d1,d2
      double precision tmp(1:Ndim_mx)
      double precision Pint1(1:Ndim_mx)
      double precision Pint2(1:Ndim_mx)
c     parameters
      double precision epsilon
      parameter(epsilon=1.0D-10)
c     label
      character*(Nchar_mx) label
      label='subroutine contour_self_intersects'

      if ((icontour.lt.1).or.(icontour.gt.Ncontour)) then
         call error(label)
         write(*,*) 'icontour=',icontour
         write(*,*) 'should be in the [1-',Ncontour,'] range'
         stop
      endif
c
      intersection_found=.false.
c     
      Nsegi=Nppc(icontour)
      do iseg=1,Nsegi
         i1=iseg
         if (iseg.eq.Nsegi) then
            i2=1
         else
            i2=iseg+1
         endif
         do j=1,dim-1
            P11(j)=contour(icontour,i1,j)
            P12(j)=contour(icontour,i2,j)
         enddo                  ! j
         P11(dim)=0.0D+0
         P12(dim)=0.0D+0
         call substract_vectors(dim,P12,P11,tmp)
         call vector_length(dim,tmp,d1)
         call identical_positions(dim,P11,P12,identical)
         if (identical) then
c            call error(label)
c            write(*,*) 'P11=',P11
c            write(*,*) 'P12=',P12
c            write(*,*) 'should be different'
c            stop
            goto 222
         endif
c     
         do jseg=1,Nsegi
            if (iseg.eq.jseg) then
               intersection_found=.false.
            else                ! iseg and jseg are different
               j1=jseg
               if (jseg.eq.Nsegi) then
                  j2=1
               else
                  j2=jseg+1
               endif
               do j=1,dim-1
                  P21(j)=contour(icontour,j1,j)
                  P22(j)=contour(icontour,j2,j)
               enddo            ! j
               P21(dim)=0.0D+0
               P22(dim)=0.0D+0
               call substract_vectors(dim,P22,P21,tmp)
               call vector_length(dim,tmp,d2)
               call identical_positions(dim,P21,P22,identical)
               if (identical) then
c                  call error(label)
c                  write(*,*) 'P21=',P21
c                  write(*,*) 'P22=',P22
c                  write(*,*) 'should be different'
c                  stop
                  goto 111
               endif            ! identical
c     
               call substract_vectors(dim,P12,P11,tmp)
               call normalize_vector(dim,tmp,u1)
               call substract_vectors(dim,P22,P21,tmp)
               call normalize_vector(dim,tmp,u2)
c     
               gamma=u1(2)*u2(1)-u1(1)*u2(2)
               if (dabs(gamma).lt.1.0D-10) then
c     This is the case when the 2 lines are parallel: no intersection
                  intersection=.false.
               else
                  intersection=.true.
                  alpha=((P11(1)-P21(1))*u2(2)-
     &                 (P11(2)-P21(2))*u2(1))/gamma
                  if (dabs(u2(1)).le.1.0D-10) then
                     beta=(P11(2)-P21(2)+alpha*u1(2))/u2(2)
                  else
                     beta=(P11(1)-P21(1)+alpha*u1(1))/u2(1)
                  endif
                  call scalar_vector(dim,alpha,u1,tmp)
                  call add_vectors(dim,P11,tmp,Pint1)
                  call scalar_vector(dim,beta,u2,tmp)
                  call add_vectors(dim,P21,tmp,Pint2)
                  call identical_positions(dim,Pint1,Pint2,identical)
c                  if (identical) then
c                     call copy_vector(dim,Pint1,Pint)
c                  else
c                     call error(label)
c                     write(*,*) 'Pint1 and Pint2 are different'
c                     write(*,*) 'P11=',P11
c                     write(*,*) 'P12=',P12
c                     write(*,*) 'u1=',u1
c                     write(*,*) 'alpha=',alpha
c                     write(*,*) 'Pint1=',Pint1
c                     write(*,*) 'P21=',P21
c                     write(*,*) 'P22=',P22
c                     write(*,*) 'u2=',u2
c                     write(*,*) 'beta=',beta
c                     write(*,*) 'Pint2=',Pint2
c                     stop
c                  endif         ! identical
               endif            ! gamma=0
               if ((intersection).and.
     &              (alpha.gt.epsilon).and.
     &              (d1-alpha.gt.epsilon).and.
     &              (beta.gt.epsilon).and.
     &              (d2-beta.gt.epsilon).and.
     &              (identical)) then
                  intersection_found=.true.
                  call copy_vector(dim,Pint1,Pint)
                  goto 666
               endif            ! intersection
            endif               ! iseg=jseg
 111        continue
         enddo                  ! jseg
 222     continue
      enddo                     ! iseg
c     
 666  continue
      return
      end
      

      
      subroutine contours_intersect(dim,
     &     mapXsize,mapYsize,
     &     Ncontour,Nppc,contour,icontour,jcontour,
     &     intersection_found)
      implicit none
      include 'max.inc'
c     
c     Purpose: detect whether or not two given contours intersect each other
c     DO NOT use when icontour=jcontour
c     
c     Input:
c       + dim: dimension of space
c       + mapXsize: size of the map along the X-coordinate [m]
c       + mapYsize: size of the map along the Y-coordinate [m]
c       + Ncontour: number of contours
c       + Nppc: number of points that define each contour
c       + contour: list of coordinates for each contour
c       + icontour: index of the first contour
c       + jcontour: index of the second contour
c     
c     Output:
c       + intersection_found: true if contours index "icontour" and "jcontour" intersect
c
c     I/O
      integer dim
      double precision mapXsize
      double precision mapYsize
      integer Ncontour
      integer Nppc(1:Ncontour_mx)
      double precision contour(1:Ncontour_mx,1:Nppc_mx,1:Ndim_mx-1)
      integer icontour
      integer jcontour
      logical intersection_found
c     temp
      integer i,j,i1,i2,j1,j2
      integer Nsegi,Nsegj,iseg,jseg
      double precision P11(1:Ndim_mx)
      double precision P12(1:Ndim_mx)
      double precision P21(1:Ndim_mx)
      double precision P22(1:Ndim_mx)
      double precision Pint(1:Ndim_mx)
      logical identical,intersection
      double precision alpha,beta,gamma
      double precision u1(1:Ndim_mx)
      double precision u2(1:Ndim_mx)
      double precision d1,d2
      double precision d3,d4
      double precision tmp(1:Ndim_mx)
      double precision x1(1:Ndim_mx)
      double precision x2(1:Ndim_mx)
      double precision uab(1:Ndim_mx)
      double precision uac(1:Ndim_mx)
      double precision uad(1:Ndim_mx)
      double precision Pint1(1:Ndim_mx)
      double precision Pint2(1:Ndim_mx)
      double precision sp1,sp2
      logical id1,id2
c     parameters
      double precision epsilon
      parameter(epsilon=1.0D-10)
c     label
      character*(Nchar_mx) label
      label='subroutine contours_intersect'

      if ((icontour.lt.1).or.(icontour.gt.Ncontour)) then
         call error(label)
         write(*,*) 'icontour=',icontour
         write(*,*) 'should be in the [1-',Ncontour,'] range'
         stop
      endif
      if ((jcontour.lt.1).or.(jcontour.gt.Ncontour)) then
         call error(label)
         write(*,*) 'jcontour=',jcontour
         write(*,*) 'should be in the [1-',Ncontour,'] range'
         stop
      endif
      if (icontour.eq.jcontour) then
         call error(label)
         write(*,*) 'icontour=',icontour
         write(*,*) 'jcontour=',jcontour
         write(*,*) 'should be different'
         stop
      endif
c     
      intersection_found=.false.
c
      Nsegi=Nppc(icontour)
      Nsegj=Nppc(jcontour)
      do iseg=1,Nsegi
         i1=iseg
         if (iseg.eq.Nsegi) then
            i2=1
         else
            i2=iseg+1
         endif
         do j=1,dim-1
            P11(j)=contour(icontour,i1,j)
            P12(j)=contour(icontour,i2,j)
         enddo                  ! j
         P11(dim)=0.0D+0
         P12(dim)=0.0D+0
         call substract_vectors(dim,P12,P11,tmp)
         call vector_length(dim,tmp,d1)
         call identical_positions(dim,P11,P12,identical)
         if (identical) then
c            call error(label)
c            write(*,*) 'P11=',P11
c            write(*,*) 'P12=',P12
c            write(*,*) 'should be different'
c            write(*,*) 'contour ',icontour
c            do i=1,Nppc(icontour)
c               write(*,*) (contour(icontour,i,j),j=1,dim-1)
c            enddo               ! i
c            stop
            goto 222
         endif
c     
         do jseg=1,Nsegj
c     Debug
c            write(*,*) 'iseg=',iseg,' and jseg=',jseg
c            write(*,*) 'P11=',P11
c            write(*,*) 'P12=',P12
c            write(*,*) 'P21=',P21
c            write(*,*) 'P22=',P22
c     Debug
            j1=jseg
            if (jseg.eq.Nsegj) then
               j2=1
            else
               j2=jseg+1
            endif
            do j=1,dim-1
               P21(j)=contour(jcontour,j1,j)
               P22(j)=contour(jcontour,j2,j)
            enddo               ! j
            P21(dim)=0.0D+0
            P22(dim)=0.0D+0
            call substract_vectors(dim,P22,P21,tmp)
            call vector_length(dim,tmp,d2)
            call identical_positions(dim,P21,P22,identical)
            if (identical) then
c               call error(label)
c               write(*,*) 'P21=',P21
c               write(*,*) 'P22=',P22
c               write(*,*) 'should be different'
c               write(*,*) 'contour ',jcontour
c               do i=1,Nppc(jcontour)
c                  write(*,*) (contour(jcontour,i,j),j=1,dim-1)
c               enddo            ! i
c               stop
               goto 111
            endif               ! identical
c     
            call substract_vectors(dim,P12,P11,tmp)
            call normalize_vector(dim,tmp,u1)
            call substract_vectors(dim,P22,P21,tmp)
            call normalize_vector(dim,tmp,u2)
c     
            gamma=u1(2)*u2(1)-u1(1)*u2(2)
c     Debug
c            write(*,*) 'gamma=',gamma
c     Debug
            if (dabs(gamma).lt.epsilon) then
               intersection_found=.false.
            else
               intersection=.true.
               alpha=((P11(1)-P21(1))*u2(2)-
     &              (P11(2)-P21(2))*u2(1))/gamma
               if (dabs(u2(1)).le.1.0D-10) then
                  beta=(P11(2)-P21(2)+alpha*u1(2))/u2(2)
               else
                  beta=(P11(1)-P21(1)+alpha*u1(1))/u2(1)
               endif
               call scalar_vector(dim,alpha,u1,tmp)
               call add_vectors(dim,P11,tmp,Pint1)
               call scalar_vector(dim,beta,u2,tmp)
               call add_vectors(dim,P21,tmp,Pint2)
               call identical_positions(dim,Pint1,Pint2,identical)
c               if (identical) then
c                  call copy_vector(dim,Pint1,Pint)
c               else
c                  call error(label)
c                  write(*,*) 'Pint1 and Pint2 are different'
c                  write(*,*) 'gamma=',gamma
c                  write(*,*) 'P11=',P11
c                  write(*,*) 'P12=',P12
c                  write(*,*) 'u1=',u1
c                  write(*,*) 'alpha=',alpha
c                  write(*,*) 'Pint1=',Pint1
c                  write(*,*) 'P21=',P21
c                  write(*,*) 'P22=',P22
c                  write(*,*) 'u2=',u2
c                  write(*,*) 'beta=',beta
c                  write(*,*) 'Pint2=',Pint2
c                  stop
c               endif            ! identical
               if ((intersection).and.
     &              (alpha.ge.epsilon).and.
     &              (d1-alpha.ge.epsilon).and.
     &              (beta.ge.epsilon).and.
     &              (d2-beta.ge.epsilon).and.
     &              (identical)) then
                  intersection_found=.true.
                  call copy_vector(dim,Pint1,Pint)
                  goto 666
               endif            ! intersection is valid
            endif               ! |gamma| < epsilon
 111        continue
         enddo                  ! jseg
 222     continue
      enddo                     ! iseg
c     
 666  continue
      return
      end


      
      subroutine segment_intersects_contour(dim,P1,P2,Np,ctr,
     &     intersection_found,Pint)
      implicit none
      include 'max.inc'
c
c     Purpose: to test the intersection between a segment and a contour
c
c     Input:
c       + dim: dimension of space
c       + P1: firt point of segment
c       + P2: second point of segment
c       + Np: number of points in contour
c       + ctr: contour
c
c     Output:
c       + intersection_found: true if a intersection between the segment
c     and a segment of the contour has been found
c       + Pint: intersection position
c
c     I/O
      integer dim
      double precision P1(1:Ndim_mx)
      double precision P2(1:Ndim_mx)
      integer Np
      double precision ctr(1:Nppc_mx,1:Ndim_mx)
      logical intersection_found
      double precision Pint(1:Ndim_mx)
c     temp
      integer iseg,Nseg
      integer i1,i2,j
      double precision P3(1:Ndim_mx)
      double precision P4(1:Ndim_mx)
      double precision tmp(1:Ndim_mx)
      double precision d1,d2
      logical identical,intersection
      double precision u1(1:Ndim_mx)
      double precision u2(1:Ndim_mx)
      double precision alpha,beta,gamma
      double precision Pint1(1:Ndim_mx)
      double precision Pint2(1:Ndim_mx)
c     parameters
      double precision epsilon
      parameter(epsilon=1.0D-10)
c     label
      character*(Nchar_mx) label
      label='subroutine segment_intersects_contour'

      call substract_vectors(dim,P2,P1,tmp)
      call vector_length(dim,tmp,d1)
      
      Nseg=Np
      do iseg=1,Nseg
         i1=iseg
         if (iseg.eq.Nseg) then
            i2=1
         else
            i2=iseg+1
         endif
         do j=1,dim-1
            P3(j)=ctr(i1,j)
            P4(j)=ctr(i2,j)
         enddo                  ! j
         P3(dim)=0.0D+0
         P4(dim)=0.0D+0
         call substract_vectors(dim,P4,P3,tmp)
         call vector_length(dim,tmp,d2)
         call identical_positions(dim,P3,P4,identical)
         if (identical) then
            goto 111
         endif                  ! identical
c     
         call substract_vectors(dim,P2,P1,tmp)
         call normalize_vector(dim,tmp,u1)
         call substract_vectors(dim,P4,P3,tmp)
         call normalize_vector(dim,tmp,u2)
c     
         gamma=u1(2)*u2(1)-u1(1)*u2(2)
         if (dabs(gamma).lt.epsilon) then
            intersection_found=.false.
         else
            intersection=.true.
            alpha=((P1(1)-P3(1))*u2(2)-
     &           (P1(2)-P3(2))*u2(1))/gamma
            if (dabs(u2(1)).le.1.0D-10) then
               beta=(P1(2)-P3(2)+alpha*u1(2))/u2(2)
            else
               beta=(P1(1)-P3(1)+alpha*u1(1))/u2(1)
            endif
            call scalar_vector(dim,alpha,u1,tmp)
            call add_vectors(dim,P1,tmp,Pint1)
            call scalar_vector(dim,beta,u2,tmp)
            call add_vectors(dim,P3,tmp,Pint2)
            call identical_positions(dim,Pint1,Pint2,identical)
            if (identical) then
               call copy_vector(dim,Pint1,Pint)
            else
               call error(label)
               write(*,*) 'Pint1 and Pint2 are different'
               write(*,*) 'gamma=',gamma
               write(*,*) 'P1=',P1
               write(*,*) 'P2=',P2
               write(*,*) 'u1=',u1
               write(*,*) 'alpha=',alpha
               write(*,*) 'Pint1=',Pint1
               write(*,*) 'P3=',P3
               write(*,*) 'P4=',P4
               write(*,*) 'u2=',u2
               write(*,*) 'beta=',beta
               write(*,*) 'Pint2=',Pint2
               stop
            endif               ! identical
            if ((intersection).and.
     &           (alpha.ge.epsilon).and.
     &           (d1-alpha.ge.epsilon).and.
     &           (beta.ge.epsilon).and.
     &           (d2-beta.ge.epsilon)) then
               intersection_found=.true.
               goto 666
            endif               ! intersection is valid
         endif                  ! |gamma| < epsilon
 111     continue
      enddo                     ! iseg

 666  continue
      return
      end
      

      
      subroutine line_line_intersection(dim,P11,P12,P21,P22,constrain,
     &     intersection_found,Pint)
      implicit none
      include 'max.inc'
c     
c     Purpose: to compute the intersection position between 2 lines defined
c     by 2 positions, on the same z=cst plane (2D)
c     
c     Input:
c       + dim: dimension of space
c       + P11: first position that defines line 1
c       + P12: second position that defines line 1
c       + P21: first position that defines line 2
c       + P22: second position that defines line 2
c       + constrain: true if intersection has to be constrained within the 2 segments
c     
c     Output:
c       + intersection_found: true if a intersection was found
c       + Pint: intersection position if intersection_found=T
c     
c     I/O
      integer dim
      double precision P11(1:Ndim_mx)
      double precision P12(1:Ndim_mx)
      double precision P21(1:Ndim_mx)
      double precision P22(1:Ndim_mx)
      logical constrain
      logical intersection_found
      double precision Pint(1:Ndim_mx)
c     temp
      logical identical,intersection
      double precision alpha,beta,gamma
      double precision u1(1:Ndim_mx)
      double precision u2(1:Ndim_mx)
      double precision d1,d2
      double precision tmp(1:Ndim_mx)
      double precision Pint1(1:Ndim_mx)
      double precision Pint2(1:Ndim_mx)
c     parameters
      double precision epsilon
      parameter(epsilon=1.0D-10)
c     label
      character*(Nchar_mx) label
      label='subroutine line_line_intersection'

      call identical_positions(dim,P11,P12,identical)
      if (identical) then
         call error(label)
         write(*,*) 'P11=',P11
         write(*,*) 'P12=',P12
         write(*,*) 'should be different'
         stop
      endif
      call identical_positions(dim,P21,P22,identical)
      if (identical) then
         call error(label)
         write(*,*) 'P21=',P21
         write(*,*) 'P22=',P22
         write(*,*) 'should be different'
         stop
      endif

      call substract_vectors(dim,P12,P11,tmp)
      call vector_length(dim,tmp,d1)
      call normalize_vector(dim,tmp,u1)
      call substract_vectors(dim,P22,P21,tmp)
      call vector_length(dim,tmp,d2)
      call normalize_vector(dim,tmp,u2)

      gamma=u1(2)*u2(1)-u1(1)*u2(2)
      intersection=.false.
c     Debug
c      write(*,*) 'gamma=',gamma
c     Debug
      if (dabs(gamma).lt.epsilon) then
c     This is the case when the 2 lines are parallel: no intersection
         intersection_found=.false.
      else
         intersection=.true.
         alpha=((P11(1)-P21(1))*u2(2)-(P11(2)-P21(2))*u2(1))/gamma
         if (dabs(u2(1)).le.1.0D-10) then
            beta=(P11(2)-P21(2)+alpha*u1(2))/u2(2)
         else
            beta=(P11(1)-P21(1)+alpha*u1(1))/u2(1)
         endif
         call scalar_vector(dim,alpha,u1,tmp)
         call add_vectors(dim,P11,tmp,Pint1)
         call scalar_vector(dim,beta,u2,tmp)
         call add_vectors(dim,P21,tmp,Pint2)
         call identical_positions(dim,Pint1,Pint2,identical)
         if (identical) then
            call copy_vector(dim,Pint1,Pint)
         else
            call error(label)
            write(*,*) 'Pint1 and Pint2 are different'
            write(*,*) 'P11=',P11
            write(*,*) 'P12=',P12
            write(*,*) 'u1=',u1
            write(*,*) 'alpha=',alpha
            write(*,*) 'Pint1=',Pint1
            write(*,*) 'P21=',P21
            write(*,*) 'P22=',P22
            write(*,*) 'u2=',u2
            write(*,*) 'beta=',beta
            write(*,*) 'Pint2=',Pint2
            stop
         endif                  ! identical
         if ((constrain).and.(intersection).and.(identical)) then
            if ((alpha.gt.epsilon).and.
     &           (d1-alpha.gt.epsilon).and.
     &           (beta.gt.epsilon).and.
     &           (d2-beta.gt.epsilon)) then
               intersection_found=.true.
            else
               intersection_found=.false.
            endif               ! intersection is within each segment
         else                   ! no constrain
            intersection_found=.true.
         endif                  ! intersection AND Pint1=Pint2
      endif                     ! gamma=0

      return
      end



      subroutine line_plane_intersection(dim,P0,u,v,P1,w,Pint)
      implicit none
      include 'max.inc'
c     
c     Purpose: to get the intersection between a line and a plane
c     
c     Input:
c       + dim: dimension of space
c       + P0: point that belongs to the plane
c       + u,v: defining directions of the plane
c       + P1: point that belongs to the line
c       + w: defining direction of the line
c     
c     Output:
c       + Pint: intersection position between plane (P0,u,v) and line (P1,w)
c     
c     I/O
      integer dim
      double precision P0(1:Ndim_mx)
      double precision u(1:Ndim_mx)
      double precision v(1:Ndim_mx)
      double precision P1(1:Ndim_mx)
      double precision w(1:Ndim_mx)
      double precision Pint(1:Ndim_mx)
c     temp
      double precision a(1:Ndim_mx,1:Ndim_mx)
      double precision y(1:Ndim_mx)
      logical valid
      double precision x(1:Ndim_mx)
      integer i,j
      double precision Pint1(1:Ndim_mx)
      double precision Pint2(1:Ndim_mx)
      double precision d
      double precision t1(1:Ndim_mx)
      double precision t2(1:Ndim_mx)
      double precision t3(1:Ndim_mx)
c     parameters
      double precision epsilon_d
      parameter(epsilon_d=1.0D-8) ! [m]
c     label
      character*(Nchar_mx) label
      label='subroutine line_plane_intersection'

      do i=1,dim
         a(i,1)=u(i)
         a(i,2)=v(i)
         a(i,3)=-w(i)
         y(i)=P1(i)-P0(i)
      enddo                     ! i
      call linear_system(0,dim,a,y,valid,x)
      if (valid) then
         call scalar_vector(dim,x(1),u,t1)
         call scalar_vector(dim,x(2),v,t2)
         call add_vectors(dim,P0,t1,t3)
         call add_vectors(dim,t3,t2,Pint1) ! Pint1: intersection position computed from the definition of the plane
         call scalar_vector(dim,x(3),w,t1)
         call add_vectors(dim,P1,t1,Pint2) ! Pint2: intersection position computed from the definition of the line
         call distance(dim,Pint1,Pint2,d)
         if (d.lt.epsilon_d) then
            call copy_vector(dim,Pint1,Pint)
         else
            call error(label)
            write(*,*) 'Pint1=',Pint1
            write(*,*) 'Pint2=',Pint2
            write(*,*) 'd=',d
            write(*,*) '> epsilon_d=',epsilon_d
            stop
         endif
      else
         call error(label)
         write(*,*) 'could not solve linear system'
         stop
      endif
      
      return
      end
