c     Copyright (C) 2020 |Meso|Star> (contact@meso-star.com)
      subroutine delaunay_triangulation(dim,Ncontour,Nppc,contour,
     &     Nv,Nf,vertices,faces)
      implicit none
      include 'max.inc'
c     
c     Purpose: to perform a Delaunay triangulation of a close surface
c     provided its contour
c     
c     Input:
c       + dim: dimension of space
c       + Ncontour: number of contours (including holes)
c       + Nppc: number of points for each contour
c       + contour: list of points that define each contour
c     
c     Output:
c       + Nv: number of vertices
c       + Nf: number of triangular faces that have been generated
c       + vertices: vertices
c       + faces: index of the 3 vertices that define each face
c     
c     I/O
      integer dim
      integer Ncontour
      integer Nppc(1:Ncontour_mx)
      double precision contour(1:Ncontour_mx,1:Nppc_mx,1:Ndim_mx)
      integer Nv,Nf
      double precision vertices(1:Nv_so_mx,1:Ndim_mx)
      integer faces(1:Nf_so_mx,1:3)
c     temp
      integer i,j,ic
      character*(Nchar_mx) poly_file,outfile
      character*(Nchar_mx) command
      character*(Nchar_mx) output_file
      logical invert_normal
      logical normal_is_unique
      double precision normal(1:Ndim_mx)
      double precision k(1:Ndim_mx)
      double precision minus_k(1:Ndim_mx)
      integer Np,ictr
      double precision ctr(1:Nppc_mx,1:Ndim_mx)
      logical is_k,is_minus_k
      logical normal_is_unique2,is_k2,is_minus_k2
      double precision normal2(1:Ndim_mx)
c     label
      character*(Nchar_mx) label
      label='subroutine delaunay_triangulation'

c     Debug
c      write(*,*) 'ok8.1'
c     Debug
      
c     vertical upward direction
      k(1)=0.0D+0
      k(2)=0.0D+0
      k(3)=1.0D+0
c     vertical downward direction
      minus_k(1)=0.0D+0
      minus_k(2)=0.0D+0
      minus_k(3)=-1.0D+0
c
      do ictr=1,Ncontour
c     Debug
c         write(*,*) 'ictr=',ictr,' /',Ncontour
c     Debug
         call get_contour(dim,Ncontour,Nppc,contour,ictr,Np,ctr)
         call contour_normal(dim,Np,ctr,normal_is_unique,normal)
c     Debug
c         write(*,*) 'Contour index ',ictr,' /',Ncontour
c         write(*,*) 'normal_is_unique=',normal_is_unique
c         if (normal_is_unique) then
c            write(*,*) 'normal=',(normal(j),j=1,dim)
c         endif                  ! normal_is_unique
c     Debug
         if (.not.normal_is_unique) then
            call error(label)
            write(*,*) 'Contour index:',ictr,' /',Ncontour
            write(*,*) 'normal is not unique'
c            do i=1,Nppc(ictr)
c               write(*,*) (contour(ictr,i,j),j=1,dim)
c            enddo               ! i
            stop
         else
            call identical_vectors(dim,normal,k,is_k)
c     Debug
c            write(*,*) 'normal_is_unique=',normal_is_unique
c            write(*,*) 'normal=',normal
c            write(*,*) 'is_k=',is_k
c     Debug
            if (.not.is_k) then
               call identical_vectors(dim,normal,minus_k,is_minus_k)
               if (is_minus_k) then
                  call reverse_contour(dim,Np,ctr)
c     double-check
                  call contour_normal(dim,Np,ctr,normal_is_unique2,normal2)
c     Debug
c                  write(*,*) 'after inversion: normal_is_unique2=',normal_is_unique2,' normal2=',normal2
c     Debug
                  call identical_vectors(dim,normal2,k,is_k2)
                  call identical_vectors(dim,normal2,minus_k,is_minus_k2)
                  if ((.not.is_k2).and.(.not.is_minus_k2)) then
                     call error(label)
                     write(*,*) 'Contour index:',ictr,' /',Ncontour
                     write(*,*) 'normal has been inverted but is still not k'
                     write(*,*) 'normal2=',normal2
                     stop
                  else
                     call set_contour(dim,Ncontour,Nppc,contour,ictr,Np,ctr)
                  endif         ! is_k=F
               else
                  call error(label)
                  write(*,*) 'Contour index:',ictr,' /',Ncontour
                  write(*,*) 'normal=',(normal(j),j=1,dim)
                  write(*,*) 'should be either k or -k'
                  stop
               endif            ! is_minus_k
            endif               ! is_k=F
         endif                  ! normal_is_unique
      enddo                     ! ictr

c     Debug
c      write(*,*) 'ok8.2'
c     Debug
      poly_file='./triangle/contour'
      call record_poly_file(dim,Ncontour,Nppc,contour,poly_file,outfile)
c     Debug
c      write(*,*) 'ok8.3'
c     Debug
c     -----------------------------------------------------------------
c     run TRIANGLE
c      
c     quality mesh (-q option)
c     command='./triangle/triangle -Qpq0L '//trim(outfile)
c
c     Add no intermediary points to the provided contours (-YY option)
      command='./triangle/triangle -QpYY '//trim(outfile)
c     -----------------------------------------------------------------      
      call exec(command)
      call read_result_files(poly_file,dim,Nv,vertices,Nf,faces)

      return
      end
