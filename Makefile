# SHELL = /bin/sh
MAINDIR=$(shell /bin/pwd)
UN=$(shell echo ${USER})
HOST=$(shell echo `hostname`)
EXEC=./gyroid_generator.exe
FOR=gfortran #-L/usr/lib
MAKE=make
#ARCH=-m486 -msparclite -mips3 # options pour une machine 32 bits
#ARCH=-march=i686 -m64 # options pour un i686 en 64 bits
ARCH=-m64
OPTI=-O3 -static -fbackslash -ffixed-line-length-none #-fast
WARNING=-fbounds-check -Warray-bounds -Walign-commons
DEBUG=

#############################
# FFLAGS= -O
# FFLAGS= -ff90
# FFLAGS= -ftrength-reduce (les loop vont plus vite cf gcc)
# FFLAGS= -g -ftrenght-reduce (cf gcc option va unused corteu)
############################Mieux vaut ajuster par des directives de compilation
############################inserer dans le prog quand le compilateur le permet.
FFLAGS = $(ARCH) $(OPTI) $(WARNING) $(IEEE) $(EMUL90) $(DEBUG)

####################################################################################
#   DIR is the path to source files directory				#
#   OBJ is the path to object files directory				#
#   SRC_PROG is the list of source files to be compiled			#
#   INCLUDES is: -I+path to directory that contains 'include' files	#
####################################################################################

DIR=${MAINDIR}/source
OBJ=${MAINDIR}/objects
DAT=${MAINDIR}/data

SRC_PROG = \
	$(DIR)/add2obj.for \
	$(DIR)/bounding_box.for \
	$(DIR)/connectivity.for \
	$(DIR)/contours.for \
	$(DIR)/coordinates.for \
	$(DIR)/delaunay_triangulation.for \
	$(DIR)/detect_command.for \
	$(DIR)/error.for \
	$(DIR)/exec.for \
	$(DIR)/filenames.for \
	$(DIR)/find_character.for \
	$(DIR)/geometry.for \
	$(DIR)/get_nlines.for \
	$(DIR)/gyroid_generator.for \
	$(DIR)/init.for \
	$(DIR)/intersections.for \
	$(DIR)/is_even.for \
	$(DIR)/materials.for \
	$(DIR)/matrix.for \
	$(DIR)/modify_obj.for \
	$(DIR)/num2str.for \
	$(DIR)/obj2stl.for \
	$(DIR)/objects.for \
	$(DIR)/patches.for \
	$(DIR)/random_gen.for \
	$(DIR)/read_data.for \
	$(DIR)/read_fct.for \
	$(DIR)/read_fluid.for \
	$(DIR)/read_mc_data.for \
	$(DIR)/read_nct.for \
	$(DIR)/read_obj.for \
	$(DIR)/read_probe_positions.for \
	$(DIR)/read_probe_times.for \
	$(DIR)/read_ref_gyroid_data.for \
	$(DIR)/read_soles.for \
	$(DIR)/read_solid.for \
	$(DIR)/record_obj.for \
	$(DIR)/rgb.for \
	$(DIR)/rotation.for \
	$(DIR)/soles.for \
	$(DIR)/solve.for \
	$(DIR)/stardis.for \
	$(DIR)/triangle_files.for \
	$(DIR)/triangulation.for \
	$(DIR)/zufall.for

INCLUDES= -I./includes

# Dependencies
# compile what ?
OBJ_PROG=$(SRC_PROG:$(DIR)/%.for=$(OBJ)/%.o)
# how to compile ?
$(OBJ)/%.o: $(DIR)/%.for
	$(FOR) $(FFLAGS) $(INCLUDES) -c -o $@ $(@:$(OBJ)/%.o=$(DIR)/%.for)

# Custom rules
to :	all # default when invoked with no rule

clean :
	@rm -f *.o $(OBJ)/*.o *.oub *.oup *.oug
	@rm -f *%
	@rm -f *~
	@rm -f $(DIR)/*.~
	@rm -f $(DIR)/*.*~
	@rm -f tmp.tmp
	@rm -f *.sum
	@rm -f *.eps
	@rm -f *.ps
	@rm -f *.jpg
	@rm -f core
	@rm -f last.kumac
	@rm -f paw.metafile
	@rm -f core
	@touch core
	@chmod 000 core
	@chmod a+r core
	@echo 'Files have been removed'

dat :
	@cd ./data;f0;make_data.exe
	@echo 'Program has been compiled'

wipe :
	@rm -f *%
	@rm -f *~
	@rm -f $(DIR)/*.~
	@rm -f $(DIR)/*.*~
	@rm -f tmp.tmp
	@rm -f *.sum
	@rm -f *.eps
	@rm -f *.ps
	@rm -f *.jpg
	@rm -f core
	@rm -f last.kumac
	@rm -f paw.metafile
	@rm -f core
	@touch core
	@chmod 000 core
	@echo 'Files have been removed'

skel :	clean
	rm -f $(EXEC)
	@echo 'Files have been removed'

where :
	@echo Current directory: $(MAINDIR)
	@echo Source directory: $(DIR)

info :
	@echo User name: $(USER)
	@echo Host name: $(HOST)
	@echo Number of cores: $(shell grep -c -e "processor" /proc/cpuinfo)

all :
	@echo $(DIR)
	@echo '------ Compilation -------'
	make -f Makefile main

fast :
	@echo $(DIR)
	@echo '------ Compilation -------'
	+make -j$(shell grep -c -e "processor" /proc/cpuinfo) -f Makefile main

main :	$(OBJ_PROG)
	@echo '------ Edition de liens -------'
	$(FOR) -o $(EXEC) $(OBJ_PROG)
	@echo '--- Use program ' $(EXEC)  '(compiled for :' $(OSV) $(OSTYPEV) 'by:' $(USER) ')'
	@date > compdate.in

run :
	@$(EXEC)
	@echo 'Done'

#outils pour debugger autres que print
gbd :
	@echo 'gdb ou xxgdb (main)'

#outils pour profiling : optimisations scalaire (ou vectorielle)
gprof :
	@gprof $(EXEC) gmon.out > gprof_sl
	@more gprof_sl

edit :
	emacs &

shell :
	xterm

man :
	make help

help :
	@echo '|------------------------------------------|'
	@echo '|------------------------------------------|'
	@echo '|  AIDE-MEMOIRE                            |'
	@echo '|------------------------------------------|'
	@echo '| help, edit, shell                        |'
	@echo '| gdb, gprof for debug                     |'
	@echo '|------------------------------------------|'
	@echo '| clean: remove  (.o, *~, etc.)            |'
	@echo '| skel: remove ' $(EXEC) '                 |'
	@echo '| to, all, run                             |'
	@echo '|------------------------------------------|'
