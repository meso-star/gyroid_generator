# gyroid_generator

The purpose of "gyroid_generator" is to produce all necessary files, and in
particular geometry files, that are required to perform a Stardis temperature
simulation within a solid whose enveloppe is given by the resolution of the
implicit gyroid equation:

sin(2𝝅*x/Lx)*cos(2𝝅*y/Ly)+sin(2𝝅*y/Ly)*cos(2𝝅*z/Lz)+sin(2𝝅*z/Lz)*cos(2𝝅*x/Lx)=t

with t the parameter of the equation.

See the user guide for examples of geometries for various values of t.


<img style="display: block; margin: auto;"
src="./Documentation/figures/gyroid_t=1point2_solidtubes.png" width="350">
<p style="text-align: center;">
Geometry produced with t=1.2
</p>


## How to build

gyroid_generator is written in fortran; you should first install a fortran
compiler. It was developped using gfortran, but if you want to use any other
compiler, you should replace "gfortran" by the name of your compiler in the "Makefile".

Then the program can be compiled using command "make". This should produce
the "gyroid_generator.exe" executable, that can be run using command:

./gyroid_generator.exe

Whenever a modidication is made to a source file, running the "make" command
again will recompile the modified file(s), then link the object files again
in order to produce the new executable.

If a include file is modified, the executable must be recompiled from scratch.
In order to do this, run the "make clean" command before running the "make"
command again, in order to clean existing object files. Or just run the
"make clean all" command.

## Usage

The full userguide can be found within the "Documentation" directory.

Input data files reside in the main directory:

+ data.in
+ fluid.in
+ mc.in
+ probe_positions.in
+ probe_times.in
+ solid.in

Output files are located in the /results directory:

+ .obj files that may be used for geometry visualization using meshlab
+ .stl files that are required for the description of interfaces in Stardis
+ model.txt is the Stardis model file
+ run.bash is the bash script that will perform the Stardis run(s)
+ intermediate storage files: node_normals.txt, ref_fct.txt, ref_gyroid.dat, ref_nct.txt, v_on_boundary.txt

## License

Copyright (C) 2023 [|Meso|Star>](http://www.meso-star.com). gyroid_generator is free
software released under the GPL v3+ license: GNU GPL version 3 or later. You
are welcome to redistribute it under certain conditions; refer to the COPYING
file for details.
