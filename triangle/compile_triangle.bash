#!/bin/bash
rm -f triangle triangle.c
if [ $HOSTNAME = 'Bob' ]; then
    ln -s triangle_lld.c triangle.c
else
    ln -s triangle_ld.c triangle.c
fi
cc -O -o triangle triangle.c -lm
exit 0
